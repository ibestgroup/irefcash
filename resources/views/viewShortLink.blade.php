<!doctype html>
<html lang="ru">
    <head>
    @php
        session_start();
        $serf_x = rand(5, 290);
        $serf_y = rand(5, 42);
        $serf_timer = 15;
    @endphp
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,700;1,200&display=swap" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="{!! url('setting/'.Setting::get('favicon')) !!}">

    <style>
        html, body {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
            font-family: 'Source Sans Pro', Arial;
        }
        /*iframe {*/
            /*width: 100%;*/
            /*height: calc(100vh - 50px);*/
        /*}*/
        body {
             padding: 0px;
             margin: 0px;
             height: 100%;
             overflow: hidden;
             font-size: 17px;
         }
        .btn-ok {
            background-color: #1d94ea;
            width: 10px;
            height: 10px;
            border-radius: 50%;
            position: absolute;
            top: {{$serf_y}}px;
            left: {{$serf_x}}px;
        }
        .serf_top_panel {
            position: relative;
            width: 100%;
            height: 100px;
            background: #ffffff;
            border-bottom: 2px solid #4266A3;
            line-height: 100px;
        }
        #framesite {
            background: #ffffff;
        }
        #timer {
            position: relative;
            height: 60px;
            width: 300px;
            padding-left: 40px;
            display: inline-block;
            line-height: 60px;
            margin: 20px 0;
            border-right: 2px solid #D8E0E9;
        }
    </style>
    <script>
        //высота браузера
        function windowHeight() {
            return document.compatMode=='CSS1Compat'?document.documentElement.clientHeight:document.body.clientHeight;
        }

        $(document).ready(function() {
            $('#framesite').css({height: windowHeight () - 102});
        });

        let tm, act;
        let t = 15;
        let v_time = 15;
        clearInterval(tm);
        let active = false, inactive = false;
        $(window).focus(function(){active = false}).blur(function(){active = true});
        function iframe_time() {
            if (t > 0) {
                if ( v_time >= 40 ) {
                    if ( navigator.userAgent.indexOf('Opera') != -1 ) {
                        inactive = ((typeof window.hasFocus != 'undefined' ? window.hasFocus() : active) ? true : false);
                    } else {
                        if(document.hasFocus()) {
                            inactive = false;
                            active = false;
                        } else {
                            inactive = true;
                            active = true;
                        }
                    }

                    if ( !inactive ) {
                        active = false;
                    }

                    if ( active == true ) {
                        $('#timer').html('Таймер не активен <span style="font-size:11px; color:#800000; cursor:pointer">активировать</span>').addClass('t_active');
                        return;
                    }
                }
                t--;
                $('#timer').html('Пожалуйста, подождите <span style="font-size:14px; color:#4266A3">'+t+' </span>сек').addClass('t_active');
            } else
            if (t == 0) {
                clearInterval(tm);
                $('#timer').html('<a href="#" onclick="serf_ok({{$serf_x}},{{$serf_y}});return false;" style="position:absolute; left:<{{$serf_x}}px; top:<{{$serf_y}}px;" class="btn-ok"></a>').removeClass('t_active');
            }
        }

        function start_time() {
            act = setInterval('onactive()', 1000);
        }

        function onactive() {
            if (v_time >= 40) {
                if ( navigator.userAgent.indexOf('Opera') != -1 ) {
                    inactive = ((typeof window.hasFocus != 'undefined' ? window.hasFocus() : active) ? true : false);
                } else {
                    if(document.hasFocus()) {
                        inactive = false;
                        active = false;
                    } else {
                        inactive = true;
                        active = true;
                    }
                }

                if ( !inactive ) {
                    clearInterval(act);
                    tm = setInterval('iframe_time();', 1000);
                        <?unset( $_SESSION['timer_serf'] ); $_SESSION['timer_serf'] = time() + $serf_timer; ?>
                }
            } else {
                clearInterval(act);
                tm = setInterval('iframe_time();', 1000);
                    <?unset( $_SESSION['timer_serf'] ); $_SESSION['timer_serf'] = time() + $serf_timer; ?>
            }
        }

        $(document).ready(function () {
            start_time();
            $('body').on('click', '.btn-ok', function () {
                $.post('/ajax/short-link/reward', {id: '{{$visitId}}' }, (data) => {
                    $('#timer').html(data.message);
                    window.location = '{{$shortLink->link}}';
                });
            });
        });
    </script>
    </head>
    <body>
        <div class="serf_top_panel">
            <div id="timer">Дождитесь загрузки сайта</div>
        </div>
        <iframe src="{{$advert->link}}" width="100%" id="framesite" frameborder="0" scrolling="yes" name="framesite"></iframe>
    </body>
</html>

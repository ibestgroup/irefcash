<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.6/clipboard.min.js" integrity="sha512-hDWGyh+Iy4Mr9AHOzUP2+Y0iVPn/BwxxaoSleEjH/i1o4EVTF/sh0/A1Syii8PWOae+uPr+T/KHwynoebSuAhw==" crossorigin="anonymous"></script>
<div class="container">
    <div class="row" id="refresh">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <div class="col-lg-10 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading header-text">Создать новую ссылку</div>
                <div class="panel-body">
                    <form action="{{route('addShortLink')}}" method="post" class="js-store" data-refresh="#refresh">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="">Ссылка для сокращения</label>
                                    <input type="text" class="form-control" name="link" placeholder="Введите ссылку">
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="">Зарабатывать на рекламе</label>
                                    <select name="monetize" id="" class="form-control">
                                        <option value="0">Не рекламировать</option>
                                        <option value="1">Не дольше 5 секунд рекламы</option>
                                        <option value="2">Показывать любую рекламу</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <label for="">Сократить</label>
                                <button class="btn btn-success">Сократить ссылку</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @forelse($links as $item)
                <div class="col-lg-12">
                    <div class="surfing-link" id="surfing-link-{{$item->id}}">
                        <div class="row">
                            <div class="col-lg-4">
                                <h3>
                                    <img src="https://www.google.com/s2/favicons?domain={{$item->link}}" alt="">
                                    <a href="{{$item->link}}">{{$item->link}}</a>
                                    <br>
                                    <span>(#{{$item->id}}) <a href="/{{\App\Libraries\Helpers::convertUs('login', $item->user_id)}}">{{\App\Libraries\Helpers::convertUs('login', $item->user_id)}}</a></span>
                                </h3>
                            </div>
                            <div class="col-lg-3">
                                <h3>
                                    <a style="text-transform: lowercase!important;" href="/s/{{$item->link_id}}" target="_blank" id="copy-{{$item->id}}">https://iref.cash/s/{{$item->link_id}}</a> <button class="btn btn-success btn-copy" data-clipboard-target="#copy-{{$item->id}}"><i class="fa fa-copy"></i></button>
                                </h3>
                            </div>
                            <div class="col-lg-2">
                                @if ($item->monetize)
                                    <h4>Заработано: {{$item->profit()}} <i class="fa fa-rub"></i></h4>
                                @else
                                    <h4>Монетизация отключена</h4>
                                @endif
                            </div>
                            <div class="col-lg-3">
                                <canvas style="height: 70px;" id="statistic-chart-{{$item->id}}" class="get-chart" data-labels="{{\App\Libraries\Helpers::getLastNDays(7, 'Y-m-d', true)}}" data-values="{{$item->completeLastDays()}}"></canvas>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            @empty
                у вас нет ссылок
            @endforelse
        </div>
    </div>
    <script>
        $(document).ready(function () {

            new ClipboardJS('.btn-copy');

            $('body').on('click', '.btn-copy', function () {
                notify('Ссылка скопирована в буфер обмена', 'success');
            });

        });

    </script>
</div>
<div class="container">
@php
@endphp
    <div class="row" id="refresh">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <div class="col-lg-10 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading header-text">Мои ссылки</div>
                <div class="panel-body">
                    <form action="{{route('addShortLinkAdvert')}}" method="post" class="js-store" data-refresh="#refresh">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="">Название</label>
                                    <input type="text" class="form-control" name="name" placeholder="Название сайта">
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="">Ссылка</label>
                                    <input type="text" class="form-control" name="link" placeholder="Ссылка на сайт">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <label for="">Рекламировать</label>
                                <button class="btn btn-success">Добавить рекламу</button>
                            </div>
                        </div>
                    </form>

                    @forelse($advert as $item)
                        <div class="col-lg-12">
                            <div class="surfing-link" id="advert-{{$item->id}}">
                                <div class="row">
                                    <div class="col-lg-1 surfing-link-play">
                                        @if ($item->status)
                                            <i class="fa fa-2x fa-play" onclick="$('#surfing-play-{{$item->id}}').submit();" style="color:#35aa47;"></i>
                                            <br>
                                            <span>работает</span>
                                        @else
                                            <i class="fa fa-2x fa-pause" onclick="$('#surfing-play-{{$item->id}}').submit();" style="color:#f1c72b;"></i>
                                            <span>остановлено</span>
                                        @endif
                                        <form action="{{route('playShortLinkAdvert')}}" class="js-store" id="surfing-play-{{$item->id}}" data-refresh="#advert-{{$item->id}}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="link_id" value="{{$item->id}}">
                                        </form>
                                    </div>
                                    <div class="col-lg-5">
                                        <h3>
                                            <img src="https://www.google.com/s2/favicons?domain={{$item->link}}" alt="">
                                            <a href="{{$item->link}}">{{$item->name}}</a>
                                            <span>(#{{$item->id}}) <a href="/{{\App\Libraries\Helpers::convertUs('login', $item->user_id)}}">{{\App\Libraries\Helpers::convertUs('login', $item->user_id)}}</a></span>
                                        </h3>
                                    </div>
                                    <div class="col-lg-3">
                                        <h4>Баланс: {{$item->balance}} <i class="fa fa-rub"></i></h4>
                                        <form action="{{ route('depositShortLinkAdvert') }}" class="js-store" data-refresh="#advert-{{$item->id}}">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="advert_id" value="{{$item->id}}">
                                                <div class="col-lg-6">
                                                    <input type="number" class="form-control" name="amount" placeholder="Сумма">
                                                </div>
                                                <div class="col-lg-6">
                                                    <button class="btn btn-success">Пополнить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-lg-3">
                                        <canvas style="height: 70px;" id="statistic-chart-{{$item->id}}" class="get-chart" data-labels="{{\App\Libraries\Helpers::getLastNDays(7, 'Y-m-d', true)}}" data-values="{{$item->completeLastDays()}}"></canvas>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    @empty
                        нет ссылок для вас
                    @endforelse
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
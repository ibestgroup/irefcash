<div class="container">
	<div class="row">
		<div class="visible-lg col-lg-2">
			{!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
		</div>

		<div class="col-md-10 col-lg-10">

			<div class="panel panel-default">
				<div class="panel-heading header-text">Мониторинг МЛМ сайтов</div>
				<div class="panel-body">
					<p>Здесь находятся сайты, которые работают через гарант.</p>
					<div class="row">
						@forelse($sites as $site)
							<div class="col-lg-6">
								<div class="col-lg-12" style="border:1px solid #eee; border-radius: 10px; margin-bottom: 10px;">
									<h2 class="text-center"><img src="{{$site->link}}/{{$site->favicon}}" style="width: 25px; height: 25px;" alt=""> {{$site->site_name}}</h2>
									<h3>Минимальный вход: {{$site->min_enter}}</h3>
									<h3>Старт: {{$site->start}}</h3>
									<h3>Участники: {{$site->members}}</h3>
									<h3>Ссылка: <a href="{{$site->link}}">{{$site->link}}</a></h3>
								</div>
							</div>
						@empty
							нет сайтов
						@endforelse
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
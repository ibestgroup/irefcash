<div class="container">

    <div class="row">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <style>
            .mark-info h3,
            .rounds h3 {
                margin-top: 0;
                text-align: center;
                border-bottom: 1px solid #eee;
            }
            .mark-info p,
            .rounds p {
                text-align: center;
            }
            .mark-info > div:nth-child(2),
            .rounds > div:nth-child(2) {
                border-right: 1px solid #eee;
                border-left: 1px solid #eee;
            }
            span {
                border-radius: 5px;
            }
        </style>
        <div class="col-lg-10 col-md-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading header-text">Транзакции по тарифу</div>
                        <div class="panel-body">
                            <div class="row" style="text-align: center;">
                                <div class="col-lg-12" style="font-weight: bold; padding: 5px; border-bottom: 1px solid #eee;">
                                    <div class="col-lg-1">#</div>
                                    <div class="col-lg-4">От кого</div>
                                    {{--<div class="col-lg-1"><i class="fa fa-arrow-right"></i></div>--}}
                                    <div class="col-lg-4">Кому</div>
                                    <div class="col-lg-3">Сумма</div>
                                </div>
                                @forelse($paymentsOrder as $item)
                                    <div class="col-lg-12" style="padding: 5px; border-bottom: 1px solid #eee;">
                                        <div class="col-lg-1">{{$item->id}}</div>
                                        <div class="col-lg-4">{{\App\Libraries\Helpers::convertUs('login', $item->from_user)}} <i class="fa fa-arrow-right"></i></div>
                                        {{--<div class="col-lg-1"><i class="fa fa-arrow-right"></i></div>--}}
                                        <div class="col-lg-4">{{\App\Libraries\Helpers::convertUs('login', $item->to_user)}}</div>
                                        <div class="col-lg-3">{{$item->amount}} <i class="fa fa-rub"></i></div>
                                    </div>
                                @empty
                                    Нет транзакций по этому тарифу
                                @endforelse
                            </div>
                        </div>
                        <div class="col-lg-12">
                            {{$paymentsOrder->appends(request()->input())->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>
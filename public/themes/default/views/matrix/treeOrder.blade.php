<div class="container">
    <div class="row">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <style>
            .mark-info h3,
            .rounds h3 {
                margin-top: 0;
                text-align: center;
                border-bottom: 1px solid #eee;
            }
            .mark-info p,
            .rounds p {
                text-align: center;
            }
            .mark-info > div:nth-child(2),
            .rounds > div:nth-child(2) {
                border-right: 1px solid #eee;
                border-left: 1px solid #eee;
            }
            span {
                border-radius: 5px;
            }
        </style>
        <div class="col-lg-10 col-md-12">
            <div class="panel panel-default">
                {{--<div class="panel-heading header-text">Информация о круге</div>--}}
                <div class="panel-body">
                    <div class="round-mark" style="background: {{($markElement->status == 1)?'rgb(225 253 240)':'#fafbfc'}};">
                        <div class="col-lg-3 col-sm-4 col-xs-4">
                            <h4 style="font-size: 13px; padding: 0; margin: 0;">Круг <i class="fa fa-circle-notch"></i></h4>
                            <h3 style="font-size: 13px;padding: 0; margin: 0;">{{$markElement->round()}}</h3>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-xs-4">
                            <h4 style="font-size: 13px; padding: 0; margin: 0;">Оборот <i class="fa fa-money"></i></h4>
                            <h3 style="font-size: 13px; padding: 0; margin: 0;">{{$markElement->sumMoney()}} <i class="fa fa-rub"></i></h3>
                        </div>
                        <div class="col-lg-5 col-sm-4 col-xs-4">
                            <h4 style="font-size: 13px; padding: 0; margin: 0;">Активаций <i class="fa fa-users"></i></h4>
                            <h3 style="font-size: 13px; padding: 0; margin: 0;">{{$markElement->activateCount()-1}}</h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-10 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading header-text">Структура тарифа <button class="js-scale-resize btn btn-success" data-action="minus"><i class="fa fa-search-minus"></i></button> <button class="js-scale-resize btn btn-success" data-action="plus"><i class="fa fa-search-plus"></i></button></div>
                <div class="panel-body">
                    <h1>{{($tarifStart)?'Общая структура тарифа':''}}</h1>
                    <div class="row" style="overflow: scroll;">
                        {!!  \App\Helpers\MarksHelper::createTree($id) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading header-text">Транзакции по тарифу (<a href="{{route('treeTransaction', ['id' => $id])}}">все</a>)</div>
                        <div class="panel-body">
                            <div class="row" style="text-align: center;">
                                <div class="col-lg-12" style="font-weight: bold; padding: 5px; border-bottom: 1px solid #eee;">
                                    <div class="col-lg-1">#</div>
                                    <div class="col-lg-4">От кого</div>
                                    {{--<div class="col-lg-1"><i class="fa fa-arrow-right"></i></div>--}}
                                    <div class="col-lg-4">Кому</div>
                                    <div class="col-lg-3">Сумма</div>
                                </div>
                                @forelse($paymentsOrder as $item)
                                    <div class="col-lg-12" style="padding: 5px; border-bottom: 1px solid #eee;">
                                        <div class="col-lg-1">{{$item->id}}</div>
                                        <div class="col-lg-4">{{\App\Libraries\Helpers::convertUs('login', $item->from_user)}} <i class="fa fa-arrow-right"></i></div>
                                        {{--<div class="col-lg-1"><i class="fa fa-arrow-right"></i></div>--}}
                                        <div class="col-lg-4">{{\App\Libraries\Helpers::convertUs('login', $item->to_user)}}</div>
                                        <div class="col-lg-3">{{$item->amount}} <i class="fa fa-rub"></i></div>
                                    </div>
                                @empty
                                    Нет транзакций по этому тарифу
                                @endforelse
                            </div>
                        </div>
                        <div class="col-lg-12">
                            {{$paymentsOrder->appends(request()->input())->links()}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading header-text">Последние активации</div>

                        <div class="panel-body">
                            <div class="row" style="text-align: center;">
                                <div class="col-lg-12" style="font-weight: bold; padding: 5px; border-bottom: 1px solid #eee; text-align: center;">
                                    {{--<div class="col-lg-1">#</div>--}}
                                    <div class="col-lg-5">Логин</div>
                                    <div class="col-lg-2"><i class="fa fa-arrow-right"></i></div>
                                    <div class="col-lg-5">Партнер</div>
                                </div>
                                @forelse($lastOrders as $item)
                                    <div class="col-lg-12" style="padding: 5px; border-bottom: 1px solid #eee;">
                                        <div class="col-lg-5">{{\App\Libraries\Helpers::convertUs('login', $item->id_user)}}</div>
                                        <div class="col-lg-2"><i class="fa fa-arrow-right"></i></div>
                                        <div class="col-lg-5">{{\App\Libraries\Helpers::convertUs('login', $item->parent)}}</div>
                                    </div>
                                @empty
                                    Нет транзакций по этому тарифу
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.js-scale-resize').on('click', function () {
                let matrixRegex = /matrix\((-?\d*\.?\d+),\s*0,\s*0,\s*(-?\d*\.?\d+),\s*0,\s*0\)/,
                    // matches = $('.js-transform-scale').css('-webkit-transform').match(matrixRegex)[1];
                    matches = $('.tf-tree').css('-webkit-transform').match(matrixRegex);
                // let scale = Number(matches);
                if (!matches) {
                    matches = 1;
                } else {
                    matches = matches[1];
                }
                let resultScale = Number(matches);


                if ($(this).data('action') == 'plus') {
                    if (resultScale < 10) resultScale += 0.3;
                } else {
                    if (resultScale > 0.3) resultScale -= 0.3;
                }

                // $('.js-transform-scale').css({'transform': 'scale(' + resultScale + ')','-webkit-transform': 'scale(' + resultScale + ')',});
                $('.tf-tree').css({'transform': 'scale(' + resultScale + ')','-webkit-transform': 'scale(' + resultScale + ')',});

                // $('.tf-tree.example').css('height', $('.js-transform-scale').height() + 100 + 'px' );
                // let q = $('.tf-tree.example').height();
                // let h = $('.js-transform-scale').height() + 100 + 'px';
                // console.log(q,h);

            });
        });
    </script>


</div>
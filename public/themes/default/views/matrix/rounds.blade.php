<div class="container">
    <div class="row">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <style>
            .mark-info h3,
            .rounds h3 {
                margin-top: 0;
                text-align: center;
                border-bottom: 1px solid #eee;
            }
            .mark-info p,
            .rounds p {
                text-align: center;
            }
            .mark-info > div:nth-child(2),
            .rounds > div:nth-child(2) {
                border-right: 1px solid #eee;
                border-left: 1px solid #eee;
            }
            span {
                border-radius: 5px;
            }
        </style>
        <div class="col-lg-10">
        @foreach($markElements as $item)
            <div class="col-lg-4 col-md-4">
                <div class="panel panel-default">
                    {{--<div class="panel-heading header-text">Информация о круге</div>--}}
                    <div class="panel-body">
                        <div class="round-mark">
                            <div class="col-lg-3 col-sm-4 col-xs-4">
                                <h4 style="font-size: 13px; padding: 0; margin: 0;">Круг <i class="fa fa-circle-notch"></i></h4>
                                <h3 style="font-size: 13px;padding: 0; margin: 0;">{{$item->round()}}</h3>
                            </div>
                            <div class="col-lg-4 col-sm-4 col-xs-4">
                                <h4 style="font-size: 13px; padding: 0; margin: 0;">Оборот <i class="fa fa-money"></i></h4>
                                <h3 style="font-size: 13px; padding: 0; margin: 0;">{{$item->sumMoney()}} <i class="fa fa-rub"></i></h3>
                            </div>
                            <div class="col-lg-5 col-sm-4 col-xs-4">
                                <h4 style="font-size: 13px; padding: 0; margin: 0;">Активаций <i class="fa fa-users"></i></h4>
                                <h3 style="font-size: 13px; padding: 0; margin: 0;">{{$item->activateCount()-1}}</h3>
                            </div>
                            <div class="clearfix"></div>

                            <a class="btn btn-info" href="/matrix/tree/{{$item->orderAdmin['id']}}">Дерево</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>
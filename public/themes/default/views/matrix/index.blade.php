<div class="container">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <div class="row">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <style>
            .mark-info h3,
            .rounds h3 {
                margin-top: 0;
                text-align: center;
                border-bottom: 1px solid #eee;
            }
            .mark-info p,
            .rounds p {
                text-align: center;
            }
            .mark-info > div:nth-child(2) {
                border-right: 1px solid #eee;
                border-left: 1px solid #eee;
            }
        </style>
        <div class="col-lg-10 col-md-12">
        @forelse($marksArray as $item)
            <div class="panel panel-default">
                <div class="panel-heading header-text">{{$item->name}}</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12 mark-info mhc">
                            <div class="col-lg-4 mhe">
                                <h3>Основные</h3>
                                <div class="row mark-info-table">
                                    <div class="col-lg-4 col-sm-4 col-xs-4" title="Ширина структуры тарифа">
                                        <div class="col-lg-12 mark-main-info">
                                            <h4><i class="fas fa-arrows-alt-h"></i> Ширина</h4>
                                            <p>{{$item->width}}</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-4" title="Тип тарифа. Мульти - все уровни покупаются одновременно. Линейный - уровни покупаются поочередно.">
                                        <div class="col-lg-12 mark-main-info">
                                            <h4><i class="fas fa-cogs"></i> Тип</h4>
                                            <p>{{($item->type == 1)?'Линейный':'Мульти'}}</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-4" title="Количество уровней в тарифе">
                                        <div class="col-lg-12 mark-main-info">
                                            <h4><i class="fas fa-signal" style="transform: rotate(90deg)"></i> Уровни</h4>
                                            <p>{{$item->level}}</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-4" title="Общая сумма входа в тариф">
                                        <div class="col-lg-12 mark-main-info">
                                            <h4><i class="fa fa-money"></i> Вход</h4>
                                            <p>{{\App\Helpers\MarksHelper::markEnter($item->id)}}<i class="fa fa-rub"></i></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-4" title="Отчисления партнеру с активации тарифа">
                                        <div class="col-lg-12 mark-main-info">
                                            <h4><i class="fa fa-user-plus"></i> Партнеру</h4>
                                            <p>{{$item->levelInfo->to_par_1}}<i class="fa fa-rub"></i></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-4" title="Сколько последних участников будет активировано в начале следующего круга бесплатно. (Если всего активаций было 10 или больше)">
                                        <div class="col-lg-12 mark-main-info">
                                            <h4><i class="fa fa-users"></i> Дальше</h4>
                                            <p>{{$item->body_next}}</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-4" title="Какой по счету круг активен сейчас.">
                                        <div class="col-lg-12 mark-main-info" style="cursor: pointer;" onclick="window.location.href = '{{route('rounds', ['id' => $item->id])}}'">
                                            <h4><i class="fas fa-circle-notch"></i> Круг</h4>
                                            <p>{{$item->round()}}</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-4" title="Длительность/Ожидание. Длительность - время действия круга с момента его начала.
                                                                                    Ожидание - количество времени до запуска следующего круга.">
                                        <div class="col-lg-12 mark-main-info">
                                            <h4><i class="fa fa-clock"></i> Время</h4>
                                            <p>{{\App\Libraries\Helpers::secToArray($item->length)}}/{{\App\Libraries\Helpers::secToArray($item->interval-$item->length)}}</p>
                                        </div>
                                    </div>
                                </div>

                                <h3 title="Буст - это усиление заработка на вашем аккаунте. Бусты не складываются, берется буст с наибольшим значением по определенной позиции. Буст активен до конца текущего круга.">Буст <i class="fa fa-bolt"></i></h3>
                                <div class="row mark-info-table">
                                    @if ($item->boost_surfing)
                                        <div class="col-lg-4 col-sm-4 col-xs-4" title="Заработок в серфинге будет увеличен на {{$item->boost_surfing}}% пока тариф активен.">
                                            <div class="col-lg-12 mark-main-info" style="background: #fff5db; border-color: #ffeb79;">
                                                <h4 style="color: #d2b20d;"><i class="far fa-window-restore"></i> Серфинг</h4>
                                                <p style="color: #44dca5; font-size: 19px; font-weight: bold;">+{{$item->boost_surfing}}%</p>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($item->boost_interview)
                                        <div class="col-lg-4 col-sm-4 col-xs-4" title="Заработок на опросах будет увеличен на {{$item->boost_interview}}% пока тариф активен.">
                                            <div class="col-lg-12 mark-main-info" style="background: #fff5db; border-color: #ffeb79;">
                                                <h4 style="color: #d2b20d;"><i class="fas fa-question-circle"></i> Опросы</h4>
                                                <p style="color: #44dca5; font-size: 19px; font-weight: bold;">+{{$item->boost_interview}}%</p>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($item->boost_link)
                                        <div class="col-lg-4 col-sm-4 col-xs-4" title="Заработок в коротких ссылках будет увеличен на {{$item->boost_link}}% пока тариф активен.">
                                            <div class="col-lg-12 mark-main-info" style="background: #fff5db; border-color: #ffeb79;">
                                                <h4 style="color: #d2b20d;"><i class="far fa-window-restore"></i> Ссылки</h4>
                                                <p style="color: #44dca5; font-size: 19px; font-weight: bold;">+{{$item->boost_link}}%</p>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($item->boost_surfing_aff)
                                        <div class="col-lg-4 col-sm-4 col-xs-4" title="Заработок с рефералов в серфинге будет увеличен на {{$item->boost_surfing_aff}}% пока тариф активен.">
                                            <div class="col-lg-12 mark-main-info" style="background: #fff5db; border-color: #ffeb79;">
                                                <h4 style="color: #d2b20d;"><i class="far fa-window-restore"></i> Серфинг (реф)</h4>
                                                <p style="color: #44dca5; font-size: 19px; font-weight: bold;">+{{$item->boost_surfing_aff}}%</p>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($item->boost_interview_aff)
                                        <div class="col-lg-4 col-sm-4 col-xs-4" title="Заработок с рефералов на опросах будет увеличен на {{$item->boost_interview_aff}}% пока тариф активен.">
                                            <div class="col-lg-12 mark-main-info" style="background: #fff5db; border-color: #ffeb79;">
                                                <h4 style="color: #d2b20d;"><i class="fas fa-question-circle"></i> Опросы (реф)</h4>
                                                <p style="color: #44dca5; font-size: 19px; font-weight: bold;">+{{$item->boost_interview_aff}}%</p>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($item->boost_link_aff)
                                        <div class="col-lg-4 col-sm-4 col-xs-4" title="Заработок с рефералов в коротких ссылках будет увеличен на {{$item->boost_link_aff}}% пока тариф активен.">
                                            <div class="col-lg-12 mark-main-info" style="background: #fff5db; border-color: #ffeb79;">
                                                <h4 style="color: #d2b20d;"><i class="far fa-window-restore"></i> Ссылки (реф)</h4>
                                                <p style="color: #44dca5; font-size: 19px; font-weight: bold;">+{{$item->boost_link_aff}}%</p>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                {{--<p> </p>--}}
                                {{--<p> </p>--}}
                                {{--<p> </p>--}}
                                {{--<p> </p>--}}
                                {{--<p title="Сколько последних активированных попадут бесплатно в следующий круг"> </p>--}}
                                {{--<p data-original-title="Номер круга, который идет сейчас" data-toggle="tooltip"> </p>--}}
                            </div>
                            <div class="col-lg-4 mhe">
                                <h3>Уровни</h3>
                                <div class="row">
                                    <div class="col-lg-1 col-sm-1 col-xs-1">
                                        <p>#</p>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-4">
                                        <p>Цена</p>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-xs-3">
                                        <p>Человек</p>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-4">
                                        <p>Доход</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                @for($i = 1; $i <= $item->level; $i++)
                                    @if($item->{'level'.$i} != 0 or $item->levelInfo->{'to_par_'.$i} != 0)
                                    <div class="row">
                                        <div class="col-lg-1 col-sm-1 col-xs-1">
                                            <p>{{$i}}</p>
                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-xs-4">
                                            <p>
                                                {!! ($item->{'level'.$i} != 0)?$item->{'level'.$i}.'<i class="fa fa-rub"></i>':'---' !!}
                                            </p>
                                        </div>
                                        <div class="col-lg-3 col-sm-3 col-xs-3">
                                            <p>
                                                {!! pow($item->width, $i) !!}<i class="fa fa-users"></i>
                                            </p>
                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-xs-4">
                                            <p>
                                                {{ \App\Helpers\MarksHelper::profitLevel($item->id, $i) }}<i class="fa fa-rub"></i>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    @endif
                                @endfor
                                <div class="row">
                                    <div class="col-lg-1 col-sm-1 col-xs-1">
                                        <p>Итого</p>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-4">
                                        <p></p>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-xs-3">
                                        <p>
                                            <span style="color: #44dca5; font-size: 19px;">
                                                {{\App\Helpers\MarksHelper::markPeople($item->id)}} <span class="fa fa-users"></span>
                                            </span>
                                        </p>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-4">
                                        <p>
                                            <span style="color: #44dca5; font-size: 19px;">
                                                {{\App\Helpers\MarksHelper::markProfit($item->id)}} <span class="fa fa-rub"></span>
                                            </span>
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>




                            </div>
                            <div class="col-lg-4 mhe">
                                <h3>Круги</h3>
                                <div class="col-lg-12 rounds" style="margin-top: 10px;">
                                    {{--{{dd($item->lastMarks())}}--}}
                                    @foreach($item->lastMarks() as $mark)
                                        <div class="col-lg-12" style="padding: 0 10px; margin-top: 10px;">
                                            {{--<div style="background: linear-gradient(to top right, #c6e7ff 37%, #1d94eb 81%); text-align: center; border-radius: 3px;">--}}
                                            <div class="round-mark" style="background: {{($mark->status == 1)?'rgb(225 253 240)':'#fafbfc'}};">
                                                {{--<h4 style="margin: 0; padding-top: 10px; font-size: 16px; /*color: #fff;*/"></h4>--}}
                                                <div class="col-lg-3 col-sm-4 col-xs-4">
                                                    <h4 style="font-size: 13px; padding: 0; margin: 0;">Круг <i class="fa fa-circle-notch"></i></h4>
                                                    <h3 style="font-size: 13px;padding: 0; margin: 0;">{{$mark->round()}}</h3>
                                                </div>
                                                <div class="col-lg-4 col-sm-4 col-xs-4">
                                                    <h4 style="font-size: 13px; padding: 0; margin: 0;">Оборот <i class="fa fa-money"></i></h4>
                                                    <h3 style="font-size: 13px; padding: 0; margin: 0;">{{$mark->sumMoney()}} <i class="fa fa-rub"></i></h3>
                                                </div>
                                                <div class="col-lg-5 col-sm-4 col-xs-4">
                                                    <h4 style="font-size: 13px; padding: 0; margin: 0;">Активаций <i class="fa fa-users"></i></h4>
                                                    <h3 style="font-size: 13px; padding: 0; margin: 0;">{{$mark->activateCount()-1}}</h3>
                                                </div>

                                                <h4 style="{{($mark->status == 1)?'font-weight:bold!important;':''}}">
                                                    <div class="col-lg-12">
                                                        @if ($mark->status == 2)
{{--                                                            <span>{{$mark->end}}</span>--}}

                                                            <div class="progress-bar-container">
                                                                <div class="progress-bar stripes animated reverse slower">
                                                                    <span class="clock-static">Круг завершен</span>
                                                                    <span class="progress-bar-inner" style="width: 100%; background: #f39c12;"></span>
                                                                </div>
                                                            </div>
                                                            <a class="btn btn-info" href="/matrix/tree/{{$mark->orderAdmin['id']}}">Дерево</a>
                                                        @elseif ($mark->status == 1)
                                                            {{--Круг идет <br>--}}
                                                            @php
                                                                $nowTime = time();
                                                                $length = $mark->mark->length;
                                                                $diffTime = $nowTime - strtotime($mark->start);
                                                                $resultWidth = $diffTime/$length*100;
                                                            @endphp

                                                            <div class="progress-bar-container">
                                                                <div class="progress-bar stripes animated reverse slower">
                                                                    <span class="clock"> {{$mark->end}}</span>
                                                                    <span class="progress-bar-inner" style="width: {{$resultWidth}}%; background: #64c787;"></span>
                                                                </div>
                                                            </div>
                                                            {{--<span class="clock"> {{$mark->end}}</span> <br>--}}
                                                            <a href="#" class="btn btn-success js-activate" data-id="{{$item->id}}">
                                                                {{ \App\Helpers\MarksHelper::calcMarkPrice($item->id) }} <i class="fa fa-rub"></i>
                                                            </a>
                                                            <a href="#" class="btn btn-success js-activate" data-id="{{$item->id}}" data-type="point">
                                                                {{ \App\Helpers\MarksHelper::calcMarkPrice($item->id)*100 }} <img src="{!! url('icon/key.png') !!}" width="17px" height="17px" alt="">
                                                            </a>
                                                            {{--<a href="#" class="btn btn-success js-activate" data-id="{{$item->id}}">Активировать</a>--}}
                                                            <a class="btn btn-info" href="/matrix/tree/{{$mark->orderAdmin['id']}}">Дерево</a>
                                                        @else
                                                            {{--Круг начнется <br>--}}

                                                            @php
                                                                $nowTime = time();
                                                                $prevEnd = strtotime($mark->end) - $mark->mark->interval*2;
                                                                $length = $mark->mark->interval*2;
                                                                $diffTime = $nowTime - $prevEnd;
                                                                $resultWidth = $diffTime/$length*100;
                                                            @endphp
                                                            {{--<span class="clock"> {{$mark->start}}</span>--}}
                                                            <div class="progress-bar-container">
                                                                <div class="progress-bar stripes animated reverse slower">
                                                                    <span class="clock"> {{$mark->start}}</span>
                                                                    <span class="progress-bar-inner" style="width: {{$resultWidth}}%"></span>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </h4>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        {{--<div class="col-lg-12 rounds mhc" style="margin-top: 10px;">--}}
                            {{--{{dd($item->lastMarks())}}--}}
                            {{--@foreach($item->lastMarks() as $mark)--}}
                                {{--<div class="col-lg-4" style="padding: 0 10px; margin-top: 10px;">--}}
                                    {{--<div style="background: linear-gradient(to top right, #c6e7ff 37%, #1d94eb 81%); text-align: center; border-radius: 3px;">--}}
                                    {{--<div class="round-mark mhe" style="background: {{($mark->status == 1)?'rgb(225 253 240)':'#fafbfc'}};">--}}
                                        {{--<h4 style="margin: 0; padding-top: 10px; font-size: 16px; /*color: #fff;*/">Круг #{{$mark->round()}}</h4>--}}
                                        {{--<div class="col-lg-6 col-sm-6 col-xs-6">--}}
                                            {{--<h4 style="">Оборот <i class="fa fa-money"></i></h4>--}}
                                            {{--<h3 style="">{{$mark->sumMoney()}} <i class="fa fa-rub"></i></h3>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-lg-6 col-sm-6 col-xs-6">--}}
                                            {{--<h4 style="">Активаций <i class="fa fa-users"></i></h4>--}}
                                            {{--<h3 style="">{{$mark->activateCount()}}</h3>--}}
                                        {{--</div>--}}

                                        {{--<h4 style="{{($mark->status == 1)?'font-weight:bold!important;':''}}">--}}
                                            {{--<div class="col-lg-12">--}}
                                            {{--@if ($mark->status == 2)--}}
                                                {{--<span>{{$mark->end}}</span>--}}

                                                    {{--<div class="progress-bar-container">--}}
                                                        {{--<div class="progress-bar stripes animated reverse slower">--}}
                                                            {{--<span class="clock-static">Круг завершен</span>--}}
                                                            {{--<span class="progress-bar-inner" style="width: 100%; background: #f39c12;"></span>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--<a class="btn btn-info" href="/matrix/tree/{{$mark->orderAdmin['id']}}">Дерево</a>--}}
                                            {{--@elseif ($mark->status == 1)--}}
                                                {{--Круг идет <br>--}}
                                                {{--@php--}}
                                                    {{--$nowTime = time();--}}
                                                    {{--$length = $mark->mark->length;--}}
                                                    {{--$diffTime = $nowTime - strtotime($mark->start);--}}
                                                    {{--$resultWidth = $diffTime/$length*100;--}}
                                                {{--@endphp--}}

                                                    {{--<div class="progress-bar-container">--}}
                                                        {{--<div class="progress-bar stripes animated reverse slower">--}}
                                                            {{--<span class="clock"> {{$mark->end}}</span>--}}
                                                            {{--<span class="progress-bar-inner" style="width: {{$resultWidth}}%; background: #64c787;"></span>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--<span class="clock"> {{$mark->end}}</span> <br>--}}
                                                {{--<a href="#" class="btn btn-success js-activate" data-id="{{$item->id}}">Активировать</a>--}}
                                                {{--<a class="btn btn-info" href="/matrix/tree/{{$mark->orderAdmin['id']}}">Дерево</a>--}}
                                            {{--@else--}}
                                                {{--Круг начнется <br>--}}

                                                    {{--@php--}}
                                                        {{--$nowTime = time();--}}
                                                        {{--$prevEnd = strtotime($mark->end) - $mark->mark->interval*2;--}}
                                                        {{--$length = $mark->mark->interval*2;--}}
                                                        {{--$diffTime = $nowTime - $prevEnd;--}}
                                                        {{--$resultWidth = $diffTime/$length*100;--}}
                                                    {{--@endphp--}}
                                                {{--<span class="clock"> {{$mark->start}}</span>--}}
                                                {{--<div class="progress-bar-container">--}}
                                                    {{--<div class="progress-bar stripes animated reverse slower">--}}
                                                        {{--<span class="clock"> {{$mark->start}}</span>--}}
                                                        {{--<span class="progress-bar-inner" style="width: {{$resultWidth}}%"></span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--@endif--}}
                                                {{--<div class="clearfix"></div>--}}
                                            {{--</div>--}}
                                        {{--</h4>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        @empty
            <div class="panel panel-default">
                <div class="panel-heading header-text">Нет тарифов</div>
                <div class="panel-body">
                    По вашему запросу не найдено ни одного тарифа
                </div>
            </div>
        @endforelse
        </div>
    </div>
    <script>
        $(document).tooltip({
            tooltipClass: 'my-tooltip',
            // track: true,
            show: {
                effect: 'fadeIn',
                duration: 400,
            },
            hide: {
                // effect: 'fadeOut',
                duration: 0,
            },
            position: {
                my: "right center"
            }
        });
    </script>

</div>
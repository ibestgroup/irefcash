<div class="container">
@php
@endphp
    <div class="row">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <style>
            .mark-info h3,
            .rounds h3 {
                margin-top: 0;
                text-align: center;
                border-bottom: 1px solid #eee;
            }
            .mark-info p,
            .rounds p {
                text-align: center;
            }
            .mark-info > div:nth-child(2),
            .rounds > div:nth-child(2) {
                border-right: 1px solid #eee;
                border-left: 1px solid #eee;
            }
        </style>
        <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
        <div class="col-lg-10 col-md-12">
{{--        @forelse($marksArray as $item)--}}
            <div class="panel panel-default">
                <div class="panel-heading header-text">Создать опрос</div>
                <div class="panel-body">

                    <form method="post" action="{{ route('addInterviewAjax') }}" class="js-store" data-refresh=".container" data-location="{{ route('myInterview') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">
                                Название
                            </label>
                            <input type="text" name="name" class="form-control" placeholder="Название">
                        </div>
                        <div class="form-group">
                            <label for="">
                                Описание
                            </label>
                            <textarea type="text" name="desc" class="form-control" placeholder="Описание" style="resize: vertical;"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">
                                 Ссылка на сайт (оставьте пустой если задание из поисковика)
                            </label>
                            <input type="text" name="link" class="form-control" placeholder="Ссылка на сайт">
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="row">
                                        <h3>Тест</h3>
                                        @for($i = 1; $i <= 5; $i++)
                                        <div class="row" style="border-bottom: 1px solid #eee; padding-bottom: 25px; margin-bottom: 25px;">
                                            <div class="col-lg-12">
                                                <div class="col-lg-6">
                                                    <label for="">
                                                        Вопрос {{$i}}
                                                    </label>
                                                    <input type="text" class="form-control" name="asks[{{$i}}][ask]">
                                                </div>
                                                <div class="col-lg-6">
                                                    <label>Правильный ответ</label>
                                                    <select name="asks[{{$i}}][answer_true]" id="" class="form-control">
                                                        <option value="1">Ответ 1</option>
                                                        <option value="2">Ответ 2</option>
                                                        <option value="3">Ответ 3</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Ответ 1</label>
                                                    <input type="text" name="asks[{{$i}}][answer_1]" class="form-control" placeholder="Ответ 1">
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Ответ 2</label>
                                                    <input type="text" name="asks[{{$i}}][answer_2]" class="form-control" placeholder="Ответ 2">
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Ответ 3</label>
                                                    <input type="text" name="asks[{{$i}}][answer_3]" class="form-control" placeholder="Ответ 3">
                                                </div>
                                            </div>
                                        </div>
                                        @endfor
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">
                                        Выделить задание
                                    </label>
                                    <select name="vip" id="" class="form-control">
                                        <option value="0">Нет</option>
                                        <option value="1">Да (+{{$surfInfo->vip}})</option>
                                        <option value="2">Да вверху (+{{$surfInfo->vip_2}})</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">
                                        Цена выполнения (мин. 0.3<i class="fa fa-rub"></i>)
                                    </label>
                                    <input type="number" step="0.0001" class="form-control" name="price" placeholder="Цена за выполнение" value="0.5">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-success">Создать задание</button>
                    </form>

                </div>
            </div>
            <script>
                CKEDITOR.replace( 'desc' );
            </script>
        {{--@empty--}}
        {{--@endforelse--}}
        </div>
    </div>


</div>
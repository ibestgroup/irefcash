<div class="container">
@php
@endphp
    <div class="row">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <style>
            .mark-info h3,
            .rounds h3 {
                margin-top: 0;
                text-align: center;
                border-bottom: 1px solid #eee;
            }
            .mark-info p,
            .rounds p {
                text-align: center;
            }
            .mark-info > div:nth-child(2),
            .rounds > div:nth-child(2) {
                border-right: 1px solid #eee;
                border-left: 1px solid #eee;
            }
        </style>

        <div class="col-lg-10 col-md-12">
            @if ($message)
                {{$message}}
            @else
                <div class="panel panel-default">
                    <div class="panel-heading header-text">Опрос {{$interview->name}} (#{{$interview->id}})</div>
                    <div class="panel-body text-center">
                        <div class="col-lg-2">
                            <img src="{{$interview->author()->avatar}}" style="max-width: 150px; max-height: 150px; border-radius: 50%;" alt="">
                        </div>
                        <div class="col-lg-5">
                            <p>Автор задания: <a href="/{{$interview->author()->username}}">{{$interview->author()->login}}</a></p>
                            <p>
                            <div class="col-lg-2" style="text-align: center; display: inline-block; vertical-align: middle; padding-top: 15px;">
                                {{\App\Libraries\Helpers::getRang($interview->user_id)}} УР.
                            </div>
                            <div class="col-lg-10">
                                {!! \App\Libraries\Helpers::getRang($interview->user_id, 'progressbar') !!}
                                Всего выполнений: {{$interview->allCount()}}
                                <br>
                                <span style="color: #55da91;">Выполнено: {{$interview->completed()}} </span>/
                                <span style="color: #daab30;"> В процессе: {{$interview->inProgress()}} </span>/
                                <span style="color: #da5555;"> Провалено: {{$interview->denied()}}</span>
                            </div>
                            </p>
                        </div>
                        <div class="col-lg-5">
                            <p>Награда:</p>
                            <h3>{{$interview->reward()}} <i class="fa fa-rub"></i></h3>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading header-text">Описание</div>
                    <div class="panel-body">
                        <p>{!! $interview->desc !!}</p>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading header-text">Тест</div>
                    <div class="panel-body" style="position: relative;" id="interview-test">
                        @if ($interview->checkComplete())
                            @if ($interview->checkComplete()->status == 1)
                                <span>Тест уже выполнен</span>
                            @elseif ($interview->checkComplete()->status == 2)
                                <span>Вы прошли этот тест неудачно</span>
                            @elseif ($interview->checkComplete()->status == 0)
                                <span class="timer">{{$interview->checkComplete()->endTime()}}</span>
                                <form action="{{ route('getRewardInterview') }}" method="post" class="js-store" data-refresh="#interview-test">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="interview_id" value="{{$interview->id}}">
                                    @foreach($interview->asks as $key => $item)
                                        <div class="col-lg-12">
                                            <h3>{{$item->ask}}</h3>
                                            <input type="hidden" name="pair[{{$key}}][ask]" value="{{$item->id}}">
                                            <input type="radio" name="pair[{{$key}}][answer]" value="1">{{$item->answer_1}}
                                            <br>
                                            <input type="radio" name="pair[{{$key}}][answer]" value="2">{{$item->answer_2}}
                                            <br>
                                            <input type="radio" name="pair[{{$key}}][answer]" value="3">{{$item->answer_3}}
                                            <br>
                                        </div>
                                    @endforeach
                                    <button class="btn btn-success">Ответить</button>
                                </form>
                            @endif
                        @else
                            <form action="{{ route('startInterviewAjax') }}" method="post" class="js-store" data-refresh="#interview-test">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$interview->id}}">
                                <button class="btn btn-success">Начать выполнение теста</button>
                            </form>
                        @endif
                    </div>
                </div>
                <script>
                    $('.timer').each(function(e,el) {
                        let time = $(el).text();
                        $(el).countdown(time, function(event) {
                            $(this).html(event.strftime('%M мин %S сек'));
                        });
                    });
                </script>
            @endif
        </div>
    </div>
</div>
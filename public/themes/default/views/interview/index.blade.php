<div class="container">
@php
@endphp
    <div class="row">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <style>
            .mark-info h3,
            .rounds h3 {
                margin-top: 0;
                text-align: center;
                border-bottom: 1px solid #eee;
            }
            .mark-info p,
            .rounds p {
                text-align: center;
            }
            .mark-info > div:nth-child(2),
            .rounds > div:nth-child(2) {
                border-right: 1px solid #eee;
                border-left: 1px solid #eee;
            }
        </style>
        <div class="col-lg-10 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading header-text">Список заданий</div>
                <div class="panel-body">
                    @if ($activeTest)
                        <div class="col-lg-12">
                            <div class="surfing-link {!! $activeTest->vipClass() !!}" style="background: #fff5a7; border: 1px solid #ece080;">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <p style="color: red">Вы проходите этот тест в данный момент</p>
                                        <h3>
                                            <img src="https://www.google.com/s2/favicons?domain={{$activeTest->link}}" alt="">
                                            <a href="{{ route('viewInterview', ['id' => $activeTest->id])}}">{{$activeTest->name}}</a>
                                            <span>(#{{$activeTest->id}}) <a href="/{{\App\Libraries\Helpers::convertUs('login', $activeTest->user_id)}}">{{\App\Libraries\Helpers::convertUs('login', $activeTest->user_id)}}</a></span>
                                        </h3>
                                        <p>
                                            <i class="far fa-clock"></i> Просмотр {{$activeTest->price}} сек
                                            <i class="fa fa-money"></i> {{$activeTest->price_result*($surfInfo->percent_reward/100) }} руб
                                            {!! ($activeTest->vip)?'<i class="fas fa-crown"></i> VIP':'' !!} {{($activeTest->vip > 1)?$activeTest->vip:''}}
                                        </p>
                                    </div>
                                    <div class="col-lg-3">
                                        <h3 style="padding-top: 20px;">
                                            <i class="fa fa-money"></i> {{$activeTest->price_result*($surfInfo->percent_reward/100) }} руб
                                        </h3>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    @endif
                    @forelse($interview as $item)
                        <div class="col-lg-12">
                            <div class="surfing-link {!! $item->vipClass() !!}">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <h3>
                                            <img src="https://www.google.com/s2/favicons?domain={{$item->link}}" alt="">
                                            <a href="{{ route('viewInterview', ['id' => $item->id])}}">{{$item->name}}</a>
                                            <span>(#{{$item->id}}) <a href="/{{\App\Libraries\Helpers::convertUs('login', $item->user_id)}}">{{\App\Libraries\Helpers::convertUs('login', $item->user_id)}}</a></span>
                                        </h3>
                                        <p>Количество вопросов в тесте: {{$item->asks->count()}}</p>
                                        <p>
                                            <i class="far fa-clock"></i> Просмотр {{$item->price}} сек
                                            <i class="fa fa-money"></i> {{$item->price_result*($surfInfo->percent_reward/100) }} руб
                                            {!! ($item->vip)?'<i class="fas fa-crown"></i> VIP':'' !!} {{($item->vip > 1)?$item->vip:''}}
                                        </p>
                                    </div>
                                    <div class="col-lg-3">
                                        <h3 style="padding-top: 20px;">
                                            <i class="fa fa-money"></i> {{$item->price_result*($surfInfo->percent_reward/100) }} руб
                                        </h3>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    @empty
                        нет опросов для вас
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
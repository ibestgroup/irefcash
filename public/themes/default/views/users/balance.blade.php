<div class="container">

    <div class="row">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <style>
            .balance p {
                font-size: 17px;
            }
            .balance img {
                vertical-align: text-top;
            }
        </style>
        <div class="col-lg-5 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading header-text">Баланс</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12 balance">
                            <p>Баланс: {{$balance}} <i class="fa fa-rub"></i></p>
                            <p>Баланс ключей: {{$balancePoint}} <img src="{!! url('icon/key.png') !!}" width="17px" height="17px" alt=""> (100 <img src="{!! url('icon/key.png') !!}" width="17px" height="17px" alt=""> = 1 <i class="fa fa-rub"></i>)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading header-text">Пополнить баланс</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12 balance">
                            <form method="get" action="{{ route('balance') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="">
                                        Платежная система
                                    </label>
                                    <select name="system" class="form-control" id="">
                                        <option value="1">Payeer</option>
                                        <option value="700">YMoney</option>
                                        <option value="701">QIWI</option>
                                        <option value="702">MasterCard</option>
                                        <option value="3">FreeKassa</option>
                                    </select>
                                    {{--<input type="text" name="name" class="form-control" placeholder="Название">--}}
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        Сумма
                                    </label>
                                    <input type="number" name="amount" class="form-control" placeholder="Сумма">
                                </div>
                                <button class="btn btn-success">Пополнить</button>
                            </form>
                            {{--<form action="{{ route('balance') }}" method="get">--}}
                                {{--<input type="text" name="system">--}}
                                {{--<input type="text" name="amount">--}}
                                {{--<input type="submit">--}}
                            {{--</form>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
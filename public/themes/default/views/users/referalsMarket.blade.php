<div class="container">
	<div class="row">
		<div class="col-md-12">
			{!! Theme::partial('user-header', \App\Libraries\Helpers::getInfoForUserHeader($username)) !!}
			<div class="row">

				<div class="col-md-2 visible-lg">
					{!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
				</div>

				<div class="col-md-10">
					<div class="row">
						<div class=" timeline">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading no-bg panel-settings">
										<h3 class="panel-title">
											Биржа рефералов
										</h3>
									</div>
									<div class="panel-body text-center">

										<table width="100%">
											<tr>
												<th class="text-center">Продает</th>
												<th class="text-center">Реферал</th>
												<th class="text-center">Цена</th>
												<th class="text-center">Купить</th>
											</tr>
											@forelse($referalSell as $referal)
												<tr>
													<td>{{\App\Libraries\Helpers::convertUs('login',$referal->user_sell)}}</td>
													<td>{{\App\Libraries\Helpers::convertUs('login',$referal->user_referal)}}</td>
													<td>{{$referal->price}} <i class="fa fa-rub"></i></td>
													<td>
														@if ($referal->user_sell == Auth::id())
															<button class="btn btn-danger js-sell-delete" data-user="{{$referal->id}}">Убрать с продажи</button>
														@else
															<button class="btn btn-success js-sell-buy" data-user="{{$referal->id}}">Купить</button>
														@endif
													</td>
												</tr>
											@empty
												<tr>
													<td colspan="5">На бирже рефералов пока что нет лотов</td>
												</tr>
											@endforelse
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
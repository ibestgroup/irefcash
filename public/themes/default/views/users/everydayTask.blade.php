<div class="container">
	<div class="row">
		<div class="col-md-12">
			{!! Theme::partial('user-header', \App\Libraries\Helpers::getInfoForUserHeader($username)) !!}
			<div class="row">

				<div class="col-md-2 visible-lg">
					{!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
				</div>

				<div class="col-md-10">
					{!! Theme::partial('everydayTasks') !!}
				</div>

			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			{!! Theme::partial('user-header', \App\Libraries\Helpers::getInfoForUserHeader($username)) !!}
			<div class="row">

				<div class="col-md-2 visible-lg">
					{!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
				</div>

				<div class="col-md-10">
					<div class="row">
						<div class=" timeline">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading no-bg panel-settings">
										<h3 class="panel-title">
											Активированные матрицы &#64;{{$username}}
										</h3>
									</div>
									<div class="panel-body">
										<div class="row">
											@forelse($orders as $order)
												<div class="col-lg-4">
													<div style="border: 1px solid #E6EAEE; background: #fafbfc; color: #5B6B81; text-align: center; border-radius: 3px; margin-bottom: 10px;">
														<h3 style="margin: 0; padding-top: 10px; font-size: 19px; /*color: #fff;">Тариф: {{$order->marks->name}}</h3>
														<div class="row">
															<div class="col-lg-4">
																<h4 style="margin: 0; padding-top: 10px; font-size: 17px; /*color: #fff;">Круг: {{$order->orderRound()}}</h4>
															</div>
															<div class="col-lg-8">
																<h4 style="margin: 0; padding-top: 10px; font-size: 17px; /*color: #fff;">Заработано: {{$order->balance}} <i class="fa fa-rub"></i></h4>
															</div>
														</div>
														<div class="btns-reward" style="padding: 5px 0;">
															<a class="btn-primary btn-reward reward" href="{{ route('treeItem', ['id' => $order->id]) }}">структура</a>
														</div>
													</div>
												</div>
											@empty
												<div class="col-lg-12">
													Не активировано еще ни одного тарифа
												</div>
											@endforelse
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
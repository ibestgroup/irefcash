<div class="container">
	<div class="row">
		<div class="col-md-12">
			{!! Theme::partial('user-header', \App\Libraries\Helpers::getInfoForUserHeader($username)) !!}
			<div class="row">

				<div class="col-md-2 visible-lg">
					{!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
				</div>

				<div class="col-md-10">
					<div class="row">
						<div class="timeline">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading no-bg panel-settings">
										<h3 class="panel-title">
											Участник МЛМ сайтов
										</h3>
									</div>
									<div class="panel-body">
										<div class="row text-center">
											<div class="col-lg-12 bold">
												<div class="mhc">
													<div class="col-lg-3 mhe">Сайт</div>
													<div class="col-lg-2 mhe">Оборот</div>
													<div class="col-lg-2 mhe">Логин</div>
													<div class="col-lg-2 mhe">Заработок</div>
													<div class="col-lg-3 mhe">Перевести деньги</div>
													<div class="clearfix"></div>
												</div>
											</div>
											<div class="col-lg-12">
												@forelse($sites as $site)
													<div class="mhc" style="padding: 10px 0; border-bottom: 1px solid #eee;">
														<div class="col-lg-3 mhe" style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
															<span class="vam">
																<img src="{{$site->siteLogo}}" alt="" width="30px" height="30px" style="border-radius: 50%;">
																<a href="{{$site->result_link}}">{{$site->name}}</a>
															</span>
														</div>
														<div class="col-lg-2 mhe">
															<span class="vam">{{$site->payments}}{!! $site->currency_icon !!}</span>
														</div>
														<div class="col-lg-2 mhe" style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
															<span class="vam">
																<img src="{{$site->avatar}}" alt="" width="30px" height="30px" style="border-radius: 50%;">
																{{$site->userInfo->login}}
																(#{!! $site->userInfo->id !!})
															</span>
														</div>
														<div class="col-lg-2 mhe">
															<span class="vam">{{$site->userInfo->profit}}{!! $site->currency_icon !!}</span>
														</div>
														<div class="col-lg-3 mhe">
															<form action="">
																<div class="row">
																	<div class="col-lg-6">
																		<input type="number" class="form-control" placeholder="Сумма" name="sum">
																	</div>
																	<div class="col-lg-6">
																		<button class="btn btn-success">Перевести</button>
																	</div>
																</div>
															</form>
														</div>
														<div class="clearfix"></div>
													</div>
												@empty

													нет сайтов
												@endforelse
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
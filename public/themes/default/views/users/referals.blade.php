<div class="container">
	<div class="row">
		<div class="col-md-12">
			{!! Theme::partial('user-header', \App\Libraries\Helpers::getInfoForUserHeader($username)) !!}
			<div class="row">

				<div class="col-md-2 visible-lg">
					{!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
				</div>

				<div class="col-md-10">
					<div class="row">
						<div class=" timeline">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading no-bg panel-settings">
										<h3 class="panel-title">
											Рефералы &#64;{{$username}}
										</h3>
									</div>
									<div class="panel-body">
										<div class="row text-center">
											<table width="100%">
												<tr>
													<th class="text-center">Логин</th>
													<th class="text-center" colspan="2" width="40%">Уровень</th>
													<th class="text-center">Заработано</th>
													<th class="text-center">Последняя активность</th>
													@if ($username == Auth::user()->username)
														<th class="text-center">Продать</th>
													@endif
												</tr>
												@forelse($referals as $referal)
													<tr>
														<td>{{$referal->username}}</td>
														<td>{{\App\Libraries\Helpers::getRang($referal->id)}}УР.</td>
														<td width="30%">{!! \App\Libraries\Helpers::getRang($referal->id, 'progressbar') !!}</td>
														<td>{{\App\Models\PaymentsOrder::where('to_user', $referal->id)->sum('amount')}}</td>
														<td>{{$referal->last_active ?? '---'}}</td>
														@if ($username == Auth::user()->username)
															<td>
																@if (!\App\Models\ReferalMarket::where('user_referal', $referal->id)->where('user_buy', 0)->count())
																	<button class="btn btn-success js-sell-modal" data-user="{{$referal->id}}" data-toggle="modal" data-target="#referal-sell">Продать</button>
																@else
																	<button class="btn btn-danger js-sell-delete" data-user="{{$referal->id}}">Убрать с продажи</button>
																@endif
															</td>
														@endif
													</tr>
												@empty
													<tr>
														<td colspan="5">У пользователя нет ни одного реферала</td>
													</tr>
												@endforelse
											</table>
											<div class="modal fade" id="referal-sell" tabindex="-1" role="dialog" aria-labelledby="referal-sell-label" aria-hidden="true">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title" id="referal-sell-label">Выставить реферала на продажу</h5>
															{{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
																{{--<span aria-hidden="true">&times;</span>--}}
															{{--</button>--}}
														</div>
														<div class="modal-body">
															<form style="display: inline;" action="/referalsmarket/sell" class="js-sell-referal" method="post">
																<input type="hidden" name="user_id" value="">
																<div class="col-md-6">
																	<input class="form-control" type="number" name="price" placeholder="Ценв">
																</div>
																<div class="col-md-6">
																	<button class="btn btn-success">Продать</button>
																</div>
																<div class="clearfix"></div>
															</form>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
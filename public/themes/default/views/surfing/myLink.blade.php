<div class="container">
@php
@endphp
    <div class="row">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <div class="col-lg-10 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading header-text">Мои сайты в серфинге</div>
                <div class="panel-body">
                    <div class="col-lg-12" style="padding-bottom: 10px;">
                        <a href="{{ route('addLink') }}" class="btn btn-success">Добавить сайт</a>
                    </div>
                    @forelse($surfLinks as $item)
                        <div class="col-lg-12">
                            <div class="surfing-link {!! $item->vipClass() !!}" id="surfing-link-{{$item->id}}">
                                <div class="row">
                                    <div class="col-lg-1 surfing-link-play">
                                        @if ($item->status)
                                            <i class="fa fa-2x fa-play" onclick="$('#surfing-play-{{$item->id}}').submit();" style="color:#35aa47;"></i>
                                            <br>
                                            <span>работает</span>
                                        @else
                                            <i class="fa fa-2x fa-pause" onclick="$('#surfing-play-{{$item->id}}').submit();" style="color:#f1c72b;"></i>
                                            <span>остановлено</span>
                                        @endif
                                            <form action="{{route('playLink')}}" class="js-store" id="surfing-play-{{$item->id}}" data-refresh="#surfing-link-{{$item->id}}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="link_id" value="{{$item->id}}">
                                            </form>
                                    </div>
                                    <div class="col-lg-5">
                                        <h3>
                                            <img src="https://www.google.com/s2/favicons?domain={{$item->link}}" alt="">
                                            <a href="{{$item->link}}">{{$item->name}}</a>
                                            <span>(#{{$item->id}}) <a href="/{{\App\Libraries\Helpers::convertUs('login', $item->user_id)}}">{{\App\Libraries\Helpers::convertUs('login', $item->user_id)}}</a></span>
                                        </h3>
                                        <p>{{$item->desc}}</p>
                                        <p>
                                            <i class="far fa-clock"></i> Просмотр {{$item->price}} сек
                                            <i class="fa fa-money"></i> {{$item->result_price}} руб
                                            {!! ($item->vip)?'<i class="fas fa-crown"></i> VIP':'' !!}
                                        </p>
                                    </div>
                                    <div class="col-lg-3">
                                        <h4>Баланс: {{$item->balance}} <i class="fa fa-rub"></i></h4>
                                        <form action="{{ route('depositLink') }}" class="js-store" data-refresh="#surfing-link-{{$item->id}}">
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="link_id" value="{{$item->id}}">
                                                <div class="col-lg-6">
                                                    <input type="number" class="form-control" name="amount" placeholder="Сумма">
                                                </div>
                                                <div class="col-lg-6">
                                                    <button class="btn btn-success">Пополнить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-lg-3">
                                        <canvas style="height: 70px;" id="statistic-chart-{{$item->id}}" class="get-chart" data-labels="{{\App\Libraries\Helpers::getLastNDays(7, 'Y-m-d', true)}}" data-values="{{$item->completeLastDays()}}"></canvas>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    @empty
                        нет ссылок для вас
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
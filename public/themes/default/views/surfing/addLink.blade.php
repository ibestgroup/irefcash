<div class="container">
@php
@endphp
    <div class="row">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <style>
            .mark-info h3,
            .rounds h3 {
                margin-top: 0;
                text-align: center;
                border-bottom: 1px solid #eee;
            }
            .mark-info p,
            .rounds p {
                text-align: center;
            }
            .mark-info > div:nth-child(2),
            .rounds > div:nth-child(2) {
                border-right: 1px solid #eee;
                border-left: 1px solid #eee;
            }
        </style>
        <div class="col-lg-10 col-md-12">
{{--        @forelse($marksArray as $item)--}}
            <div class="panel panel-default">
                <div class="panel-heading header-text">Создать задание</div>
                <div class="panel-body">

                    <form method="post" action="{{ route('addInterviewAjax') }}" class="js-store" data-refresh=".container" data-location="{{route('myLink')}}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">
                                Название
                            </label>
                            <input type="text" name="name" class="form-control" placeholder="Название">
                        </div>
                        <div class="form-group">
                            <label for="">
                                Описание
                            </label>
                            <input type="text" name="desc" class="form-control" placeholder="Описание">
                        </div>
                        <div class="form-group">
                            <label for="">
                                 Ссылка на сайт
                            </label>
                            <input type="text" name="link" class="form-control" placeholder="Ссылка на сайт">
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">
                                        Посещений в сутки
                                    </label>
                                    <input type="text" name="visit_day" class="form-control" placeholder="Посещений в сутки">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">
                                        Время просмотра
                                    </label>
                                    <select name="price" id="" class="form-control">
                                        @foreach(\App\Libraries\Helpers::prices() as $item)
                                            <option value="{{$item}}">{{$item}} сек {{$surfInfo->{'price_'.$item} }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">
                                        Последующий переход на сайт
                                    </label>
                                    <select name="redirect" id="" class="form-control">
                                        <option value="0">Нет</option>
                                        <option value="1">Да (+{{$surfInfo->redirect}})</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">
                                        Выделить задание
                                    </label>
                                    <select name="vip" id="" class="form-control">
                                        <option value="0">Нет</option>
                                        <option value="1">Да (+{{$surfInfo->vip}})</option>
                                        <option value="2">Да вверху (+{{$surfInfo->vip_2}})</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">
                                        Уникальные посещения
                                    </label>
                                    <select name="unique_ip" id="" class="form-control">
                                        <option value="0">Нет</option>
                                        <option value="1">Да</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-success">Создать задание</button>
                    </form>

                </div>
            </div>
        {{--@empty--}}
        {{--@endforelse--}}
        </div>
    </div>


</div>
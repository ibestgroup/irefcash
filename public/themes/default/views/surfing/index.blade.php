<div class="container">
    <div class="row">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <style>
            .mark-info h3,
            .rounds h3 {
                margin-top: 0;
                text-align: center;
                border-bottom: 1px solid #eee;
            }
            .mark-info p,
            .rounds p {
                text-align: center;
            }
            .mark-info > div:nth-child(2),
            .rounds > div:nth-child(2) {
                border-right: 1px solid #eee;
                border-left: 1px solid #eee;
            }
        </style>
        <div class="col-lg-10 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading header-text">Список заданий</div>
                <div class="panel-body">
                    @forelse($surfLinks as $item)
                        <div class="col-lg-12">
                            <div class="surfing-link {!! $item->vipClass() !!}">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <h3>
                                            <img src="https://www.google.com/s2/favicons?domain={{$item->link}}" alt="">
                                            <a href="{{ route('viewLink', ['id' => $item->id])}}" target="_blank">{{$item->name}}</a>
                                            <span>(#{{$item->id}}) <a href="/{{\App\Libraries\Helpers::convertUs('login', $item->user_id)}}">{{\App\Libraries\Helpers::convertUs('login', $item->user_id)}}</a></span>
                                        </h3>
                                        <p>{{$item->desc}}</p>
                                        <p>
                                            <i class="far fa-clock"></i> Просмотр {{$item->price}} сек
                                            <i class="fa fa-money"></i> {{$surfInfo->{'price_'.$item->price}*($surfInfo->percent_reward/100) }} руб
                                            {!! ($item->vip)?'<i class="fas fa-crown"></i> VIP':'' !!} {{($item->vip > 1)?$item->vip:''}}
                                            {!! ($item->user_id == Auth::id())?'<i class="fa fa-user"></i>Вы автор':'' !!}
                                        </p>
                                    </div>
                                    <div class="col-lg-3">
                                        <h3 style="padding-top: 20px;">
                                            <i class="fa fa-money"></i> {{$surfInfo->{'price_'.$item->price}*($surfInfo->percent_reward/100) }} руб
                                        </h3>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    @empty
                        нет ссылок для вас
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
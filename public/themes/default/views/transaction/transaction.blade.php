<div class="container">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <div class="row">
        <div class="col-md-2 visible-lg">
            {!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
        </div>
        <div class="col-lg-10 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading header-text">История операций</div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">От кого</th>
                            <th scope="col">Кому</th>
                            <th scope="col">Сумма</th>
                            <th scope="col">Тип</th>
                            <th scope="col">Подробнее</th>
                            <th scope="col">Дата</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($transaction as $item)
                                <tr>
                                    <th scope="row">{{$item->id}}</th>
                                    <td>{{\App\Libraries\Helpers::convertUs('login', $item->from_user)}}</td>
                                    <td>{{\App\Libraries\Helpers::convertUs('login', $item->to_user)}}</td>
                                    <td style="color: {!! ($item->to_user == Auth::id())?'#55da91':'#da5555' !!}">{{@$item->info()->amount}}<i class="fa fa-rub"></i></td>
                                    <td>{!! $item->transactionType() !!}</td>
                                    <td>{!! $item->transactionType('typeInfo') !!}</td>
                                    <td>{{$item->date}}</td>
                                </tr>
                            @empty
                                <tr>
                                    <th scope="row">1</th>
                                    <td>По вашему запросу не найдено ни одного тарифа</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <div style="text-align: center;">
                        {{$transaction->appends(request()->input())->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).tooltip({
            tooltipClass: 'my-tooltip',
            // track: true,
            show: {
                effect: 'fadeIn',
                duration: 400,
            },
            hide: {
                // effect: 'fadeOut',
                duration: 0,
            },
            position: {
                my: "right center"
            }
        });
    </script>

</div>
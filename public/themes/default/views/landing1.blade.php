<div class="container">
  <div class="text-center welcome-message">
    <h2>Социальная сеть по заработку</h2>
    @if (Config::get('app.env') == 'demo')
    <p>
      Check out the RTL version of Socialite: <a href="http://socialite-rtl.laravelguru.com" target="_blank">Click Here</a>
    </p>
    @endif
  </div>
  <div class="row mhc">
    <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
      <div class="panel panel-default mhe">
        <div class="panel-body nopadding">
          <div class="icon-boxes">
            <div class="application-image">
              <img src="{!! url('setting/app-icon1.png') !!}" alt="">
            </div>
            <div class="app-files">
              {!! Setting::get('home_widget_one') !!}
            </div>
          {{-- <div class="new-campaign">
            <a href="#" class="btn btn-success app-btn">Start new Campaign</a>
          </div> --}}
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
    <div class="panel panel-default mhe">
      <div class="panel-body nopadding">
        <div class="icon-boxes">
          <div class="application-image">
            <img src="{!! url('setting/app-icon2.png') !!}" alt="">
          </div>
          <div class="app-files">
            {!! Setting::get('home_widget_two') !!}
          </div>
          {{-- <div class="new-campaign">
            <a href="#" class="btn btn-success app-btn">View your Reports</a>
          </div> --}}
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
    <div class="panel panel-default mhe">
      <div class="panel-body nopadding">
        <div class="icon-boxes">
          <div class="application-image">
            <img src="{!! url('setting/app-icon3.png') !!}" alt="">
          </div>
          <div class="app-files">
            {!! Setting::get('home_widget_three') !!}
          </div>
          {{-- <div class="new-campaign">
            <a href="#" class="btn btn-success app-btn">Start Using Dashboard</a>
          </div> --}}
        </div>
      </div>
    </div>
  </div>
</div><!-- /row -->

<div class="row tpadding-20">
  <div class="col-md-7 features-list">
    <h2>{!! Setting::get('home_list_heading') !!}</h2>
    <div class="panel panel-default">
      <div class="panel-body">
        <ul class="list-unstyled">
          @if(Setting::get('home_feature_one_icon') != null || Setting::get('home_feature_one') != null)
          <li>
            <i class="fa fa-{{ Setting::get('home_feature_one_icon') }}"></i> {{ Setting::get('home_feature_one') }}
          </li>
          @endif
          @if(Setting::get('home_feature_two_icon') != null || Setting::get('home_feature_two') != null)
            <li>
              <i class="fa fa-{{ Setting::get('home_feature_two_icon') }}"></i> {{ Setting::get('home_feature_two') }}
            </li>
          @endif
          @if(Setting::get('home_feature_three_icon') != null || Setting::get('home_feature_three') != null)
            <li>
              <i class="fa fa-{{ Setting::get('home_feature_three_icon') }}"></i> {{ Setting::get('home_feature_three') }}
            </li>
          @endif
          @if(Setting::get('home_feature_four_icon') != null || Setting::get('home_feature_four') != null)
            <li>
              <i class="fa fa-{{ Setting::get('home_feature_four_icon') }}"></i> {{ Setting::get('home_feature_four') }}
            </li>
          @endif
          @if(Setting::get('home_feature_five_icon') != null || Setting::get('home_feature_five') != null)
            <li>
              <i class="fa fa-{{ Setting::get('home_feature_five_icon') }}"></i> {{ Setting::get('home_feature_five') }}
            </li>
          @endif
          @if(Setting::get('home_feature_six_icon') != null || Setting::get('home_feature_six') != null)
            <li>
              <i class="fa fa-{{ Setting::get('home_feature_six_icon') }}"></i> {{ Setting::get('home_feature_six') }}
            </li>
          @endif
          @if(Setting::get('home_feature_seven_icon') != null || Setting::get('home_feature_seven') != null)
            <li>
              <i class="fa fa-{{ Setting::get('home_feature_seven_icon') }}"></i> {{ Setting::get('home_feature_seven') }}
            </li>
          @endif
          @if(Setting::get('home_feature_eight_icon') != null || Setting::get('home_feature_eight') != null)
            <li>
              <i class="fa fa-{{ Setting::get('home_feature_eight_icon') }}"></i> {{ Setting::get('home_feature_eight') }}
            </li>
          @endif
          @if(Setting::get('home_feature_nine_icon') != null || Setting::get('home_feature_nine') != null)
            <li>
              <i class="fa fa-{{ Setting::get('home_feature_nine_icon') }}"></i> {{ Setting::get('home_feature_nine') }}
            </li>
          @endif
          @if(Setting::get('home_feature_ten_icon') != null || Setting::get('home_feature_ten') != null)
            <li>
              <i class="fa fa-{{ Setting::get('home_feature_ten_icon') }}"></i> {{ Setting::get('home_feature_ten') }}
            </li>
          @endif
        </ul>
      </div>
    </div>
  </div>
  <div class="col-md-5">

    <h2 class="register-heading">Create an Account</h2>
    <div class="panel panel-default">
      <div class="panel-body nopadding">

        <div class="login-bottom">

          <ul class="signup-errors text-danger list-unstyled"></ul>

          <form method="POST" class="signup-form" action="{{ url('/register') }}">
            {{ csrf_field() }}

            <div class="row">
              <div class="col-md-6">
                <fieldset class="form-group{{ $errors->has('affiliate') ? ' has-error' : '' }}">
                  {{ Form::label('affiliate', trans('auth.affiliate_code')) }}<i class="optional">(optional)</i>
                  @if(isset($_GET['affiliate']))
                  {{ Form::text('affiliate', $_GET['affiliate'], ['class' => 'form-control', 'id' => 'affiliate', 'disabled' =>'disabled']) }}
                  {{ Form::hidden('affiliate', $_GET['affiliate']) }}
                  @else
                  {{ Form::text('affiliate', NULL, ['class' => 'form-control', 'id' => 'affiliate', 'placeholder'=> trans('auth.affiliate_code')]) }}
                  @endif

                  @if ($errors->has('affiliate'))
                  <span class="help-block">
                    {{ $errors->first('affiliate') }}
                  </span>
                  @endif
                </fieldset>
              </div>
              <div class="col-md-6">
                <fieldset class="form-group required {{ $errors->has('email') ? ' has-error' : '' }}">
                  {{ Form::label('email', trans('auth.email_address')) }} 
                  {{ Form::text('email', NULL, ['class' => 'form-control', 'id' => 'email', 'placeholder'=> trans('auth.welcome_to')]) }}
                  @if ($errors->has('email'))
                  <span class="help-block">
                    {{ $errors->first('email') }}
                  </span>
                  @endif
                </fieldset>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                  {{ Form::label('name', trans('auth.name')) }} 
                  {{ Form::text('name', NULL, ['class' => 'form-control', 'id' => 'name', 'placeholder'=> trans('auth.name')]) }}
                  @if ($errors->has('name'))
                  <span class="help-block">
                    {{ $errors->first('name') }}
                  </span>
                  @endif
                </fieldset>
              </div>
              <div class="col-md-6">
                <fieldset class="form-group required {{ $errors->has('gender') ? ' has-error' : '' }}">
                  {{ Form::label('gender', trans('common.gender')) }} 
                  {{ Form::select('gender', array('female' => 'Female', 'male' => 'Male', 'other' => 'None'), null, ['placeholder' => trans('auth.select_gender'), 'class' => 'form-control']) }}
                  @if ($errors->has('gender'))
                  <span class="help-block">
                    {{ $errors->first('gender') }}
                  </span>
                  @endif
                </fieldset>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <fieldset class="form-group required {{ $errors->has('username') ? ' has-error' : '' }}">
                  {{ Form::label('username', trans('common.username')) }} 
                  {{ Form::text('username', NULL, ['class' => 'form-control', 'id' => 'username', 'placeholder'=> trans('common.username')]) }}
                  @if ($errors->has('username'))
                  <span class="help-block">
                    {{ $errors->first('username') }}
                  </span>
                  @endif
                </fieldset>
              </div>
              <div class="col-md-6">
                <fieldset class="form-group required {{ $errors->has('password') ? ' has-error' : '' }}">
                  {{ Form::label('password', trans('auth.password')) }} 
                  {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder'=> trans('auth.password')]) }}
                  @if ($errors->has('password'))
                  <span class="help-block">
                    {{ $errors->first('password') }}
                  </span>
                  @endif
                </fieldset>
              </div>
            </div>

            <div class="row">
              @if(Setting::get('birthday') == "on")
              <div class="col-md-6">
                <fieldset class="form-group">
                  {{ Form::label('birthday', trans('common.birthday')) }}<i class="optional">(optional)</i>
                  <div class="input-group date datepicker">
                    <span class="input-group-addon addon-left calendar-addon">
                      <span class="fa fa-calendar"></span>
                    </span>
                    {{ Form::text('birthday', NULL, ['class' => 'form-control', 'id' => 'datepicker1']) }}
                    <span class="input-group-addon addon-right angle-addon">
                      <span class="fa fa-angle-down"></span>
                    </span>
                  </div>
                </fieldset>
              </div>
              @endif

              @if(Setting::get('city') == "on")
              <div class="col-md-6">
                <fieldset class="form-group">
                  {{ Form::label('city', trans('common.current_city')) }}<i class="optional">(optional)</i>
                  {{ Form::text('city', NULL, ['class' => 'form-control', 'placeholder' => trans('common.current_city')]) }}
                </fieldset>
              </div>
              @endif   
            </div>

            <div class="row">
              @if(Setting::get('captcha') == "on")
              <div class="col-md-12">
                <fieldset class="form-group{{ $errors->has('captcha_error') ? ' has-error' : '' }}">
                  {!! app('captcha')->display() !!}
                  @if ($errors->has('captcha_error'))
                  <span class="help-block">
                    {{ $errors->first('captcha_error') }}
                  </span>
                  @endif
                </fieldset>
              </div>    
              @endif    
            </div>

            {{ Form::button(trans('auth.signup_to_dashboard'), ['type' => 'submit','class' => 'btn btn-success btn-submit']) }}
          </form>
        </div>  
        @if((env('GOOGLE_CLIENT_ID') != NULL && env('GOOGLE_CLIENT_SECRET') != NULL) ||
          (env('TWITTER_CLIENT_ID') != NULL && env('TWITTER_CLIENT_SECRET') != NULL) ||
          (env('FACEBOOK_CLIENT_ID') != NULL && env('FACEBOOK_CLIENT_SECRET') != NULL) ||
          (env('LINKEDIN_CLIENT_ID') != NULL && env('LINKEDIN_CLIENT_SECRET') != NULL) )
          <div class="divider-login">
            <div class="divider-text"> {{ trans('auth.login_via_social_networks') }}</div>
          </div>
          @endif
          <ul class="list-inline social-connect">
            @if(env('GOOGLE_CLIENT_ID') != NULL && env('GOOGLE_CLIENT_SECRET') != NULL)
            <li><a href="{{ url('google') }}" class="btn btn-social google-plus"><span class="social-circle"><i class="fa fa-google-plus" aria-hidden="true"></i></span></a></li> 
            @endif

            @if(env('TWITTER_CLIENT_ID') != NULL && env('TWITTER_CLIENT_SECRET') != NULL)
            <li><a href="{{ url('twitter') }}" class="btn btn-social tw"><span class="social-circle"><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
            @endif

            @if(env('FACEBOOK_CLIENT_ID') != NULL && env('FACEBOOK_CLIENT_SECRET') != NULL)
            <li><a href="{{ url('facebook') }}" class="btn btn-social fb"><span class="social-circle"><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
            @endif

            @if(env('LINKEDIN_CLIENT_ID') != NULL && env('LINKEDIN_CLIENT_SECRET') != NULL) 
            <li><a href="{{ url('linkedin') }}" class="btn btn-social linkedin"><span class="social-circle"><i class="fa fa-linkedin" aria-hidden="true"></i></span></a></li>
            @endif
          </ul>
        </div>
      </div><!-- /panel -->
    </div>
  <div class="clearfix"></div>
  <section>
    <ul id="UL_1" style="margin: 100px auto;">
      <li id="LI_2">
        <span class="fa fa-2x fa-users"></span> <strong id="STRONG_4">5,120</strong> Etiam
      </li>
      <li id="LI_5">
        <span class="fa fa-2x fa-users"></span> <strong id="STRONG_7">8,192</strong> Magna
      </li>
      <li id="LI_8">
        <span class="fa fa-2x fa-users"></span> <strong id="STRONG_10">2,048</strong> Tempus
      </li>
      <li id="LI_11">
        <span class="fa fa-2x fa-users"></span> <strong id="STRONG_13">4,096</strong> Aliquam
      </li>
      <li id="LI_14">
        <span class="fa fa-2x fa-users"></span> <strong id="STRONG_16">1,024</strong> Nullam
      </li>
    </ul>
  </section>
  </div><!-- /row -->
</div><!-- /container -->
{!! Theme::asset()->container('footer')->usePath()->add('app', 'js/app.js') !!}
<style>
  #UL_1 {
    block-size: 166px;
    border-block-end-color: rgb(99, 99, 99);
    border-block-start-color: rgb(99, 99, 99);
    border-inline-end-color: rgb(99, 99, 99);
    border-inline-start-color: rgb(99, 99, 99);
    box-sizing: border-box;
    caret-color: rgb(99, 99, 99);
    color: rgb(99, 99, 99);
    column-rule-color: rgb(99, 99, 99);
    cursor: default;
    display: flex;
    height: 166px;
    inline-size: 896px;
    margin-block-end: 48px;
    margin-block-start: 0px;
    padding-inline-start: 0px;
    perspective-origin: 448px 83px;
    text-align: center;
    text-decoration: none solid rgb(99, 99, 99);
    text-size-adjust: 100%;
    transform-origin: 448px 83px;
    width: 896px;
    border: 0px none rgb(99, 99, 99);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    margin: 0px 0px 48px;
    outline: rgb(99, 99, 99) none 0px;
    padding: 0px;
  }/*#UL_1*/

  #UL_1:after {
    border-block-end-color: rgb(99, 99, 99);
    border-block-start-color: rgb(99, 99, 99);
    border-inline-end-color: rgb(99, 99, 99);
    border-inline-start-color: rgb(99, 99, 99);
    box-sizing: border-box;
    caret-color: rgb(99, 99, 99);
    color: rgb(99, 99, 99);
    column-rule-color: rgb(99, 99, 99);
    cursor: default;
    display: block;
    text-align: center;
    text-decoration: none solid rgb(99, 99, 99);
    text-size-adjust: 100%;
    border: 0px none rgb(99, 99, 99);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(99, 99, 99) none 0px;
  }/*#UL_1:after*/

  #UL_1:before {
    border-block-end-color: rgb(99, 99, 99);
    border-block-start-color: rgb(99, 99, 99);
    border-inline-end-color: rgb(99, 99, 99);
    border-inline-start-color: rgb(99, 99, 99);
    box-sizing: border-box;
    caret-color: rgb(99, 99, 99);
    color: rgb(99, 99, 99);
    column-rule-color: rgb(99, 99, 99);
    cursor: default;
    display: block;
    text-align: center;
    text-decoration: none solid rgb(99, 99, 99);
    text-size-adjust: 100%;
    border: 0px none rgb(99, 99, 99);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(99, 99, 99) none 0px;
  }/*#UL_1:before*/

  #LI_2 {
    block-size: 166px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    height: 166px;
    inline-size: 179.203px;
    min-block-size: auto;
    min-height: auto;
    min-inline-size: auto;
    min-width: auto;
    padding-block-end: 24px;
    padding-block-start: 24px;
    padding-inline-end: 24px;
    padding-inline-start: 24px;
    perspective-origin: 89.5938px 83px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    transform-origin: 89.6016px 83px;
    width: 179.203px;
    background: rgb(239, 168, 176) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(255, 255, 255);
    border-radius: 8px 0px 0px 8px;
    flex: 1 1 0%;
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
    padding: 24px;
  }/*#LI_2*/

  #LI_2:after {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#LI_2:after*/

  #LI_2:before {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#LI_2:before*/

  #SPAN_3 {
    block-size: 57px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    bottom: 0px;
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    display: inline-block;
    height: 57px;
    inline-size: 33px;
    inset-block-end: 0px;
    inset-block-start: 0px;
    inset-inline-end: 0px;
    inset-inline-start: 0px;
    left: 0px;
    perspective-origin: 16.5px 28.5px;
    position: relative;
    right: 0px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    top: 0px;
    transform-origin: 16.5px 28.5px;
    width: 33px;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
    transition: background-color 0.2s ease-in-out 0s, color 0.2s ease-in-out 0s;
  }/*#SPAN_3*/

  #SPAN_3:after {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#SPAN_3:after*/

  #SPAN_3:before {
    block-size: 57px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    content: '""';
    cursor: default;
    display: inline-block;
    height: 57px;
    inline-size: 33px;
    perspective-origin: 16.5px 28.5px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    transform-origin: 16.5px 28.5px;
    width: 33px;
    border: 0px none rgb(255, 255, 255);
    font: 900 44px / 57.2px "Font Awesome 5 Free";
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#SPAN_3:before*/

  #STRONG_4, #STRONG_7, #STRONG_10, #STRONG_13, #STRONG_16 {
    block-size: 35px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    display: block;
    height: 35px;
    inline-size: 131.203px;
    letter-spacing: -0.8px;
    perspective-origin: 65.5938px 17.5px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    transform-origin: 65.6016px 17.5px;
    width: 131.203px;
    border: 0px none rgb(255, 255, 255);
    font: 300 32px / 35.2px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#STRONG_4, #STRONG_7, #STRONG_10, #STRONG_13, #STRONG_16*/

  #STRONG_4:after, #STRONG_7:after, #STRONG_10:after, #STRONG_13:after, #STRONG_16:after {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    letter-spacing: -0.8px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 32px / 35.2px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#STRONG_4:after, #STRONG_7:after, #STRONG_10:after, #STRONG_13:after, #STRONG_16:after*/

  #STRONG_4:before, #STRONG_7:before, #STRONG_10:before, #STRONG_13:before, #STRONG_16:before {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    letter-spacing: -0.8px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 32px / 35.2px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#STRONG_4:before, #STRONG_7:before, #STRONG_10:before, #STRONG_13:before, #STRONG_16:before*/

  #LI_5 {
    block-size: 166px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    height: 166px;
    inline-size: 179.203px;
    min-block-size: auto;
    min-height: auto;
    min-inline-size: auto;
    min-width: auto;
    padding-block-end: 24px;
    padding-block-start: 24px;
    padding-inline-end: 24px;
    padding-inline-start: 24px;
    perspective-origin: 89.5938px 83px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    transform-origin: 89.6016px 83px;
    width: 179.203px;
    background: rgb(199, 156, 200) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(255, 255, 255);
    flex: 1 1 0%;
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
    padding: 24px;
  }/*#LI_5*/

  #LI_5:after {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#LI_5:after*/

  #LI_5:before {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#LI_5:before*/

  #SPAN_6 {
    block-size: 57px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    bottom: 0px;
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    display: inline-block;
    height: 57px;
    inline-size: 49.5px;
    inset-block-end: 0px;
    inset-block-start: 0px;
    inset-inline-end: 0px;
    inset-inline-start: 0px;
    left: 0px;
    perspective-origin: 24.75px 28.5px;
    position: relative;
    right: 0px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    top: 0px;
    transform-origin: 24.75px 28.5px;
    width: 49.5px;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
    transition: background-color 0.2s ease-in-out 0s, color 0.2s ease-in-out 0s;
  }/*#SPAN_6*/

  #SPAN_6:after {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#SPAN_6:after*/

  #SPAN_6:before {
    block-size: 57px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    content: '""';
    cursor: default;
    display: inline-block;
    height: 57px;
    inline-size: 49.5px;
    perspective-origin: 24.75px 28.5px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    transform-origin: 24.75px 28.5px;
    width: 49.5px;
    border: 0px none rgb(255, 255, 255);
    font: 44px / 57.2px "Font Awesome 5 Free";
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#SPAN_6:before*/

  #LI_8 {
    block-size: 166px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    height: 166px;
    inline-size: 179.203px;
    min-block-size: auto;
    min-height: auto;
    min-inline-size: auto;
    min-width: auto;
    padding-block-end: 24px;
    padding-block-start: 24px;
    padding-inline-end: 24px;
    padding-inline-start: 24px;
    perspective-origin: 89.5938px 83px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    transform-origin: 89.6016px 83px;
    width: 179.203px;
    background: rgb(168, 156, 200) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(255, 255, 255);
    flex: 1 1 0%;
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
    padding: 24px;
  }/*#LI_8*/

  #LI_8:after {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#LI_8:after*/

  #LI_8:before {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#LI_8:before*/

  #SPAN_9 {
    block-size: 57px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    bottom: 0px;
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    display: inline-block;
    height: 57px;
    inline-size: 55px;
    inset-block-end: 0px;
    inset-block-start: 0px;
    inset-inline-end: 0px;
    inset-inline-start: 0px;
    left: 0px;
    perspective-origin: 27.5px 28.5px;
    position: relative;
    right: 0px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    top: 0px;
    transform-origin: 27.5px 28.5px;
    width: 55px;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
    transition: background-color 0.2s ease-in-out 0s, color 0.2s ease-in-out 0s;
  }/*#SPAN_9*/

  #SPAN_9:after {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#SPAN_9:after*/

  #SPAN_9:before {
    block-size: 57px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    content: '""';
    cursor: default;
    display: inline-block;
    height: 57px;
    inline-size: 55px;
    perspective-origin: 27.5px 28.5px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    transform-origin: 27.5px 28.5px;
    width: 55px;
    border: 0px none rgb(255, 255, 255);
    font: 900 44px / 57.2px "Font Awesome 5 Free";
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#SPAN_9:before*/

  #LI_11 {
    block-size: 166px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    height: 166px;
    inline-size: 179.203px;
    min-block-size: auto;
    min-height: auto;
    min-inline-size: auto;
    min-width: auto;
    padding-block-end: 24px;
    padding-block-start: 24px;
    padding-inline-end: 24px;
    padding-inline-start: 24px;
    perspective-origin: 89.5938px 83px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    transform-origin: 89.6016px 83px;
    width: 179.203px;
    background: rgb(155, 178, 225) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(255, 255, 255);
    flex: 1 1 0%;
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
    padding: 24px;
  }/*#LI_11*/

  #LI_11:after {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#LI_11:after*/

  #LI_11:before {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#LI_11:before*/

  #SPAN_12 {
    block-size: 57px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    bottom: 0px;
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    display: inline-block;
    height: 57px;
    inline-size: 55px;
    inset-block-end: 0px;
    inset-block-start: 0px;
    inset-inline-end: 0px;
    inset-inline-start: 0px;
    left: 0px;
    perspective-origin: 27.5px 28.5px;
    position: relative;
    right: 0px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    top: 0px;
    transform-origin: 27.5px 28.5px;
    width: 55px;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
    transition: background-color 0.2s ease-in-out 0s, color 0.2s ease-in-out 0s;
  }/*#SPAN_12*/

  #SPAN_12:after {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#SPAN_12:after*/

  #SPAN_12:before {
    block-size: 57px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    content: '""';
    cursor: default;
    display: inline-block;
    height: 57px;
    inline-size: 55px;
    perspective-origin: 27.5px 28.5px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    transform-origin: 27.5px 28.5px;
    width: 55px;
    border: 0px none rgb(255, 255, 255);
    font: 900 44px / 57.2px "Font Awesome 5 Free";
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#SPAN_12:before*/

  #LI_14 {
    block-size: 166px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    height: 166px;
    inline-size: 179.203px;
    min-block-size: auto;
    min-height: auto;
    min-inline-size: auto;
    min-width: auto;
    padding-block-end: 24px;
    padding-block-start: 24px;
    padding-inline-end: 24px;
    padding-inline-start: 24px;
    perspective-origin: 89.5938px 83px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    transform-origin: 89.6016px 83px;
    width: 179.203px;
    background: rgb(140, 201, 240) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(255, 255, 255);
    border-radius: 0px 8px 8px 0px;
    flex: 1 1 0%;
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
    padding: 24px;
  }/*#LI_14*/

  #LI_14:after {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#LI_14:after*/

  #LI_14:before {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#LI_14:before*/

  #SPAN_15 {
    block-size: 57px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    bottom: 0px;
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    display: inline-block;
    height: 57px;
    inline-size: 49.5px;
    inset-block-end: 0px;
    inset-block-start: 0px;
    inset-inline-end: 0px;
    inset-inline-start: 0px;
    left: 0px;
    perspective-origin: 24.75px 28.5px;
    position: relative;
    right: 0px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    top: 0px;
    transform-origin: 24.75px 28.5px;
    width: 49.5px;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
    transition: background-color 0.2s ease-in-out 0s, color 0.2s ease-in-out 0s;
  }/*#SPAN_15*/

  #SPAN_15:after {
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    cursor: default;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    border: 0px none rgb(255, 255, 255);
    font: 300 16px / 26.4px "Source Sans Pro", Helvetica, sans-serif;
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#SPAN_15:after*/

  #SPAN_15:before {
    block-size: 57px;
    border-block-end-color: rgb(255, 255, 255);
    border-block-start-color: rgb(255, 255, 255);
    border-inline-end-color: rgb(255, 255, 255);
    border-inline-start-color: rgb(255, 255, 255);
    box-sizing: border-box;
    caret-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    column-rule-color: rgb(255, 255, 255);
    content: '""';
    cursor: default;
    display: inline-block;
    height: 57px;
    inline-size: 49.5px;
    perspective-origin: 24.75px 28.5px;
    text-align: center;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    transform-origin: 24.75px 28.5px;
    width: 49.5px;
    border: 0px none rgb(255, 255, 255);
    font: 44px / 57.2px "Font Awesome 5 Free";
    list-style: outside none none;
    outline: rgb(255, 255, 255) none 0px;
  }/*#SPAN_15:before*/


</style>
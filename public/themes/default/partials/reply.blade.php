<li class="commented delete_comment_list comment-replied">
 <div class="comments"> <!-- main-comment -->
  <div class="commenter-avatar">
    <a href="#"><img src="{{ $reply->user->avatar }}" title="{{ $reply->user->name }}" alt="{{ $reply->user->name }}"></a>
  </div>
  <div class="comments-list">
    <div class="commenter">
      @if($reply->user_id == Auth::user()->id)
      <a href="#" class="delete-comment delete_comment" data-commentdelete-id="{{ $reply->id }}"><i class="fa fa-times"></i></a>
      @endif  
      <div class="commenter-name">
        <a href="{{ url($reply->user->username) }}">{{ $reply->user->name }}</a><span class="comment-description">{{ $reply->description }}</span>
      </div>
      <ul class="list-inline comment-options">
          <?php
          if(!$reply->comments_liked->contains(Auth::user()->id)) {
              $likeIcon = '<i class="far fa-heart"></i>';
              $likeClass = '';
          } else {
              $likeIcon = '<i class="fas fa-heart active-like"></i>';
              $likeClass = 'active-like';
          }
          ?>
        <li>
          <a href="#" class="text-capitalize like-comment like-comment-{{$reply->id}} like" data-comment-id="{{ $reply->id }}">
            {!! $likeIcon !!}
          </a>
          <a href="#" class="show-users-modal count-likes-comment-{{$reply->id}} like3-{{ $reply->id }} {!! $likeClass !!}" data-heading="{{ trans('common.likes') }}" data-rec-id="{{$reply->id}}" data-type="comment">
            {{ ($reply->comments_liked->count())?$reply->comments_liked->count():'' }}
          </a>
        </li>
        <li>.</li>
        <li>
          <time class="post-time timeago" datetime="{{ $reply->created_at }}" title="{{ $reply->created_at }}">{{ $reply->created_at }}</time>
          {{--<time class="post-time timeago" datetime="{{ $reply->created_at }}+00:00" title="{{ $reply->created_at }}+00:00">{{ $reply->created_at }}+00:00</time>--}}
        </li>
      </ul>
    </div>
  </div>
</div><!-- main-comment -->
</li>

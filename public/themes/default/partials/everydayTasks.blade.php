<div class="col-lg-12 panel-default"
     style="margin-bottom: 30px; padding-bottom: 10px; /*background: #eec781;">
    <h2 style="margin: 10px 0px 0px; text-align: center; text-transform: uppercase;"><span
                style="color: #e0995e">Ключи</span> за ежедневные задания</h2>
    <div>
        <?php $tasks = \App\Models\EverydayTasks::query()->get(); ?>
        <div class="row">
            @forelse($tasks as $task)
                <div class="col-lg-2" style="padding: 0 10px;">
                    {{--<div style="background: linear-gradient(to top right, #c6e7ff 37%, #1d94eb 81%); text-align: center; border-radius: 3px;">--}}
                    <div style="border: 1px solid #E6EAEE; background: #fafbfc; color: #5B6B81; text-align: center; border-radius: 3px;">
                        <h4 style="margin: 0; padding-top: 10px; font-size: 16px; /*color: #fff;"><img
                                    src="{!! url('icon/'.$task->img) !!}" width="25px" height="25px"
                                    alt=""> {{$task->name}}</h4>

                        <h4 style="margin-top: 10px; margin-bottom: 0px; padding-top: 0; font-size: 16px; /*color: #ffffff; background: #1d94ea;">
                            Награда: {{$task->price}} <img src="{!! url('icon/key.png') !!}" width="20px" height="20px"
                                                           alt="" style="vertical-align: text-bottom;">
                        </h4>
                        <div class="btns-reward" data-type="{{$task->type}}" style="padding: 5px 0;">
                            @if ($task->checkComplete())
                                @if ($task->checkComplete('status'))
                                    <button type="submit" class="btn-primary btn-reward reward"
                                            data-type="{{$task->type}}">получено
                                    </button>
                                @else
                                    <button type="submit" class="btn-success get-reward" data-type="{{$task->type}}">
                                        забрать
                                    </button>
                                @endif
                            @else
                                <button type="submit" class="btn-warning btn-reward" data-type="{{$task->type}}">не
                                    выполнено
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            @empty
                Нет заданий
            @endforelse
        </div>
    </div>
</div>
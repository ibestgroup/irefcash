<div class="widget-events widget-left-panel">
	<div class="menu-list">
		<ul class="list-unstyled">
			<li><a href="{{ url(Auth::user()->username) }}" class="btn menu-btn"><i class="fa fa-user" aria-hidden="true"></i>{{ trans('common.my_profile') }} </a></li>
			<li class="{!! (Request::segment(1)=='' ? 'active' : '') !!}"><a href="{{ url('/') }}" class="btn menu-btn"><i class="fa fa-trophy" aria-hidden="true"></i>{{ trans('common.home') }}</a></li>

			@if(Setting::get('enable_browse') == 'on')
				<li class="{!! (Request::segment(1)=='browse' ? 'active' : '') !!}"><a href="{{ url('/browse') }}" class="btn menu-btn"><i class="fa fa-globe" aria-hidden="true"></i>{{ trans('common.browse') }} </a></li>
			@endif

			<li class="{!! (Request::segment(2)=='albums' ? 'active' : '') !!}"><a href="{{ url('/'.Auth::user()->username.'/albums') }}" class="btn menu-btn"><i class="fa fa-film" aria-hidden="true"></i>{{ trans('common.albums') }}</a></li>

			<li class="{!! (Request::segment(1)=='messages' ? 'active' : '') !!}"><a href="{{ url('messages') }}" class="btn menu-btn"><i class="fa fa-comments" aria-hidden="true"></i>{{ trans('common.messages') }}</a></li>

{{--			<li><a href="{{ url(Auth::user()->username.'/pages') }}" class="btn menu-btn"><i class="fa fa-file-text" aria-hidden="true"></i>{{ trans('common.pages') }} <span class="event-circle">{{ Auth::user()->own_pages()->count() }}</span></a></li>--}}

			<li><a href="{{ url(Auth::user()->username.'/groups') }}" class="btn menu-btn"><i class="fa fa-group" aria-hidden="true"></i>{{ trans('common.groups') }} <span class="event-circle">{{ Auth::user()->own_groups()->count() }}</span></a></li>

			<li><a href="{{ url(Auth::user()->username.'/everydaytask') }}" class="btn menu-btn"><i class="fa fa-tasks" aria-hidden="true"></i>Ежедневные <span class="event-circle">{{ Auth::user()->tasks() }}</a></li>
			<li><a href="{{ url(Auth::user()->username.'/transaction') }}" class="btn menu-btn"><i class="fa fa-history" aria-hidden="true"></i>Операции</a></li>
{{--			<li><a href="{{ url(Auth::user()->username.'/saved') }}" class="btn menu-btn"><i class="fa fa-save" aria-hidden="true"></i>{{ trans('common.saved_items') }} </a></li>--}}

{{--			<li><a href="{{ url(Auth::user()->username.'/matrix') }}" class="btn menu-btn"><i class="fa fa-save" aria-hidden="true"></i>Матрицы </a></li>--}}

			<li><a href="{{ url('/'.Auth::user()->username.'/settings/general') }}" class="btn menu-btn"><i class="fa fa-cog" aria-hidden="true"></i>{{ trans('common.settings') }}</a></li>
			{{--<li class="{!! (Request::segment(1)=='monitoring' ? 'active' : '') !!}"><a href="{{ route('monitoring') }}" class="btn menu-btn"><i class="fa fa-columns" aria-hidden="true"></i>Мониторинг</a></li>--}}
		</ul>
	</div>
    <div class="menu-heading">
        Заработок
    </div>

    <div class="menu-list">
        <ul class="list-unstyled">
            <li><a href="/matrix" class="btn menu-btn"><i class="fas fa-vector-square"></i>Матрицы</a></li>
            <li><a href="/surfing" class="btn menu-btn"><i class="far fa-window-restore"></i>Серфинг</a></li>
            <li><a href="/interview" class="btn menu-btn"><i class="fas fa-question-circle"></i>Опросы</a></li>
            <li><a href="{{ route('shortLink') }}" class="btn menu-btn"><i class="fas fa-link"></i>Ссылки</a></li>
            <li><a href="{{ url(Auth::user()->username.'/matrix') }}" class="btn menu-btn"><i class="fas fa-ticket-alt"></i>Лотерея</a></li>
        </ul>
    </div>

    <div class="menu-heading">
        Реклама
    </div>
    <div class="menu-list">
        <ul class="list-unstyled">
			{{--<li><a href="/matrix" class="btn menu-btn"><i class="fas fa-vector-square"></i>Матрицы</a></li>--}}
			<li><a href="{{ route('myLink') }}" class="btn menu-btn"><i class="far fa-window-restore"></i>Серфинг <i class="fas fa-bullhorn pull-right"></i></a></li>
			<li><a href="{{ route('myInterview') }}" class="btn menu-btn"><i class="fas fa-question-circle"></i>Опросы <i class="fas fa-bullhorn pull-right"></i></a></li>
			<li><a href="{{ route('shortLinkAdvert') }}" class="btn menu-btn"><i class="fas fa-link"></i>Ссылки <i class="fas fa-bullhorn pull-right"></i></a></li>
			<li><a href="{{ route('shortLinkAdvert') }}" class="btn menu-btn"><i class="fas fa-link"></i>Баннеры <i class="fas fa-bullhorn pull-right"></i></a></li>
		</ul>
    </div>
	@if (isset($trending_tags))
	<div class="menu-heading">
		{{ trans('common.most_trending') }}
	</div>
	<div class="categotry-list">
		<ul class="list-unstyled">
			@if($trending_tags != "")
				@foreach($trending_tags as $trending_tag)
				<li><span class="hash-icon"><i class="fa fa-hashtag"></i></span> <a href="{{ url('?hashtag='.$trending_tag->tag) }}">{{ $trending_tag->tag }}</a></li>
				@endforeach
			@else
				<span class="text-warning">{{ trans('messages.no_tags') }}</span>
				
			@endif
		</ul>
	</div>
	@endif
</div><!-- /widget-events -->
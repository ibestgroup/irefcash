<div class="col-lg-12 panel-default"
     style="padding-bottom: 10px; margin-bottom: 10px; /*background: #eec781;">
    <h2 style="margin: 10px 0px 0px; text-align: center; text-transform: uppercase;"><span
                style="color: #e0995e">Место для вашей рекламы</h2>
    <div>
        <?php $tasks = \App\Models\EverydayTasks::query()->get(); ?>
        <div class="row">
            @forelse($tasks as $task)
                <div class="col-lg-2" style="padding: 0 10px;">
                    <div style="border: 1px solid #E6EAEE; background: #fafbfc; color: #5B6B81; text-align: center; border-radius: 3px;">
                        <h4 style="margin: 0; padding-top: 10px; font-size: 16px;">Заголовок</h4>

                        <p style="margin-top: 5px; margin-bottom: 0px; padding-top: 0; font-size: 13px; /*color: #ffffff; background: #1d94ea;">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>
                        <div class="btns-reward" data-type="{{$task->type}}" style="padding: 5px 0;">
                            <a class="btn-success get-reward" href="{{$task->type}}" style="padding: 0 5px;">
                                перейти
                            </a>
                        </div>
                    </div>
                </div>
            @empty
                Нет заданий
            @endforelse
        </div>
    </div>
</div>
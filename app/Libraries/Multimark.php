<?php

namespace App\Libraries;

use App\Helpers\MarksHelper;
use App\Models\MarksElement;
use App\Models\MarksInside;
use App\Models\OrdersMarkInside;
use App\User;

class Multimark {
    public $height = 99;
    public $overflow;
    public $width;
    public $refer;
    public $parentUser;
    public $id_mark;
    public $ch_po;
    public $ms_prefix;
    public $auto;
    public $ordersTree;
    public $orders;
    public $levelsMas = array();
    public $parentMark;
    public $ordersMark;
    public $logins;

    public function __construct($refer, $id_mark, $auto = false, $markElement)
    {
        global $dom;
        $this->refer = $refer;
        $this->id_mark = $id_mark;
        $this->ms_prefix = ($auto)?'_inside':'';
        $this->auto = $auto;
//        $this->active_mark = MarksHelper::activeRound($this->id_mark, false);
        $this->statement = ['mark' => $this->id_mark, 'mark_element' => $markElement];
        $this->ordersMark = OrdersMarkInside::class;
        $this->mark = MarksInside::find($this->id_mark);
        $this->width = $this->mark->width;
        $this->overflow = $this->mark->overflow;  //TODO: Здесь будет единица, только если маркетинг - 1 ( #youngoldman# Здесь может быть в обоих случаях единица)

        //Родитель из таблицы users
        $this->parentUser = User::find($this->refer);
        $check_mark = $this->ordersMark::where($this->statement)->where('id_user', $this->refer)->count();
        $for_check_mark = $this->parentUser->refer;
        while ( $check_mark == 0 ) {// Если refer нулевого уровня - ищем первого ненулевого выше в структуре.
            $check_mark = $this->ordersMark::where($this->statement)->where('id_user', $for_check_mark)->count();
//            dd($check_mark, $for_check_mark, $this->id_mark, $this->active_mark);
            $this->parentUser = User::where('id', $for_check_mark)->first();
            $for_check_mark = $this->parentUser->refer;
        }

        $this->refer = $this->parentUser->id;
    }


    public function overflowMulti() {

        $orders = array();
        $users =  $this->ordersMark::sharedLock()->get();

        foreach ($users as $user) {
            $orders[] = $user;
        }

        $orderId = $this->ordersMark::where($this->statement)->where('id_user', $this->refer)->sharedLock()->max('id');

        if ($this->auto) {
            $countOrdersInside = $this->ordersMark::where($this->statement)
                                                ->where('id_user', $this->refer)
                                                ->where('status', 0)
                                                ->sharedLock()
                                                ->count();

            if ($countOrdersInside > 0) {
                $order_inside = $this->ordersMark::where($this->statement)
                                            ->where('id_user', $this->refer)
                                            ->where('status', 0)
                                            ->sharedLock()
                                            ->get()[0]['id'];
            } else {
                $order_inside = $this->ordersMark::where($this->statement)
                                            ->where('id_user', $this->refer)
                                            ->sharedLock()
                                            ->get()[0]['id'];
            }

            $orderId = $this->ordersMark::where($this->statement)
                                    ->where('id_user', $this->refer)
                                    ->sharedLock()
                                    ->first();

            if ($this->overflow == 1) {
                $ordersId = $this->ordersMark::where($this->statement)
                                        ->where('id_user', $this->refer)
                                        ->sharedLock()
                                        ->get();

                foreach ($ordersId as $ord) {
                    if (MarksHelper::checkClosed($ord->id) == false) {
                        $orderId = $this->ordersMark::where('id', $ord->id)
                                                    ->sharedLock()
                                                    ->first();
                        break;
                    }
                }
            }

        }



        if ($this->overflow) {

            $this->ch_po = $orderId['id'];



            //Строим массив с зависимостями
            foreach ($orders as $key  => $order) {
                $this->orders[$order['par_or']][] = $order['id'];
            }


            //Формируем уровневый массив
            $this->outLevels($this->ch_po);
            $out = $this->searchParent();

            if (!$this->auto) {
                return $this->ordersMark::where('id', $out)->get()[0]['id_user'];
            }

            return $out;
        }

        return ($this->auto)?$order_inside:$this->refer;
    }

    function searchParent () {
        for ($i = 1; $i <= count($this->levelsMas); $i++) {
            foreach ($this->levelsMas[$i] as $id => $child) {

                $c = array_search($id, $child);
                if ($c !== false)
                    unset($child[$c]);
                if (count($child) < $this->width) {
                    return $id;
                } else {
                    foreach ($child as $child_id) {
                        if ($id != $child_id) {
                            if (count($this->levelsMas[$i+1][$child_id]) < $this->width)
                            return $child_id;
                        }
                    }
                }
            }
        }
    }

    function outLevels($parent_id = 1, $level = 1) {

        if (isset($this->orders[$parent_id]) && !empty($this->orders[$parent_id])) {
            foreach ($this->orders[$parent_id] as $k => $mark) { //Обходи
                if ($mark != 1 && $mark != $parent_id) {
                    //Рекурсивно вызываем эту же функцию, но с новым $parent_id и $level
                    $this->outLevels($mark, $level + 1);
                    $this->levelsMas[$level][$parent_id] = $this->orders[$parent_id];
                } else {
                    $this->levelsMas[$level][$parent_id] = $this->orders[$parent_id];
                }
            }
        } else {
            $this->levelsMas[$level][$parent_id] = array();
        }
    }

    static function outTree($parent_id = 1, $orders = [], $logins = []) {
        $mas = array();
        if (isset($orders[$parent_id])) {
            foreach ($orders[$parent_id] as $mark) { //Обходим
                if ($mark['id'] != $parent_id) {
                    $mark['name'] = $logins[$mark['user']];
                    //Рекурсивно вызываем эту же функцию, но с новым $parent_id

                    $mark['children'] = self::outTree($mark['id'], $orders, $logins);
                    $mas[] = $mark;
                }
            }
        }

        return $mas;
    }

    public static function createTree($orderID) {
        $mark = OrdersMarkInside::where('id', $orderID)->first();
        $markId = $mark->mark;
        $markEl = $mark->mark_element;
        $marks = OrdersMarkInside::where('mark', $markId)->where('mark_element', $markEl)->get();

        foreach ($marks as $key  => $order) {
            $orders[$order['par_or']][] = [
                'id' => $order->id,
                'user' => $order->id_user,
                'profit' => $order->balance,
            ];
            $users[] = $order->id_user;
        }

        $users = User::whereIn('id', $users)->select('id', 'login')->get();
        foreach ($users as $user) {
            $logins[$user->id] = $user->login;
        }

//        dd($orders);

        return self::outTree($orderID, $orders, $logins);
    }
}

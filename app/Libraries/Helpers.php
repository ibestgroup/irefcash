<?php
/**
 * Created by PhpStorm.
 * User: danilsysoev
 * Date: 20/12/2020
 * Time: 10:05
 */

namespace App\Libraries;


use App\Hashtag;
use App\Models\EverydayTasks;
use App\Models\EverydayTasksComplete;
use App\Models\EverydayTasksRandom;
use App\Models\MarksElement;
use App\Models\MarksInside;
use App\Models\OrdersMarkInside;
use App\Models\Transaction;
use App\Models\TransactionDepositAd;
use App\Models\TransactionEarn;
use App\Role;
use App\Setting;
use App\Timeline;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Helpers
{

    public static function convertUs($meth, $data) {
        if ($meth == 'id') {
            return User::where('login', $data)->first()->id;
        } elseif($meth == 'login') {
            return @User::find($data)->username;
        }
    }

    public static function everydayTask($type = 'login') {
        $task = EverydayTasks::where('type', $type)->first();
        $taskMessage = '';
        if (!$task->checkComplete()) {
            EverydayTasksComplete::create([
                'status' => 0,
                'user_id' => Auth::id(),
                'task_id' => $task->id,
                'type' => $task->type,
                'amount' => $task->price,
                'day' => date('Y-m-d'),
            ]);
            $taskMessage = 'Задание выполнено! Заберите награду!';
            Helpers::addExpPoints(Auth::id(), 3);
        }

        return $taskMessage;
    }

    public static function everydayTaskRandom($meth = 'create') {

        $day = date('Y-m-d');
        $arrayTypes = [
            'ufo' => [
                'price' => rand(1,10),
                'random' => rand(1, 50),
                'in_day' => 1,
                'name' => 'Лови НЛО!',
                'img' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Circle-icons-ufo.svg/1024px-Circle-icons-ufo.svg.png',
                'cursor' => "cursor: url('https://i.ibb.co/Tws0zkx/sachok128.png') 40 40, auto",
            ],
            'pig' => [
                'price' => rand(1,10),
                'random' => rand(1, 50),
                'in_day' => 1,
                'name' => 'Разбей копилку!',
                'img' => 'https://s1.iconbird.com/ico/0612/SEVEN/w256h2561339360453piggybank2.png',
                'cursor' => "cursor: url('https://s1.iconbird.com/ico/0612/GooglePlusInterfaceIcons/w128h1281338911352hammer.png') 40 40, auto",
            ],
        ];

        foreach ($arrayTypes as $key => $type) {

            $task = EverydayTasksRandom::where('user_id', Auth::id())->where('day', $day)->where('type', $key);

            if ($meth == 'create') {
                $countAny = EverydayTasksRandom::where('user_id', Auth::id())->where('day', $day)->where('status', 0)->count();
                if ($task->count() < $type['in_day'] and !$countAny) {
                    if ($type['random'] == 1) {

                        EverydayTasksRandom::create([
                            'status' => 0,
                            'type' => $key,
                            'user_id' => Auth::id(),
                            'price' => $type['price'],
                            'day' => $day,
                        ]);

                    }
                }

            } elseif ($meth == 'show') {
                if ($task->where('status', 0)->count()) {
                    $taskId = $task->where('status', 0)->first()->id;
//                    switch ($key) {
//                        case 'ufo':
                            $arrayTB = ['top', 'bottom'];
                            $arrayLR = ['left', 'right'];
                            $randVertical = [rand(0,1), rand(10,400)];
                            $randHorizontal = [rand(0,1), rand(10,800)];

                            $html = '
                                <div style="position: absolute; '.$arrayLR[$randHorizontal[0]].': '.$randHorizontal[1].'px; '.$arrayTB[$randVertical[0]].': '.$randVertical[1].'px;" class="ufo random-task">
                                    <img class="get-reward-random" data-id="'.$taskId.'" src="'.$type['img'].'" width="150px" height="150px" style="'.$type['cursor'].'" alt=""><br>
                                    <div>'.$type['name'].'</div>
                                </div>
                            ';
                            return $html;
//                            break;
//                        default:
//                            break;
//                    }
                }

            }

        }


    }

    public static function myRound($number, $type = 'low') {

        return bcdiv($number, 1, 4) * 1;

    }

    public static function priorityArray($num) {
        $num = (string)$num;
        $int = preg_split('//', $num);
        array_pop($int);
        array_shift($int);

        foreach ($int as $key => $value) {
            if ($value == 1) {
                $int[$key] = 'cashout';
            } elseif ($value == 2) {
                $int[$key] = 'level_up';
            } elseif ($value == 3) {
                $int[$key] = 'reinvest';
            } elseif ($value == 4) {
                $int[$key] = 'reinvest_first';
            }
        }

        return $int;
    }

    public static function getInfoForUserHeader($username) {
        $timeline = Timeline::where('username', $username)->with('user', 'user.pageLikes', 'user.groups')->first();
        $user = $timeline->user;
        $admin_role_id = Role::where('name', '=', 'admin')->first();
        $joined_groups = $user->groups()->where('role_id', '!=', $admin_role_id->id)->where('status', '=', 'approved')->get();
        $joined_groups_count = $joined_groups->count();

        $following_count = $user->following()->where('status', '=', 'approved')->get()->count();
        $followers_count = $user->followers()->where('status', '=', 'approved')->get()->count();
        $followRequests = $user->followers()->where('status', '=', 'pending')->get();

        $confirm_follow_setting = $user->getUserSettings(Auth::user()->id);
        $follow_confirm = $confirm_follow_setting->confirm_follow;

        $live_user_settings = $user->getUserPrivacySettings(Auth::user()->id, $user->id);
        $privacy_settings = explode('-', $live_user_settings);
        $user_post = $privacy_settings[1];
        $user_events = $user->events()->whereDate('end_date', '>=', date('Y-m-d', strtotime(Carbon::now())))->get();
        $guest_events = $user->getEvents();

        return compact('timeline', 'user', 'username', 'followRequests', 'follow_confirm', 'following_count', 'followers_count', 'user_post', 'joined_groups_count', 'user_events', 'guest_events');
    }

    public static function addExpPoints($us_id, $amount) {
        DB::beginTransaction();
        $user = User::find($us_id);
        $points = $user->exp_point;
        $resultPoints = $user->exp_point + $amount;
        $rang = $user->rang;

        $rangArray = self::rangArray();
        foreach ($rangArray as $key => $item) {
            if ($resultPoints >= $item['from'] and $resultPoints < $item['to']) {
                $resultRang = $key;
            }
        }

        User::find($us_id)->update([
            'exp_point' => $resultPoints,
            'rang' => $resultRang,
        ]);

        DB::commit();
    }

    public static function rangArray() {
        return [
            [
                'from' => 0,
                'to' => 100,
                'name' => 'Гость',
                'percent' => 5
            ],
            [
                'from' => 100,
                'to' => 500,
                'name' => 'Новичок',
                'percent' => 10
            ],
            [
                'from' => 500,
                'to' => 1000,
                'name' => 'Ученик',
                'percent' => 15
            ],
            [
                'from' => 1000,
                'to' => 10000,
                'name' => 'Знающий',
                'percent' => 20
            ],
            [
                'from' => 10000,
                'to' => 100000,
                'name' => 'Трудяга',
                'percent' => 25
            ],
            [
                'from' => 100000,
                'to' => 500000,
                'name' => 'Богач',
                'percent' => 30
            ],
            [
                'from' => 500000,
                'to' => 2500000,
                'name' => 'Магнат',
                'percent' => 35
            ],
            [
                'from' => 2500000,
                'to' => 10000000,
                'name' => 'Миллионер',
                'percent' => 40
            ],
            [
                'from' => 10000000,
                'to' => 100000000,
                'name' => 'Мультимиллионер',
                'percent' => 50
            ],
        ];
    }

    public static function getRang($us_id, $meth = 'num') {
        $user = User::find($us_id);
        if ($meth == 'num') {
            return $user->rang;
        } elseif ($meth == 'name') {
            return self::rangArray()[$user->rang]['name'];
        } elseif ($meth == 'progress') {
            $rang = self::rangArray()[$user->rang];
            $points = $user->exp_point;
            $pointOnLevel = $points - $rang['from'];
            $allPointOnLevel = $rang['to'] - $rang['from'];
            $width = $pointOnLevel/$allPointOnLevel;
            return [$pointOnLevel, $allPointOnLevel, $width];
        } elseif ($meth == 'progressbar') {
            $rang = self::rangArray()[$user->rang];
            $points = $user->exp_point;
            $pointOnLevel = $points - $rang['from'];
            $allPointOnLevel = $rang['to'] - $rang['from'];
            $width = $pointOnLevel/$allPointOnLevel*100;
            $result = '<div class="progress-bar-container">
                            <div class="progress-bar stripes animated reverse slower" data-toggle="tooltip" data-original-title="Очки опыта @'.self::convertUs('login', $us_id).'">
                                <span class="progress-bar-text"> '.$pointOnLevel.'/'.$allPointOnLevel.' ОП</span>
                                <span class="progress-bar-inner" style="width: '.$width.'%"></span>
                            </div>
                        </div>';
            return $result;
        }
    }

    public static function trandingTags() {
        $trending_tags = Hashtag::orderBy('count', 'desc')->get();
        if (count($trending_tags) > 0) {
            if (count($trending_tags) > (int) Setting::get('min_items_page')) {
                $trending_tags = $trending_tags->random((int) Setting::get('min_items_page'));
            }
        } else {
            $trending_tags = '';
        }

        return $trending_tags;
    }

    public static function prices() {
        $array = [5, 10, 15, 20, 30, 40, 60, 80, 100];
        return $array;
    }

    public static function getLastNDays($days, $format = 'Y-m-d', $json = true){
        $m = date("m"); $de= date("d"); $y= date("Y");
        $dateArray = array();
        for($i=0; $i<=$days-1; $i++){
            $dateArray[] = date($format, mktime(0,0,0,$m,($de-$i),$y));
        }

        $result = array_reverse($dateArray);
        if ($json) {
            $result = json_encode($result);
        }

        return $result;
    }

    public static function shortLink($id) {

        $chars = "abcdefghjkmnpqrstvwxyzABCDEFGHJKLMNPQRSTVWXYZ";

        $length = strlen($chars);

        $code = "";
        while ($id > $length - 1) {
            // determine the value of the next higher character
            // in the short code should be and prepend
            $key = (int)fmod($id, $length);

            $code = $chars[$key] . $code;
            // reset $id to remaining value to be converted
            $id = (int)floor($id / $length);
        }

        $code = $chars[$id] . $code;

        return $code;

    }

    public static function secToArray($secs)
    {
        $res = array();

        $res['days'] = floor($secs / 86400);
        $secs = $secs % 86400;

        $res['hours'] = floor($secs / 3600);
        $secs = $secs % 3600;

        $res['minutes'] = floor($secs / 60);
        $res['sec'] = $secs % 60;

        $string['days'] = ($res['days'])?$res['days'].'д ':'';
        $string['hours'] = ($res['hours'])?$res['hours'].'ч ':'';
        $string['minutes'] = ($res['minutes'])?$res['minutes'].'м ':'';
        $string['sec'] = ($res['sec'])?$res['sec'].'с ':'';
        $resString = '';
        foreach ($string as $item) {
            $resString .= $item;
        }

        return $resString;
    }

    public static function checkBoost($userId, $boostType, $amount = 0, $taskId = 0) {

        $markElements = MarksElement::where('status', 1)->get();

        $boosts = [
            'boost_surfing' => 0,
            'boost_interview' => 0,
            'boost_link' => 0,
            'boost_surfing_aff' => 0,
            'boost_interview_aff' => 0,
            'boost_link_aff' => 0,
        ];

        if (!array_key_exists($boostType, $boosts)) {
            return 0;
        }

        if ($markElements) {

            foreach ($markElements as $item) {

                $checkActivate = OrdersMarkInside::where('id_user', $userId)->where('mark_element', $item->id)->count();

                if ($checkActivate) {

                    $mark = MarksInside::find($item->mark_id);

                    foreach ($boosts as $key => $boost) {
                        if ( $mark->{$key} > $boosts[$key] ) {
                            $boosts[$key] = $mark->{$key};
                        }
                    }

                }

            }

        } else {
            return 0;
        }

        if ($amount == 0) {
            return $boosts[$boostType];
        } else {

            // transaction here $boostType ++
            $resultSum = $amount / 100 * $boosts[$boostType];

            if ($resultSum != 0) {
                Helpers::createTransaction([
                    'table' => 'transaction_earn',
                    'type' => $boostType,
                    'task_id' => $taskId,
                    'from_user_id' => 0,
                    'user_id' => $userId,
                    'amount' => $resultSum,
                ]);

                User::whereId($userId)->increment('balance', $resultSum);
            }
        }
    }


    /**
     * @param array $data<table,type> sfsa
     */
    public static function createTransaction(array $data) {


        if ($data['table'] == 'transaction_earn') {

            $transaction = TransactionEarn::create([
                'from_id' => $data['task_id'],
                'from_user_id' => $data['from_user_id'],
                'user_id' => $data['user_id'],
                'type' => $data['type'],
                'amount' => $data['amount'],
                'date' => Carbon::now(),
            ]);

        } elseif ($data['table'] == 'transaction_deposit_ad') {

            $transaction = TransactionDepositAd::create([
                'user_id' => $data['from_user_id'],
                'task_id' => $data['task_id'],
                'type' => $data['type'],
                'amount' => $data['amount'],
                'date' => Carbon::now()
            ]);

        }

        if ($transaction->id) {

            Transaction::create([
                'transaction_id' => $transaction->id,
                'from_user' => $data['from_user_id'],
                'to_user' => $data['user_id'],
                'type' => $data['table'],
                'date' => Carbon::now(),
            ]);

        }
    }

    public static function getIP()
    {
        return $_SERVER['HTTP_X_REAL_IP'] ?? $_SERVER['REMOTE_ADDR'];
    }

    public static function checkWhitelist($system)
    {
        return in_array(self::getIP(), config("paysystems.{$system}.whitelist"));
    }


}
#!/bin/bash

beta=true

echo "==> Checking for brew..."
which brew > /dev/null
if [ $? -ne 0 ]; then
	echo "==> Homebrew is not installed on your Mac. For StableA7 to work properly, you need to install it."
	read -p "==> Press enter/return to install Homebrew... "
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

echo "==> Installing dependencies..."
brew install autoconf automake bsdiff libimobiledevice libtool libzip lsusb openssl pkg-config wget

echo "==> Checking for libirecovery..."
which irecovery > /dev/null
if [ $? -ne 0 ]; then
	echo "==> Downloading libirecovery..."
	git clone https://github.com/libimobiledevice/libirecovery.git

	echo "==> Making libirecovery..."
	cd libirecovery
	./autogen.sh && make

	echo
	echo "==> Installing libirecovery. This might ask for your password..."
	sudo make install
	cd ..
	rm -rf libirecovery
fi

echo "==> Checking for libfragmentzip..."
if [ ! -d /usr/local/include/libfragmentzip ]; then
	echo "==> Downloading libfragmentzip..."
	git clone https://github.com/tihmstar/libfragmentzip.git

	echo "==> Making libfragmentzip..."
	cd libfragmentzip
	./autogen.sh && make

	echo
	echo "==> Installing libfragmentzip. This might ask for your password..."
	sudo make install
	cd ..
	rm -rf libfragmentzip
fi

clear
echo "**************** StableA7 ****************"
echo
echo "=> By Luke:"
echo "=>   - u/TheLukeGuy"
echo "=>   - @ConsoleLogLuke"
echo

if [ $beta == true ]; then
	echo "==> WARNING: This is a beta version. Things might not work properly or at all."
	echo
fi

echo "==> Which futurerestore patch would you like to use?"
echo "==> If you are unsure, choose option 1. This is only present in this beta version."
echo
echo "=> (1) Normal/Auto"
echo "=>   - Attempts to perform a restore without custom dylibs"
echo "=>   - Inconsistent, sometimes works and sometimes doesn't"
echo "=>   - Works on macOS High Sierra, Mojave, and Catalina"
echo "=>   - Doesn't require root"
echo "=>   - Switches to rsu patch if it fails"
echo
echo "=> (2) /rsu"
echo "=>   - Makes futurerestore look for custom dylibs in /rsu instead of /usr"
echo "=>   - Only works on macOS High Sierra and Mojave"
echo "=>   - Works on macOS Catalina with SIP disabled and / remounted as rw"
echo "=>   - Requires root"
echo
read -p "==> Which patch would you like to use? (1/2): " patch

if [ "$patch" != 1 ] && [ "$patch" != 2 ]; then
	echo "==> Invalid patch number, you must choose either 1 or 2."
	exit 1
fi

echo "==> Creating work directory..."
if [ -d StableA7 ]; then
	rm -r StableA7
fi
mkdir StableA7
cd StableA7

echo "==> Downloading binaries..."
wget -O bin.zip https://gitlab.com/devluke/stablea7/-/archive/master/stablea7-master.zip?path=bin -q --show-progress
unzip -q bin.zip
cp -r stablea7-master-bin/bin .
chmod +x bin/*
rm -r stablea7-master-bin bin.zip

echo "==> Downloading patches..."
wget -O patch.zip https://gitlab.com/devluke/stablea7/-/archive/master/stablea7-master.zip?path=patch -q --show-progress
unzip -q patch.zip
cp -r stablea7-master-patch/patch .
rm -r stablea7-master-patch patch.zip

macos=$(sw_vers -productVersion)
minor=$(echo "$macos" | awk -F. '{ print $2; }')

function rsu {
	if [ $minor -eq 15 ]; then
		echo
		echo "==> The rsu patch only works on macOS Catalina if SIP is disabled."
		read -p "==> Press enter/return if SIP is disabled. Otherwise, disable it now... "

		echo "==> Remounting / as rw..."
		sudo mount -uw /
	fi

	echo "==> Downloading rsu..."
	wget -O rsu.zip https://gitlab.com/devluke/stablea7/raw/master/rsu.zip -q --show-progress
	unzip -q rsu.zip
	rm rsu.zip

	echo "==> Moving rsu to /rsu. This might ask for your password..."
	sudo rm -rf /rsu 2> /dev/null
	sudo mv rsu /
}

function binary {
	echo "==> Renaming futurerestore binary..."
	if [ $minor -ge 14 ] && [ $patch -eq 1 ]; then
		mv bin/futurerestore_normal bin/futurerestore
	elif [ $minor -le 13 ] && [ $patch -eq 1 ]; then
		mv bin/old_futurerestore_normal bin/futurerestore
	elif [ $patch -eq 2 ]; then
		if [ $minor -ge 14 ]; then
			mv bin/futurerestore_rsu bin/futurerestore
		elif [ $minor -le 13 ]; then
			mv bin/old_futurerestore_rsu bin/futurerestore
		fi

		if [ $minor -eq 15 ]; then
			echo
			echo "==> The rsu patch only works on macOS Catalina if SIP is disabled."
			read -p "==> Press enter/return if SIP is disabled. Otherwise, disable it now... "

			echo "==> Remounting / as rw..."
			sudo mount -uw /
		fi

		echo "==> Downloading rsu..."
		wget -O rsu.zip https://gitlab.com/devluke/stablea7/raw/master/rsu.zip -q --show-progress
		unzip -q rsu.zip
		rm rsu.zip

		echo "==> Moving rsu to /rsu. This might ask for your password..."
		sudo rm -rf /rsu 2> /dev/null
		sudo mv rsu /
	fi
}

binary

echo "==> Waiting for device..."
info=
while :; do
	info=$(bin/igetnonce 2> /dev/null | grep ,)
	if [ $? -eq 0 ]; then
		break
	fi
done

identifier=${info#*, }
identifier=${identifier%% *}
echo "==> $identifier found!"

model=
if [ $identifier == iPhone6,1 ] || [ $identifier == iPhone6,2 ]; then
	model=iphone6
	echo "==> Your iPhone 5S is supported!"
elif [ $identifier == iPad4,1 ] || [ $identifier == iPad4,2 ]; then
	model=ipad4
	echo "==> Your iPad Air is supported!"
elif [ $identifier == iPad4,4 ] || [ $identifier == iPad4,5 ]; then
	model=ipad4b
	echo "==> Your iPad Mini 2 is supported!"
else
	echo
	echo "==> Your device is not supported. Only the following devices work with this tool:"
	echo
	echo "=>   - iPhone 5S (iPhone6,1 or iPhone6,2)"
	echo "=>   - iPad Air (iPad4,1 or iPad4,2)"
	echo "=>   - iPad Mini 2 (iPad4,4 or iPad4,5)"
	echo
	echo "==> Sorry! ;("
	exit 1
fi

echo
echo "==> If you already downloaded an IPSW file, you can use it with one of these two options:"
echo
echo "=>   - Move the IPSW file to the StableA7 folder and press enter/return"
echo "=>   - Drag the IPSW file into this Terminal window and press enter/return"
echo
read -p "==> If you would like the script to download the IPSW file, just press enter/return... " ipsw

ipsw=${ipsw//\\/}
if [ -f "$ipsw" ]; then
	echo "==> Copying IPSW..."
	cp "$ipsw" . 2> /dev/null
fi

echo "==> Checking for IPSW..."
mv *.ipsw restore.ipsw 2> /dev/null
if [ -f restore.ipsw ]; then
	echo "==> IPSW found!"
else
	echo "==> Downloading IPSW..."
	wget -O restore.ipsw https://api.ipsw.me/v4/ipsw/download/$identifier/14G60 -q --show-progress
fi

echo "==> Extracting IPSW..."
unzip -q -d ipsw restore.ipsw

echo "==> Copying iBEC/iBSS..."
cp ipsw/Firmware/dfu/iBEC.$model.RELEASE.im4p ibec.im4p
cp ipsw/Firmware/dfu/iBSS.$model.RELEASE.im4p ibss.im4p

echo "==> Patching iBEC/iBSS..."
bspatch ibec.im4p ibec.patched.im4p patch/ibec_$model.patch
bspatch ibss.im4p ibss.patched.im4p patch/ibss_$model.patch

echo "==> Copying patched iBEC/iBSS to IPSW..."
rm ipsw/Firmware/dfu/iBEC.$model.RELEASE.im4p
rm ipsw/Firmware/dfu/iBSS.$model.RELEASE.im4p
cp ibec.patched.im4p ipsw/Firmware/dfu/iBEC.$model.RELEASE.im4p
cp ibss.patched.im4p ipsw/Firmware/dfu/iBSS.$model.RELEASE.im4p

echo "==> Creating custom IPSW..."
cd ipsw
zip -q ../custom.ipsw -r0 *
cd ..

echo "==> Cleaning up..."
rm -r ibec.im4p ibss.im4p patch restore.ipsw

echo "==> Downloading ipwndfu..."
wget -O ipwndfu.zip https://github.com/MatthewPierson/ipwndfu_public/archive/master.zip -q --show-progress
unzip -q ipwndfu.zip
rm ipwndfu.zip
mv ipwndfu_public-master ipwndfu

function pwn {
	cd ipwndfu
	while :; do
		read -p "==> Please put your device in DFU mode and press enter/return... "

		echo "==> Closing all iTunes/Finder windows..."
		killall iTunes 2> /dev/null
		killall iTunesHelper 2> /dev/null
		osascript -e 'tell application "Finder" to close windows' > /dev/null

		echo "==> Attempting to enter pwned DFU mode..."
		if [ "$1" == --ipwndfu-verbose ]; then
			./ipwndfu -p
			echo
		else
			./ipwndfu -p > /dev/null
		fi
		result=$(lsusb 2> /dev/null | grep -c "checkm8")
		if [ $result -eq 1 ]; then
			break
		fi
		echo "==> Exploit failed. Please exit DFU mode."
	done

	echo "==> Removing signature checks..."
	python rmsigchks.py > /dev/null

	cd ..

	echo "==> Sending test file to device..."
	echo "Hey Apple, how ya feelin' right now? Oh, that's too bad." >> test.txt
	irecovery -f test.txt

	echo "==> Sending patched iBSS/iBEC to device..."
	irecovery -f ibss.patched.im4p
	irecovery -f ibec.patched.im4p

	echo "==> Waiting for device to reconnect..."
	while :; do
		bin/igetnonce &> /dev/null
		if [ $? -eq 0 ]; then
			break
		fi
	done
}

pwn

echo "==> Downloading OTA manifests..."
wget -O manifests.zip https://gitlab.com/devluke/stablea7/raw/master/A7_10.3.3_OTA_Manifests.zip -q --show-progress
unzip -q manifests.zip
rm manifests.zip

function getnonce {
	echo "==> Getting ECID and ApNonce..."
	ecid=$(bin/igetnonce | grep ECID=)
	ecid=${ecid#*ECID=}
	apnonce=$(bin/igetnonce | grep ApNonce=)
	apnonce=${apnonce#*ApNonce=}
}

getnonce

echo "==> Copying OTA manifest..."
cp 10.3.3/BuildManifest_"$identifier"_1033_OTA.plist BuildManifest.plist

if [ $identifier == iPhone6,1 ] || [ $identifier == iPhone6,2 ] || [ $identifier == iPad4,2 ] || [ $identifier == iPad4,5 ]; then
	echo "==> Copying baseband..."
	cp ipsw/Firmware/Mav7Mav8-7.60.00.Release.bbfw baseband.bbfw
	baseband=true
else
	baseband=false
fi

echo "==> Copying SEP..."
if [ $identifier == iPad4,1 ]; then
	cp ipsw/Firmware/all_flash/sep-firmware.j71.RELEASE.im4p sep.im4p
elif [ $identifier == iPad4,2 ]; then
	cp ipsw/Firmware/all_flash/sep-firmware.j72.RELEASE.im4p sep.im4p
elif [ $identifier == iPad4,4 ]; then
	cp ipsw/Firmware/all_flash/sep-firmware.j85.RELEASE.im4p sep.im4p
elif [ $identifier == iPad4,5 ]; then
	cp ipsw/Firmware/all_flash/sep-firmware.j86.RELEASE.im4p sep.im4p
elif [ $identifier == iPhone6,1 ]; then
	cp ipsw/Firmware/all_flash/sep-firmware.n51.RELEASE.im4p sep.im4p
elif [ $identifier == iPhone6,2 ]; then
	cp ipsw/Firmware/all_flash/sep-firmware.n53.RELEASE.im4p sep.im4p
fi

function apticket {
	echo "==> Requesting ticket..."
	bin/tsschecker -e $ecid -d $identifier -s -o -i 9.9.10.3.3 --buildid 14G60 -m BuildManifest.plist --apnonce $apnonce > /dev/null
	mv *.shsh ota.shsh
}

apticket

echo "==> Cleaning up..."
rm -r 10.3.3 ipsw test.txt

function restore {
	echo
	read -p "==> Preparation complete! Press enter/return to restore... "

	echo "==> Restoring device to 10.3.3..."
	status=
	if [ $baseband == true ]; then
		bin/futurerestore -t ota.shsh -s sep.im4p -m BuildManifest.plist -b baseband.bbfw -p BuildManifest.plist custom.ipsw
		status=$?
	else
		bin/futurerestore -t ota.shsh -s sep.im4p -m BuildManifest.plist --no-baseband custom.ipsw
		status=$?
	fi

	if [ $status -ne 0 ]; then
		echo
		echo "==> Restoring failed. Attempting to exit recovery mode..."
		bin/futurerestore --exit-recovery &> /dev/null
		if [ $patch == 1 ]; then
			echo "==> You are using the normal patch which probably caused the restore to fail."
			read -p "==> Press enter/return to try restoring again with the rsu patch... "

			read -p "==> Please exit DFU mode and press enter/return... "
			echo "==> Exiting recovery mode..."
			bin/futurerestore --exit-recovery &> /dev/null

			patch=2
			rm ota.shsh

			binary
			pwn
			getnonce
			apticket
			restore
		else
			exit 1
		fi
	fi
}

restore

echo "==> Deleting work directory..."
cd ..
rm -r StableA7

if [ $patch -eq 2 ]; then
	echo "==> Deleting /rsu. This might ask for your password..."
	sudo rm -r /rsu
fi

echo
echo "==> Restore succeeded! Enjoy 10.3.3!"

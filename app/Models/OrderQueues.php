<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderQueues extends Model
{
    protected $table = 'order_queues';

    protected $fillable = [
        'user_id',
        'mark',
        'status',
        'micro_time',
        'active',
    ];

    public $timestamps = false;
}
<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class EverydayTasksComplete extends Model {
    public $timestamps = false;

    protected $table = 'everyday_tasks_complete';
    protected $fillable = [
        'status',
        'user_id',
        'task_id',
        'type',
        'amount',
        'day',
    ];
}
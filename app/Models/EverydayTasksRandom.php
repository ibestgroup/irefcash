<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class EverydayTasksRandom extends Model {
    public $timestamps = false;

    protected $table = 'everyday_tasks_random';
    protected $fillable = [
        'status',
        'type',
        'user_id',
        'price',
        'day',
    ];

}
<?php
namespace App\Models;
use App\Libraries\Helpers;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SurfingLink extends Model {
    public $timestamps = false;

    protected $table = 'surfing_link';
    protected $fillable = [
        'user_id',
        'status',
        'name',
        'desc',
        'link',
        'price',
        'redirect',
        'vip',
        'unique_ip',
        'visit_day',
        'balance',
        'complete',
        'denied',
        'check',
        'icon',
        'result_price',
    ];

    public function vipClass() {
        if ($this->vip == 1) {
            return 'vip';
        } elseif ($this->vip == 2) {
            return 'vip-2';
        }
    }

    public function reward() {
        $surfInfo = SurfingSetting::find(1);
        $result = $surfInfo->{'price_'.$this->price}*($surfInfo->percent_reward/100);
        return $result;
    }

    public function priceAmount() {
        $surfInfo = SurfingSetting::find(1);
        return $surfInfo->{'price_'.$this->price};
    }

    public function todayComplete() {
        return $this->belongsTo(SurfingComplete::class, 'id', 'link_id')->whereDate('date', Carbon::today())->where('status', 1)->count();
    }

    public function userComplete() {
        return $this->belongsTo(SurfingComplete::class, 'id', 'link_id')->where('user_id', Auth::id())->where('status', 1)->count();
    }

//    public function complete() {
////        return $this->hasMany(SurfingComplete::class, 'id', 'link_id');
////    }
///
    public function completes() {
        return $this->hasMany(SurfingComplete::class, 'link_id', 'id');
    }

    public function completeLastDays() {
        $lastDays = Helpers::getLastNDays(7, 'Y-m-d', false);
        $arrayCompletes = [];

        foreach ($lastDays as $key => $day) {
            $arrayCompletes[$key] = SurfingComplete::where('link_id', $this->id)->whereDate('date', $day)->count();
        }

        return json_encode($arrayCompletes);
    }

    public function surfInfo() {
        return SurfingSetting::find(1);
    }
}
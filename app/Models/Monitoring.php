<?php
namespace App\Models;
use App\Libraries\Helpers;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Monitoring extends Model {
    public $timestamps = false;

    protected $table = 'monitoring';
    protected $fillable = [
        'site_id',
        'site_name',
        'start',
        'min_enter',
        'members',
        'deposits',
        'favicon',
        'link',
        'subdomain',
    ];

}
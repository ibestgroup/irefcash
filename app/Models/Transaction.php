<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

    public $timestamps = false;


    protected $table = 'transaction';
    protected $fillable = [
        'transaction_id',
        'from_user',
        'to_user',
        'type',
        'date',
    ];

    public function info($type = 'payment')
    {
        $type = $this->type;

        $table = new Payments();
        if ($type == 'payment') {
            $table = new Payments();
        } elseif ($type == 'payments_order') {
            $table = new PaymentsOrder();
        } elseif ($type == 'cashout') {
            $table = new Cashout();
        } elseif ($type == 'transaction_earn') {
            $table = new TransactionEarn();
        } elseif ($type == 'transaction_deposit_ad') {
            $table = new TransactionDepositAd();
        }

        return $this->belongsTo($table,'transaction_id','id')->first();
    }

    public function transactionType($meth = 'type') {

        $typeInfo = '';

        switch ($this->type) {
            case 'cashout':
                $typeName = 'Вывод';
                break;
            case 'payment':
                $typeName = 'Пополнение';
                break;
            case 'payments_order':
                $typeName = 'МЛМ Матрицы';
                break;
            case 'transaction_earn':
                $typeName = 'Заработок';

                switch ($this->info()->type) {
                    case 'surfing_reward':
                        $typeInfo = 'Серфинг';
                        break;
                    case 'boost_surfing':
                        $typeInfo = 'Буст серфинга';
                        break;
                    case 'surfing_reward_affiliate':
                        $typeInfo = 'Серфинг(реферальные)';
                        break;
                    case 'surfing_reward_affiliate_ad':
                        $typeInfo = 'Серфинг(реферальные с рекламы)';
                        break;
                    case 'boost_surfing_aff':
                        $typeInfo = 'Буст реферальных серфинга';
                        break;
                }

                break;
            case 'transaction_deposit_ad':
                $typeName = 'Траты на рекламу';
                break;
            default:
                $typeName = 'ololo';
                break;
        }

        if ($meth == 'type')
            return $typeName;
        elseif ($meth == 'typeInfo')
            return $typeInfo;
    }

}
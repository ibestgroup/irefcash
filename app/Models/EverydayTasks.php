<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class EverydayTasks extends Model {
    public $timestamps = false;

    protected $table = 'everyday_tasks';
    protected $fillable = [
        'status',
        'name',
        'type',
        'price',
        'img',
    ];

    public function checkComplete($check = 'count') {
        $day = date('Y-m-d');
        $taskComplete = $this->belongsTo(EverydayTasksComplete::class, 'id', 'task_id');
        if ($check == 'count') {
            return $taskComplete->where('user_id', Auth::id())->where('day', $day)->count();
        } else {
            return $taskComplete->where('user_id', Auth::id())->where('day', $day)->first()->status;
        }
    }
}
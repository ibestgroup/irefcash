<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferalMarket extends Model
{
    protected $table = 'referal_market';
    public $timestamps = false;


    protected $fillable = [
        'user_sell',
        'user_referal',
        'user_buy',
        'price',
        'date',
        'date_buy',
    ];

}

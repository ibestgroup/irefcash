<?php
namespace App\Models;
use App\Libraries\Helpers;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ShortLinkVisit extends Model {
    public $timestamps = false;

    protected $table = 'short_link_visit';
    protected $fillable = [
        'status',
        'link_id',
        'distributor_id',
        'advert_id',
        'date',
        'ip',
    ];

}
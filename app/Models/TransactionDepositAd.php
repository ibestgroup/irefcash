<?php
namespace App\Models;
use App\Libraries\Helpers;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransactionDepositAd extends Model {

    public $timestamps = false;

    protected $table = 'transaction_deposit_ad';
    protected $fillable = [
        'user_id',
        'task_id',
        'type',
        'amount',
        'date',
    ];

}
<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class CoreSiteList extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'site_list';
    protected $fillable = [
        'user_id',
        'subdomain',
        'type',
        'create_date',
        'lang',
        'currency',
    ];

    public static function siteInfo($id) {

        $site = CoreSiteList::where('id', $id)->get()[0];

        Config::set("database.connections.".$site['subdomain'], [
            "host" => "127.0.0.1",
            "database" => $site['subdomain'],
            "username" => 'root',
            "password" => "DrinkVodkaPlayDotka228",
            'driver' => 'mysql'
        ]);

        return SiteInfo::on($site['subdomain'])->first();
    }
}
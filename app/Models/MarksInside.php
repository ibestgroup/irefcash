<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarksInside extends Model
{
    protected $table = 'marks_inside';
    public $timestamps = false;


    protected $fillable = [
        'name',
        'desc',
        'status',
        'level',
        'width',
        'overflow',
        'type',
        'interval',
        'length',
        'boost_surfing',
        'boost_interview',
        'boost_link',
        'boost_surfing_aff',
        'boost_interview_aff',
        'boost_link_aff',
    ];

    public function orders() {
        return $this->hasMany('\App\Models\OrdersMarkInside', 'id', 'mark');
    }

    public function levelInfo() {
        return $this->belongsTo('\App\Models\MarksLevelInfo', 'id', 'id');
    }

    public function round() {

        $marks = MarksElement::where('mark_id', $this->id)->where('status', '>', 0)->count();
        if (!MarksElement::where('mark_id', $this->id)->where('status', 1)->count()) {
            $nextMark = $marks+1;
            return $marks.'->'.$nextMark;
        }
        return $marks;
        foreach ($marks as $key => $mark) {

            if ($this->id == $mark->id) {
                return $key+1;
            }
        }
    }

    public function lastMarks() {

        $marks = MarksElement::where('mark_id', $this->id)->latest('start')->limit(2)->get()->reverse();
        return $marks;

    }
}

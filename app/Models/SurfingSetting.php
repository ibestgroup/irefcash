<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SurfingSetting extends Model {
    public $timestamps = false;

    protected $table = 'surfing_setting';
    protected $fillable = [
        'redirect',
        'price_5',
        'price_10',
        'price_15',
        'price_20',
        'price_30',
        'price_40',
        'price_60',
        'price_80',
        'price_100',
        'percent',
        'vip',
        'vip_2',
        'short_link_price',
        'short_link_reward',
    ];
}
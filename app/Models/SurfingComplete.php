<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SurfingComplete extends Model {
    public $timestamps = false;

    protected $table = 'surfing_complete';
    protected $fillable = [
        'user_id',
        'link_id',
        'date',
        'status',
        'ip',
    ];

}
<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class PaymentsOrder extends Model {
    protected $table = 'payments_order';
    public $timestamps = false;

    protected $fillable = [
        'from_user',
        'to_user',
        'from_wallet',
        'wallet',
        'from_level',
        'level',
        'amount',
        'date',
        'direct',
        'mark',
        'type',
        'mark_element',
    ];

    /**
     * Получает сумму значений столбца amount из таблицы payments_order
     * @param  int $userId - id пользователя
     * @return int         - сумма выплат
     */
    public static function getPayments($userId){
        $userId = intval($userId);
        // $payments = self::select(\DB::raw('SUM(amount) as total'))->where('to_user', $userId)->first()->toArray()['total'];
        $payments = self::select()->where('to_user', $userId)->sum('amount');

        return $payments;
    }

    /**
     * Связь с получателем платежа
     */
    public function reciever()
    {
        return $this->hasOne('\App\Models\Users', 'id', 'to_user');
    }

    /**
     * Связь с отправителем платежа
     */
    public function sender()
    {
        return $this->hasOne('\App\Models\Users', 'id', 'from_user');
    }

    public function payments() {
        return $this->belongsToMany(Payments::class, 'payment');
    }

    public function cashout() {
        return $this->belongsToMany(Cashout::class, 'cashout');
    }
}

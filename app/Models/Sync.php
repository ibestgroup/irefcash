<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Sync extends Model {
    protected $table = 'sync_out_sites';
    public $timestamps = false;

    protected $fillable = [
        'user_social',
        'user_out_site',
        'site_type',
        'site_id',
    ];
}

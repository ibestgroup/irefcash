<?php
namespace App\Models;
use App\Libraries\Helpers;
use App\User;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Interview extends Model {
    public $timestamps = false;

    protected $table = 'interview';
    protected $fillable = [
        'id',
        'user_id',
        'status',
        'name',
        'desc',
        'link',
        'price_result',
        'balance',
        'date',
        'complete',
        'denied',
        'check',
        'vip',
    ];

    public function vipClass() {
        if ($this->vip == 1) {
            return 'vip';
        } elseif ($this->vip == 2) {
            return 'vip-2';
        }
    }

    public function reward() {
        $surfInfo = SurfingSetting::find(1);
        $result = $this->price_result*($surfInfo->percent_reward/100);
        return $result;
    }

    public function priceAmount() {
        $surfInfo = SurfingSetting::find(1);
        return $surfInfo->{'price_'.$this->price};
    }

    public function asks() {
        return $this->hasMany(InterviewAsk::class, 'interview_id', 'id');
    }

    public function author() {
        return User::find($this->user_id);
    }

    public function checkComplete() {
        $count = InterviewComplete::where('interview_id', $this->id)->where('user_id', Auth::id())->count();
        if ($count) {
            return InterviewComplete::where('interview_id', $this->id)->where('user_id', Auth::id())->first();
        } else {
            return 0;
        }
    }

    public function completeLastDays() {
        $lastDays = Helpers::getLastNDays(7, 'Y-m-d', false);
        $arrayCompletes = [];

        foreach ($lastDays as $key => $day) {
            $arrayCompletes[$key] = InterviewComplete::where('interview_id', $this->id)->whereDate('date', $day)->count();
        }

        return json_encode($arrayCompletes);
    }

    public  function completed()
    {
        return InterviewComplete::where(['interview_id' => $this->id, 'status' => 1])->count();
    }

    public function denied()
    {
        return InterviewComplete::where(['interview_id' => $this->id, 'status' => 2])->count();
    }

    public function inProgress()
    {
        return InterviewComplete::where(['interview_id' => $this->id, 'status' => 0])->count();
    }

    public function allCount()
    {
        return InterviewComplete::where('interview_id', $this->id)->count();
    }

}
<?php
namespace App\Models;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class InterviewAsk extends Model {
    public $timestamps = false;

    protected $table = 'interview_ask';
    protected $fillable = [
        'id',
        'interview_id',
        'ask',
        'answer_true',
        'answer_1',
        'answer_2',
        'answer_3',
    ];


}
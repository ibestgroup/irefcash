<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarksElement extends Model
{
    protected $table = 'marks_element';
    public $timestamps = false;


    protected $fillable = [
        'status',
        'mark_id',
        'start',
        'end',
    ];

    public function mark() {
        return $this->belongsTo('\App\Models\MarksInside', 'mark_id', 'id');
    }

    public function orderAdmin() {
        return $this->belongsTo('\App\Models\OrdersMarkInside', 'id', 'mark_element');
    }

    public function sumMoney() {
        return $this->belongsTo('\App\Models\PaymentsOrder', 'id', 'mark_element')->where('type', 'activate')->sum('amount');
    }

    public function activateCount() {
        return $this->belongsTo('\App\Models\OrdersMarkInside', 'id', 'mark_element')->count();
    }

    public function round() {
        $marks = $this->where('mark_id', $this->mark_id)->get();

        foreach ($marks as $key => $mark) {

            if ($this->id == $mark->id) {
                return $key+1;
            }
        }
    }
}

<?php
namespace App\Models;
use App\Libraries\Helpers;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ShortLinkAdvert extends Model {
    public $timestamps = false;

    protected $table = 'short_link_advert';
    protected $fillable = [
        'user_id',
        'status',
        'name',
        'link',
        'balance',
        'date',
    ];

    public function visits() {
        return $this->hasMany(ShortLinkVisit::class, 'advert_id', 'id')->where('ip', $_SERVER['REMOTE_ADDR']);
    }

    public function completeLastDays() {
        $lastDays = Helpers::getLastNDays(7, 'Y-m-d', false);
        $arrayCompletes = [];

        foreach ($lastDays as $key => $day) {
            $arrayCompletes[$key] = ShortLinkVisit::where('advert_id', $this->id)->whereDate('date', $day);
        }

        return json_encode($arrayCompletes);
    }

}
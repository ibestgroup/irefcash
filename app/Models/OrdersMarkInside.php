<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class OrdersMarkInside extends Model
{
    protected $table = 'orders_mark_inside';
    public $timestamps = false;

    protected $fillable = [
        'id_user',
        'parent',
        'mark',
        'mark_element',
        'level',
        'balance',
        'pay_people',
        'status',
        'par_or',
        'date',
        'reinvest',
        'cashout'
    ];

    public function marks() {
        return $this->belongsTo(MarksInside::class, 'mark', 'id');
    }

    public function orderRound() {
        return MarksElement::where('mark_id', $this->mark)->where('id', '<=', $this->mark_element)->count();
    }

//    public function profit() {
//        return MarksElement::where('mark_id', $this->mark)->where('id', '<=', $this->mark_element)->count();
//    }

    public function id_user() {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }

    public function curator() {
        return $this->belongsTo(User::class, 'parent', 'id');
    }

    public function marksInfo() {
        return $this->hasMany(MarksInside::class, 'id', 'mark');
    }
}

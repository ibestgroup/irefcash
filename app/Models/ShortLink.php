<?php
namespace App\Models;
use App\Libraries\Helpers;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ShortLink extends Model {
    public $timestamps = false;

    protected $table = 'short_link';
    protected $fillable = [
        'user_id',
        'link_id',
        'link',
        'visit',
        'monetize',
    ];

    public function completeLastDays() {
        $lastDays = Helpers::getLastNDays(7, 'Y-m-d', false);
        $arrayCompletes = [];

        foreach ($lastDays as $key => $day) {
            $arrayCompletes[$key] = ShortLinkVisit::where('link_id', $this->id)->where('status', 1)->whereDate('date', $day)->count();
        }

        return json_encode($arrayCompletes);
    }

    public function profit() {
        $reward = SurfingSetting::find(1)->short_link_reward;
        return $this->hasMany(ShortLinkVisit::class, 'link_id', 'id')->where('status', 1)->count()*$reward;
    }

}
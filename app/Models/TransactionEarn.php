<?php
namespace App\Models;
use App\Libraries\Helpers;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransactionEarn extends Model {

    public $timestamps = false;

    protected $table = 'transaction_earn';
    protected $fillable = [
        'from_id',
        'from_user_id',
        'user_id',
        'type',
        'amount',
        'date',
    ];

}
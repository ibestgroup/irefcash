<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class InterviewComplete extends Model {
    public $timestamps = false;

    protected $table = 'interview_complete';
    protected $fillable = [
        'user_id',
        'interview_id',
        'date',
        'status',
        'ip',
    ];

    public function endTime() {
        $time = strtotime($this->date) + 900;
        $time = date('Y-m-d H:i:s', $time);
        return $time;
    }

}
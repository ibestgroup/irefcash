<?php

namespace App\Http\Controllers;

use App\Libraries\Helpers;
use App\Models\CoreSiteList;
use App\Models\Monitoring;
use App\Setting;
use Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Teepluss\Theme\Facades\Theme;
use Validator;

class MonitoringController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
        
        $this->middleware('auth');
    }

    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */

    public function index()
    {
        $searchSites = CoreSiteList::all();
        foreach ($searchSites as $item) {
            $checkMonitoring = Monitoring::where('site_id', $item->id)->count();
            if (!$checkMonitoring) {
                $sitePrefix = preg_replace('/_iadminbest/', '', $item->subdomain);
                $siteLink = 'http://' . $sitePrefix . '.iadminlocal.best';

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $siteLink.'/for-monitoring');
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

                $data = curl_exec($ch);
                curl_close($ch);

                $site = json_decode($data);

                if ($data) {
                    Monitoring::create([
                        'site_id' => $site->site_id,
                        'site_name' => $site->name,
                        'start' => $site->start,
                        'min_enter' => $site->min_enter,
                        'members' => $site->members,
                        'deposits' => $site->payments,
                        'favicon' => $site->logo,
                        'link' => $site->result_link,
                        'subdomain' => $site->subdomain,
                    ]);
                }
            }
        }
        $q = 1;
        $sites = Monitoring::all();


        $trending_tags = Helpers::trandingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('Оплачиваемые клики '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('monitoring', compact('trending_tags', 'q', 'sites'))->render();

    }

}

<?php

namespace App\Http\Controllers;

use App\Events\MessagePublished;
use App\Hashtag;
use App\Helpers\MarksHelper;
use App\Libraries\Activate;
use App\Libraries\Helpers;
use App\Models\EverydayTasks;
use App\Models\EverydayTasksComplete;
use App\Models\MarksElement;
use App\Models\MarksInside;
use App\Models\OrderQueues;
use App\Models\OrdersMarkInside;
use App\Models\PaymentsOrder;
use App\Models\Transaction;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Event;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Teepluss\Theme\Facades\Theme;
use Validator;

class TransactionController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
        
        $this->middleware('auth');
    }

    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index()
    {
        $transaction = Transaction::where('from_user', Auth::id())
                                    ->orWhere('to_user', Auth::id())
                                    ->orderByDesc('id')
                                    ->paginate(20);

        $trending_tags = Helpers::trandingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('МЛМ Матрицы '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('transaction.transaction', compact('transaction', 'trending_tags'))->render();
    }



}

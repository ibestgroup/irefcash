<?php

namespace App\Http\Controllers;

use App\Events\MessagePublished;
use App\Hashtag;
use App\Helpers\MarksHelper;
use App\Libraries\Activate;
use App\Libraries\Helpers;
use App\Models\EverydayTasks;
use App\Models\EverydayTasksComplete;
use App\Models\Interview;
use App\Models\InterviewAsk;
use App\Models\InterviewComplete;
use App\Models\MarksElement;
use App\Models\MarksInside;
use App\Models\OrdersMarkInside;
use App\Models\PaymentsOrder;
use App\Models\SurfingComplete;
use App\Models\SurfingLink;
use App\Models\SurfingSetting;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Event;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Teepluss\Theme\Facades\Theme;
use Validator;

class InterviewController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->middleware('auth');
    }

    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */

    public function index()
    {
        $surfInfo = SurfingSetting::find(1);

        $activeTest = InterviewComplete::where('user_id', Auth::id())->where('status', 0)->count();
        if ($activeTest) {

            $dateEnd = strtotime(InterviewComplete::where('user_id', Auth::id())->where('status',
                    0)->first()->date) + 900;
            $test = InterviewComplete::where('user_id', Auth::id())->where('status', 0)->first();
            if (time() > $dateEnd) {

                $interviewActive = Interview::where('id', $test->interview_id)->first();
                Interview::where('id', $test->interview_id)->increment('balance', $interviewActive->price_result);
                $test->update(['status' => 2]);

                $activeTest = 0;
            } else {
                $activeTest = Interview::where('id', $test->interview_id)->first();
            }
        }

        $completes = InterviewComplete::where('user_id', Auth::id())->where('status', '>', 0)->pluck('interview_id');
        $interview = Interview::whereNotIn('id', $completes)->where('status', 1)->orderBy('vip', 'desc')->get();


        $trending_tags = Helpers::trandingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('Опросы ' . Setting::get('title_seperator') . ' ' . Setting::get('site_title') . ' ' . Setting::get('title_seperator') . ' ' . Setting::get('site_tagline'));

        return $theme->scope('interview.index',
            compact('trending_tags', 'surfInfo', 'interview', 'activeTest'))->render();
    }

    public function addInterviewPage()
    {
        $surfInfo = SurfingSetting::find(1);

        $trending_tags = Helpers::trandingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('Опросы ' . Setting::get('title_seperator') . ' ' . Setting::get('site_title') . ' ' . Setting::get('title_seperator') . ' ' . Setting::get('site_tagline'));

        return $theme->scope('interview.addInterview', compact('trending_tags', 'surfInfo'))->render();
    }

    public function myInterviewPage()
    {
        $surfInfo = SurfingSetting::find(1);
        $interview = Interview::where('user_id', Auth::id())->get();

        $trending_tags = Helpers::trandingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('Опросы ' . Setting::get('title_seperator') . ' ' . Setting::get('site_title') . ' ' . Setting::get('title_seperator') . ' ' . Setting::get('site_tagline'));

        return $theme->scope('interview.myInterview', compact('trending_tags', 'surfInfo', 'interview'))->render();
    }

    public function addInterview(Request $request)
    {

        $prices = Helpers::prices();

        $element['name'] = $request->name;
        $element['desc'] = $request->desc;
        $element['price'] = $request->price;
        $element['vip'] = (isset($request->vip)) ? $request->vip : 0;


        $errMessage = '';
        foreach ($element as $key => $item) {
            if ($item == '') {
                $errMessage = $key . 'Заполните все поля <br>';
            }
        }

        $element['link'] = $request->link;

        if ($request->price < 0.3) {
            $errMessage .= 'Минимальная сумма 0.3 <br>';
        }

        if (!empty($errMessage)) {
            return Response::json(['type' => 'error', 'message' => $errMessage], 200);
        }

        $surfInfo = SurfingSetting::find(1);
        $resultPrice = $element['price'];

        if ($element['vip']) {
            if ($element['vip'] == 1) {
                $resultPrice += $surfInfo->vip;
            } elseif ($element['vip'] == 2) {
                $resultPrice += $surfInfo->vip_2;
            }
        }

        $interview = Interview::create([
            'status' => 0,
            'user_id' => Auth::id(),
            'name' => $element['name'],
            'desc' => $element['desc'],
            'link' => $element['link'],
            'price_result' => $resultPrice,
            'vip' => $element['vip'],
            'balance' => 0,
            'complete' => 0,
            'denied' => 0,
            'check' => 0,
            'date' => Carbon::now(),
        ]);

        foreach ($request->asks as $ask) {
            if ($ask['ask'] != '') {
                InterviewAsk::create([
                    'interview_id' => $interview->id,
                    'ask' => $ask['ask'],
                    'answer_true' => $ask['answer_true'],
                    'answer_1' => $ask['answer_1'],
                    'answer_2' => $ask['answer_2'],
                    'answer_3' => $ask['answer_3'],
                ]);
            }
        }


        return Response::json(['type' => 'success', 'message' => 'Задание успешно создано', 'link' => '/surfing'], 200);
    }

    public function depositInterview(Request $request)
    {
        DB::beginTransaction();
        if (empty($request->amount)) {
            return Response::json(['type' => 'error', 'message' => 'Введите сумму', 'link' => '/surfing'], 200);
        }
        $request->amount *= 1;
        if (!is_int($request->amount)) {
            return Response::json([
                'type' => 'error',
                'message' => 'Вы должны ввести целое число',
                'link' => '/surfing'
            ], 200);
        }

        $amount = (int)$request->amount;
        $linkId = (int)$request->link_id;

        if ($amount < 10) {
            return Response::json(['type' => 'error', 'message' => 'Минимальная сумма 10 руб.', 'link' => '/surfing'],
                200);
        }

        if (Auth::user()->balance < $amount) {
            return Response::json([
                'type' => 'error',
                'message' => 'На балансе недостаточно средств',
                'link' => '/surfing'
            ], 200);
        }

        $checkLink = Interview::whereId($linkId)->count();
        if (!$checkLink) {
            return Response::json(['type' => 'error', 'message' => 'Ссылка не существует', 'link' => '/surfing'], 200);
        }
        $link = Interview::whereId($linkId)->first();

        if ($link->user_id != Auth::id()) {
            return Response::json(['type' => 'error', 'message' => 'Кажется это не ваша ссылка.', 'link' => '/surfing'],
                200);
        }


        User::whereId(Auth::id())->decrement('balance', $amount);
        Interview::whereId($linkId)->increment('balance', $amount);

        // transaction here +++

        Helpers::createTransaction([
            'table' => 'transaction_deposit_ad',
            'type' => 'interview',
            'task_id' => $linkId,
            'from_user_id' => Auth::id(),
            'user_id' => 0,
            'amount' => $amount,
        ]);

        DB::commit();
        return Response::json(['type' => 'success', 'message' => 'Баланс пополнен', 'link' => '/surfing'], 200);
    }

    public function playInterview(Request $request)
    {
        $linkId = (int)$request->link_id;
        $link = Interview::find($linkId);

        if ($link->balance < $link->price_result) {
            return Response::json([
                'type' => 'error',
                'message' => 'Недостаточно средств для запуска. Пополните баланс',
                'link' => '/surfing'
            ], 200);
        }

        if ($link->user_id != Auth::id()) {
            return Response::json(['type' => 'error', 'message' => 'Кажется это не ваша ссылка.', 'link' => '/surfing'],
                200);
        }

        if ($link->status == 0) {
            $status = 1;
            $message = "Ссылка запущена в ротацию";
        } elseif ($link->status == 1) {
            $status = 0;
            $message = "Ротация ссылки приостановлена";
        }

        Interview::whereId($linkId)->update([
            'status' => $status,
        ]);

        return Response::json(['type' => 'success', 'message' => $message, 'link' => '/interview'], 200);

    }

    public function viewInterview($id)
    {

        try {
            $interview = Interview::findOrFail($id);
            $message = false;
        } catch (\Exception $e) {
            $message = 'Задания не существует';
        }


        $trending_tags = Helpers::trandingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('Опросы ' . Setting::get('title_seperator') . ' ' . Setting::get('site_title') . ' ' . Setting::get('title_seperator') . ' ' . Setting::get('site_tagline'));

        return $theme->scope('interview.interview', compact('interview', 'message'))->render();
    }

    public function startInterview(Request $request)
    {

        DB::beginTransaction();
        try {
            if (InterviewComplete::where('user_id', Auth::id())->where('status', 0)->count()) {
                $dateEnd = strtotime(InterviewComplete::where('user_id', Auth::id())->where('status',
                        0)->first()->date) + 900;
                if (time() > $dateEnd) {

                    $test = InterviewComplete::where('user_id', Auth::id())->where('status', 0)->first();
                    $interviewActive = Interview::where('id', $test->interview_id)->first();
                    Interview::where('id', $test->interview_id)->increment('balance', $interviewActive->price_result);
                    $test->update(['status' => 2]);

                } else {
                    return Response::json(['type' => 'error', 'message' => 'У вас есть незавершенные тесты']);
                }
            }
            $interview = Interview::where('id', $request->id)->firstOrFail();
        } catch (\Exception $e) {
            return Response::json(['type' => 'error', 'message' => 'Задания не существует']);
        }

        $checkComplete = InterviewComplete::where('user_id', Auth::id())
            ->where('interview_id', $request->id)
            ->first();

        if ($checkComplete) {
            if ($checkComplete->status == 0) {
                return Response::json(['type' => 'error', 'message' => 'Вы уже проходили этот тест']);
            } elseif ($checkComplete->status == 1) {
                return Response::json(['type' => 'error', 'message' => 'Тест уже пройден']);
            } elseif ($checkComplete->status == 2) {
                return Response::json(['type' => 'error', 'message' => 'Тест уже пройден неудачно']);
            }
        }

        if ($interview->status == 0) {
            return Response::json(['type' => 'error', 'message' => 'Это задание уже недоступно для выполнения']);
        }


        Interview::find($interview->id)->decrement('balance', $interview->price_result);

        $interviewNow = Interview::find($interview->id);
        if ($interviewNow->balance < $interviewNow->price_result) {
            SurfingLink::find($interview->id)->update(['status' => 0]);
        }

        Interview::find($interview->id)->increment('check');

        InterviewComplete::create([
            'user_id' => Auth::id(),
            'interview_id' => $request->id,
            'date' => Carbon::now(),
            'status' => 0,
            'ip' => $_SERVER['REMOTE_ADDR'],
        ]);

        DB::commit();
        return Response::json(['type' => 'success', 'message' => 'Вы начали проходить тест']);

    }

    public function checkInterview(Request $request)
    {

    }

    public function getReward(Request $request)
    {

        DB::beginTransaction();
        try {
            $interview = Interview::findOrFail($request->interview_id);
            $interviewComplete = InterviewComplete::where('interview_id', $request->interview_id)->where('user_id',
                Auth::id())->firstOrFail();
        } catch (\Exception $e) {
            return Response::json(['type' => 'error', 'message' => 'Что-то пошло не так'], 200);
        }

        if ($interviewComplete->status > 0) {
            return Response::json(['type' => 'error', 'message' => 'Тест уже завершен'], 200);
        }

        if (time() > strtotime($interviewComplete->date) + 900) {
            InterviewComplete::where('interview_id', $interview->id)->where('user_id',
                Auth::id())->update(['status' => 2]);
            Interview::find($interview->id)->increment('balance', $interview->price_result);
            Interview::find($interview->id)->increment('denied');
            Interview::find($interview->id)->decrement('check');
            DB::commit();
            return Response::json(['type' => 'error', 'message' => 'Время истекло'], 200);
        }

        $answers = $request->pair;

        foreach ($answers as $key => $item) {
            $trueAnswer = InterviewAsk::find($item['ask'])->answer_true;
            if ($item['answer'] != $trueAnswer) {
                InterviewComplete::where('interview_id', $interview->id)->where('user_id',
                    Auth::id())->update(['status' => 2]);
                Interview::find($interview->id)->increment('balance', $interview->price_result);
                Interview::find($interview->id)->increment('denied');
                Interview::find($interview->id)->decrement('check');
                DB::commit();
                return Response::json(['type' => 'error', 'message' => 'Тест пройден неверно'], 200);
            }
        }


        $reward = $interview->reward();
        $affiliateRewardAd = $interview->price_result / 10;
        $affiliateRewardUs = $reward / 10;


        // transaciton here +++

        InterviewComplete::where('interview_id', $interview->id)->where('user_id', Auth::id())->update(['status' => 1]);
        $author = User::find($interview->user_id);
        User::whereId(Auth::id())->increment('balance', $reward);
        Helpers::createTransaction([
            'table' => 'transaction_earn',
            'type' => 'surfing_reward',
            'task_id' => $interview->id,
            'from_user_id' => $interview->user_id,
            'user_id' => Auth::id(),
            'amount' => $reward,
        ]);
        User::whereId(Auth::user()->refer)->increment('balance', $affiliateRewardUs);
        Helpers::createTransaction([
            'table' => 'transaction_earn',
            'type' => 'surfing_reward_affiliate',
            'task_id' => $interview->id,
            'from_user_id' => Auth::id(),
            'user_id' => Auth::user()->refer,
            'amount' => $affiliateRewardUs,
        ]);
        User::whereId($author->refer)->increment('balance', $affiliateRewardAd);
        Helpers::createTransaction([
            'table' => 'transaction_earn',
            'type' => 'surfing_reward_affiliate_add',
            'task_id' => $interview->id,
            'from_user_id' => $interview->user_id,
            'user_id' => $author->refer,
            'amount' => $affiliateRewardAd,
        ]);

        Helpers::checkBoost(Auth::id(), 'boost_interview', $reward, $interview->id);
        Helpers::checkBoost(Auth::user()->refer, 'boost_interview_aff', $affiliateRewardUs, $interview->id);

        Interview::find($interview->id)->increment('complete');
        Interview::find($interview->id)->decrement('check');

        DB::commit();
        return Response::json(['type' => 'success', 'message' => "Тест пройден. Вы получили $reward"], 200);


    }

}

<?php

namespace App\Http\Controllers;

use App\Libraries\Helpers;
use App\Libraries\PayTM;
use App\Models\Payments;
use App\Models\PaymentsCrypto;
use App\Models\PaymentSystems;
use App\Models\SiteInfo;
use App\Models\Users;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Libraries\Domain;
use Illuminate\Http\RedirectResponse;
use App\Libraries\CoinPaymentsAPI;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;


class BalanceController extends Controller
{

    public $value = 0;
    static public $encryptionMethod = 'AES-256-CBC';
    static public $iv = '1sbbbsdfgdfhsdgf'; // just use it as is.. initial vector

    public function balance(Request $request)
    {

        $m_orderid = Payments::count() + 1;
        $m_orderid = (string)$m_orderid;

        $system = $request->input('system');
        if (in_array($system, [1,2,114])) {

            $m_shop = config('paysystems.payeer.id');
            $m_amount = number_format($request->input('amount'), 2, '.', '');
            $m_curr = 'RUB';
            $m_desc = base64_encode('Пополнение iref.cash');
            $m_key = config('paysystems.payeer.password');

            $arHash = array(
                $m_shop,
                $m_orderid,
                $m_amount,
                $m_curr,
                $m_desc,
            );

            $arParams = array(
                'success_url' => 'http://iref.cash/profile',
                'reference' => array(
                    'user_id' => Auth::id()
                ),
            );

            $key = md5(config('paysystems.payeer.password') . $m_orderid);
            $m_params = @urlencode(base64_encode(openssl_encrypt(json_encode($arParams), 'AES-256-CBC', $key, OPENSSL_RAW_DATA)));

            $arHash[] = $m_params;

            $arHash[] = $m_key;
            $sign = strtoupper(hash('sha256', implode(":", $arHash)));

            $redirect = 'https://payeer.com/merchant/?m_shop=' . $m_shop
                . '&m_orderid=' . $m_orderid
                . '&m_amount=' . $m_amount
                . '&m_curr=' . $m_curr
                . '&m_desc=' . urlencode($m_desc)
                . '&m_sign=' . $sign
                . '&m_params=' . urlencode($m_params)
                . '&m_cipher_method=AES-256-CBC&m_process=send'
            ;

            return redirect($redirect);

        } else if ($system == 3) {

            $merchant_id = config('paysystems.freekassa.id');
            $secret_word = config('paysystems.freekassa.password');

            $amount = (int)$request->input('amount');

            $sign = md5("$merchant_id:$amount:$secret_word:RUB:$m_orderid");
            $redirect = 'https://pay.freekassa.ru/?'.
                'm=' . $merchant_id . '&' .
                'oa=' . $amount . '&' .
                'currency=RUB&' .
                'o=' . $m_orderid . '&' .
                's=' . $sign . '&' .
                'us_order=' . $m_orderid . '&' .
                'us_id=' . Auth::id() . '&';
            return redirect($redirect);
        } elseif (in_array($system, [700,701,702,703])) {

//            $paymentSystem = PaymentSystems::where('code', $system)->first();
//            if ($paymentSystem->currency != Helpers::siteCurrency('number')) {
//                exit('Ошибка');
//            }

            $amountResult = $request->input('amount')/100*(100+4);
//            $amountResult = $request->input('amount')/100*(100+$paymentSystem->input_fee);

            switch($system) {
                case 700:
                    $paymentMethod = 'yandex';
                    break;
                case 701:
                    $paymentMethod = 'qiwi';
                    break;
                case 702:
                    $paymentMethod = 'visamaster.rur';
                    break;
                case 703:
                    $paymentMethod = 'advcash.usd';
                    break;
                default:
                    $paymentMethod = 'yandex';
            }

            $clientID = 2934;
            $secret = "DannaDiD19";
            $currency = 'RUR';
            $orderId = Auth::id() . '-' . $m_orderid . '-' . rand(1, 999999);

            $data = [
                "CLIENT_ID" => $clientID,
                "INVOICE_ID" => $orderId,
                "AMOUNT" => $amountResult,
                "CURRENCY" => $currency,
                "PAYMENT_CURRENCY" => $paymentMethod,
                "DESCRIPTION" => 'Пополнение баланса на iref.cash Пользователь #' . Auth::id(),
                "SUCCESS_URL" => "https://iref.cash",
                "FAIL_URL" => "https://iref.cash",
                "STATUS_URL" => "https://iref.cash/checkpayobm?payment_id=" . $orderId
            ];

            $sign = base64_encode(md5($secret . base64_encode(sha1(implode("", $data), true)) . $secret, true));

            $data["SIGN_ORDER"] = implode(";", array_keys($data));
            $data["SIGN"] = $sign;

            echo '
                <form id="myForm" action="https://acquiring.obmenka.ua/acs" method="POST">
                    <input type="hidden" name="CLIENT_ID" value="'.$data['CLIENT_ID'].'" />
                    <input type="hidden" name="SIGN" value="'.$data['SIGN'].'" />
                    <input type="hidden" name="SIGN_ORDER" value="'.$data['SIGN_ORDER'].'" />
                    <input type="hidden" name="INVOICE_ID" value="'.$data['INVOICE_ID'].'" />
                    <input type="hidden" name="AMOUNT" value="'.$data['AMOUNT'].'" />
                    <input type="hidden" name="CURRENCY" value="'.$data['CURRENCY'].'" />
                    <input type="hidden" name="PAYMENT_CURRENCY" value="'.$data['PAYMENT_CURRENCY'].'" />
                    <input type="hidden" name="DESCRIPTION" value="'.$data['DESCRIPTION'].'" />
                    <input type="hidden" name="SUCCESS_URL" value="'.$data['SUCCESS_URL'].'" />
                    <input type="hidden" name="FAIL_URL" value="'.$data['FAIL_URL'].'" />
                    <input type="hidden" name="STATUS_URL" value="'.$data['STATUS_URL'].'" />
                </form>
                
                <script type="text/javascript">
                    document.getElementById("myForm").submit();
                </script>
            ';
        }
    }
}

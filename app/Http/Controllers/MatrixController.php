<?php

namespace App\Http\Controllers;

use App\Events\MessagePublished;
use App\Hashtag;
use App\Helpers\MarksHelper;
use App\Libraries\Activate;
use App\Libraries\Helpers;
use App\Models\EverydayTasks;
use App\Models\EverydayTasksComplete;
use App\Models\MarksElement;
use App\Models\MarksInside;
use App\Models\OrderQueues;
use App\Models\OrdersMarkInside;
use App\Models\PaymentsOrder;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Event;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Teepluss\Theme\Facades\Theme;
use Validator;

class MatrixController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
        
        $this->middleware('auth');
    }

    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index($id = null)
    {
        MarksHelper::checkRounds();
        $marks = MarksElement::query();
        $marksArray = MarksInside::query();
        if ($id != null) {
            $marks = $marks->where('mark_id', $id);
            $marksArray = $marksArray->where('id', $id);
        }
        $marksArray = $marksArray->get();
        $marks = $marks->get();


        $trending_tags = Hashtag::orderBy('count', 'desc')->get();
        if (count($trending_tags) > 0) {
            if (count($trending_tags) > (int) Setting::get('min_items_page')) {
                $trending_tags = $trending_tags->random((int) Setting::get('min_items_page'));
            }
        } else {
            $trending_tags = '';
        }

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('МЛМ Матрицы '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('matrix.index', compact('trending_tags', 'marks', 'marksArray'))->render();
    }

    public static function rounds($id) {

        $markElements = MarksElement::where('mark_id', $id)->get();

        $trending_tags = Helpers::trandingTags();
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('МЛМ Матрицы '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('matrix.rounds', compact('id', 'markElements', 'trending_tags'))->render();
    }

    public static function activate(Request $request) {


        if (MarksHelper::marksError()) {
            return Response::json([
                "success" => false,
                "message" => "Ошибка #203655. Есть ошибка в одном из маркетингов. Перейдите в админ панель, чтобы исправить её.",
            ], 200);
        }

        $mark = $request->input('mark');

        $markElement = MarksElement::where('mark_id', $mark)->where('status', 1)->first();
        if ($markElement) {
            $end = strtotime($markElement->end);
            $now = time();
            if ($end < $now) {
//                MarksElement::find($markElement->id)->update(['status' => 2]);
                return Response::json(['type' => 'error', 'message' => 'Маркетинг уже завершился. Попробуйте в следующем круге.'], 200);
            }
        } else {
            return Response::json(['type' => 'error', 'message' => 'Маркетинг не активен'], 200);
        }

        if ($request->input('us_id')) {
            $us_id = $request->input('us_id');
        } else {
            $us_id = Auth::id();
        }

        if ($request->input('type') == 'point') {
            $type = 'point';
        } else {
            $type = 'balance';
        }



        DB::beginTransaction();
        $user = User::find($us_id);



        if ($type == 'point') {
            $resultBalance = $user->balance_point;
            $resultPrice = MarksHelper::calcMarkPrice($mark) * 100;
            $resultColumn = 'balance_point';
            $message = 'Недостаточно ключей';
        } else {
            $resultBalance = $user->balance;
            $resultPrice = MarksHelper::calcMarkPrice($mark);
            $resultColumn = 'balance';
            $message = 'Недостаточно средств';
        }

        if ($resultBalance < $resultPrice) {
            return Response::json(['type' => 'error', 'message' => $message], 200);
        }

        try {

            $microtime = microtime(true);
            User::whereId($us_id)->decrement($resultColumn, $resultPrice);

            \App\Jobs\Activate::dispatch($us_id, $mark, 'free');

            DB::commit();

            return Response::json([
                "type" => "success",
                "message" => "Активация добавлена в очередь.",
            ], 200);

        } catch (\Exception $e) {
            DB::rollBack();
        }


//        $activate = new Activate();
//        try {
//            $activate->activate($us_id, $request->input('mark'), 0 , 'activate');
//
//            DB::beginTransaction();
//            DB::commit();
//            return Response::json([
//                "type" => "success",
//                "message" => "Тариф активирован"
//            ], 200);
//        } catch (\Exception $e) {
//            DB::rollBack();
//
//            DB::beginTransaction();
//            DB::commit();
//            return Response::json([
//                "type" => "error",
//                "message" => $e->getMessage(),
//
//            ], 200);
//        }
    }

    public function treeOrder($id) {

        $order = OrdersMarkInside::find($id);
        $lastOrders = OrdersMarkInside::where('mark_element', $order->mark_element)->latest('date')->limit(10)->get();
        $tarifStart = true;
        if ($order->id == $order->par_or) {
            $paymentsOrder = PaymentsOrder::where('mark_element', $order->mark_element)->where('wallet', '!=', 'to_balance')->where('to_user', '!=', 1)->orderBy('id', 'desc')->paginate(10);
        } else {
            $tarifStart = false;
            $paymentsOrder = PaymentsOrder::where('to_user', '!=', 1)
                                        ->where('wallet', $id)
                                        ->orderBy('id', 'desc')
                                        ->paginate(10);
        }

        $markElement = MarksElement::find($order->mark_element);

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('Дерево тарифа '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('matrix.treeOrder', compact('id', 'paymentsOrder', 'tarifStart', 'lastOrders', 'markElement'))->render();
    }

    public function treeOrderFull($id)
    {
        return view('treeOrderFull', compact('id'))->render();
    }

    public function treeTransaction($id)
    {
        $order = OrdersMarkInside::find($id);
        $tarifStart = true;
        if ($order->id == $order->par_or) {
            $paymentsOrder = PaymentsOrder::where('mark_element', $order->mark_element)->where('to_user', '!=', 1)->where('wallet', '!=', 'to_balance')->orderBy('id', 'desc')->paginate(25);
        } else {
            $tarifStart = false;
            $paymentsOrder = PaymentsOrder::where('wallet', $id)->where('to_user', '!=', 1)->where('wallet', '!=', 'to_balance')->orderBy('id', 'desc')->paginate(25);
        }

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('Транзакции по тарифу '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('matrix.treeTransaction', compact('id', 'paymentsOrder', 'tarifStart'))->render();
    }

}

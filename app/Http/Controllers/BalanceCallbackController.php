<?php

namespace App\Http\Controllers;

//use App\Helpers\AffiliateHelper;
//use App\Libraries\CPayeer;
use App\Libraries\Helpers;
//use App\Libraries\PartnerCore;
//use App\Models\Affilate;
//use App\Models\BonusPayment;
//use App\Models\CashoutCrypto;
//use App\Models\CoreParking;
use App\Models\CoreSiteList;
//use App\Models\Currency;
//use App\Models\HashPower;
//use App\Models\Notice;
//use App\Models\PaymentFinestCore;
use App\Models\Payments;
//use App\Models\PaymentsCrypto;
//use App\Models\SiteBalance;
//use App\Models\SiteInfo;
use App\Models\Transaction;
//use App\Models\Users;
//use Auth;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
//use App\Libraries\Domain;
use Illuminate\Http\RedirectResponse;
//use App\Libraries\CoinPaymentsAPI;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;
use Ixudra\Curl\Facades\Curl;
use Ixudra\Curl\CurlService;

class BalanceCallbackController extends Controller
{

    public function checkPay(Request $request) {
        // Отклоняем запросы с IP-адресов, которые не принадлежат Payeer
        if (!Helpers::checkWhitelist('payeer')) return;

        if (isset($request->m_operation_id) && isset($request->m_sign)) {

            $m_key = config('paymentsystems.payeer.password');
            // Формируем массив для генерации подписи
            $arHash = array(
                $request->m_operation_id,
                $request->m_operation_ps,
                $request->m_operation_date,
                $request->m_operation_pay_date,
                $request->m_shop,
                $request->m_orderid,
                $request->m_amount,
                $request->m_curr,
                $request->m_desc,
                $request->m_status
            );

            // Если были переданы дополнительные параметры, то добавляем их в массив
            if (isset($request->m_params)) {
                $arHash[] = $request->m_params;
            }

            // Добавляем в массив секретный ключ
            $arHash[] = $m_key;

            // Формируем подпись
            $sign_hash = strtoupper(hash('sha256', implode(':', $arHash)));

            $myParams = json_decode($request->m_params);

            // Если подписи совпадают и статус платежа “Выполнен”
            if ($request->m_sign == $sign_hash && $request->m_status == 'success')
            {
                $paymentCount = Payments::where('payment_id', $request->m_operation_id)->count();

                if ($paymentCount == 0) {

                    $resultAmount = $request->m_amount;
                    \App\User::where('id', $myParams->reference->user_id)->increment('balance', $resultAmount);

                    $payment = Payments::create([
                        'user_id' => $myParams->reference->user_id,
                        'payment_id' => $request->m_operation_id,
                        'amount' => $request->m_amount,
                        'date' => Carbon::now(),
                        'payment_system' => 1,
                    ]);

                    Transaction::create([
                        'transaction_id' => $payment->id,
                        'from_user' => 0,
                        'to_user' => $myParams->reference->user_id,
                        'type' => 'payment',
                        'date' => Carbon::now(),
                    ]);

                    exit($request->m_orderid.'|success');
                }

                exit($request->m_orderid.'|success');

            }

            exit($request->m_orderid.'|error');

        }
    }

    public function checkPayFK(Request $request) {
        $merchant_secret = config('paymentsystems.freekassa.password');

        if (!Helpers::checkWhitelist('freekassa')) {
            die("hacking attempt!");
        }

        $sign = md5($request->MERCHANT_ID.':'.$request->AMOUNT.':'.$merchant_secret.':'.$request->MERCHANT_ORDER_ID);
        if ($sign == $request->SIGN) {

            $userID = $request->us_id;
            $operation = $request->intid;
            $amount = number_format($request->AMOUNT, 2, '.', '');
            $check_operation = Payments::where('payment_id', $operation)->count();


            if (!$check_operation) {

                User::where('id', $userID)->increment('balance', $amount);

                $payment = Payments::create([
                    'user_id' => (int)$userID,
                    'payment_id' => $operation,
                    'amount' => $amount,
                    'date' => Carbon::now(),
                    'payment_system' => 750,
                ]);

                Transaction::create([
                    'transaction_id' => $payment->id,
                    'from_user' => 0,
                    'to_user' => (int)$userID,
                    'type' => 'payment',
                    'date' => Carbon::now(),
                ]);

                die('YES');
            } else {
                die('NO');
            }
        } else {
            die('NO');
        }
    }

    public function checkPayObm(Request $request) {
        $paymentId = $request->input('payment_id');
        $transactionInfo = explode('-', $request->input('payment_id'));
        $userID = $transactionInfo[0];
        $paymentID = $transactionInfo[1];

        $salt = 'DannaDiD19';
        $data = [
            'payment_id' => $paymentId,
        ];

        $data = json_encode($data);

        $dpaySecure = base64_encode(md5($salt . base64_encode(sha1($data, true)) . $salt, true));

        $curlService = new CurlService();
        $answer = json_decode($curlService->to('https://acquiring_api.obmenka.ua/api/einvoice/status')
            ->withContentType('application/json')
            ->withHeaders( array(
                'DPAY_CLIENT: 2934',
                'DPAY_SECURE: '.$dpaySecure ) )
            ->withData($data)
            ->post());

        $checkPayment = Payments::where('payment_id', $paymentID)->count();
        if ($checkPayment) {
            exit('Эта транзакция уже выполнена');
        } else {
            if ($answer->status == 'PAYED' or $answer->status == 'FINISHED' ) {

                User::where('id', $userID)->increment('balance', $answer->accrual_amount);

                $payment = Payments::create([
                    'user_id' => (int)$userID,
                    'payment_id' => $paymentID,
                    'amount' => $answer->accrual_amount,
                    'date' => Carbon::now(),
                    'payment_system' => 996,
                ]);

                Transaction::create([
                    'transaction_id' => $payment->id,
                    'from_user' => 0,
                    'to_user' => (int)$userID,
                    'type' => 'payment',
                    'date' => Carbon::now(),
                ]);
            }
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Events\MessagePublished;
use App\Hashtag;
use App\Helpers\MarksHelper;
use App\Libraries\Activate;
use App\Libraries\Helpers;
use App\Models\EverydayTasks;
use App\Models\EverydayTasksComplete;
use App\Models\MarksElement;
use App\Models\MarksInside;
use App\Models\OrdersMarkInside;
use App\Models\PaymentsOrder;
use App\Models\SurfingComplete;
use App\Models\SurfingLink;
use App\Models\SurfingSetting;
use App\Models\Transaction;
use App\Models\TransactionDepositAd;
use App\Models\TransactionEarn;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Event;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Teepluss\Theme\Facades\Theme;
use Validator;

class SurfingController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
        
        $this->middleware('auth');
    }

    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */

    public function index()
    {
        $surfInfo = SurfingSetting::find(1);

        $completes = SurfingComplete::where('user_id', Auth::id())->whereDate('date', Carbon::today())->where('status', 1)->pluck('link_id');
        $surfLinks = SurfingLink::has('completes', '<', DB::raw('visit_day'))->whereNotIn('id', $completes)->where('status', 1)->orderBy('vip', 'desc')->get();

        $trending_tags = Helpers::trandingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('Оплачиваемые клики '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('surfing.index', compact('trending_tags', 'surfInfo', 'surfLinks'))->render();
    }

    public function addLinkPage()
    {
        $surfInfo = SurfingSetting::find(1);

        $trending_tags = Helpers::trandingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('Оплачиваемые клики '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('surfing.addLink', compact('trending_tags', 'surfInfo'))->render();
    }

    public function myLinkPage()
    {
        $surfInfo = SurfingSetting::find(1);
        $surfLinks = SurfingLink::where('user_id', Auth::id())->get();

        $trending_tags = Helpers::trandingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('Оплачиваемые клики '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('surfing.myLink', compact('trending_tags', 'surfInfo', 'surfLinks'))->render();
    }

    public function addLink(Request $request) {

        $prices = Helpers::prices();

        $element['name'] = $request->name;
        $element['desc'] = $request->desc;
        $element['link'] = $request->link;
        $element['visit_day'] = $request->visit_day;
        $element['price'] = $request->price;
        $element['redirect'] = (isset($request->redirect))?$request->redirect:0;
        $element['vip'] = (isset($request->vip))?$request->vip:0;
        $element['unique_ip'] = (isset($request->unique_ip))?$request->unique_ip:0;

        foreach ($element as $key => $item) {
            if ($item == '') {
                $errMessage = 'Заполните все поля <br>';
            }
        }

        if (!in_array($element['price'], $prices)) {
            $errMessage .= 'Не правильно указано время просмотра';
        }


        if (!empty($errMessage)) {
            return Response::json(['type' => 'error', 'message' => $errMessage], 200);
        }

        $surfInfo = SurfingSetting::find(1);
        $resultPrice = $surfInfo->{'price_'.$element['price']};
        if ($element['redirect']) {
            $resultPrice += $surfInfo->redirect;
        }

        if ($element['vip']) {
            if ($element['vip'] == 1) {
                $resultPrice += $surfInfo->vip;
            } elseif ($element['vip'] == 2) {
                $resultPrice += $surfInfo->vip_2;
            }
        }


        SurfingLink::create([
            'status' => 0,
            'user_id' => Auth::id(),
            'name' => $element['name'],
            'desc' => $element['desc'],
            'link' => $element['link'],
            'price' => $element['price'],
            'redirect' => $element['redirect'],
            'vip' => $element['vip'],
            'unique_ip' => $element['unique_ip'],
            'visit_day' => $element['visit_day'],
            'balance' => 0,
            'complete' => 0,
            'denied' => 0,
            'check' => 0,
            'result_price' => $resultPrice,
        ]);

        return Response::json(['type' => 'success', 'message' => 'Задание успешно создано', 'link' => '/surfing'], 200);
    }

    public function depositLink(Request $request) {
        DB::beginTransaction();
        if (empty($request->amount)) {
            return Response::json(['type' => 'error', 'message' => 'Введите сумму', 'link' => '/surfing'], 200);
        }
        $request->amount *= 1;
        if (!is_int($request->amount)) {
            return Response::json(['type' => 'error', 'message' => 'Вы должны ввести целое число', 'link' => '/surfing'], 200);
        }

        $amount = (int)$request->amount;
        $linkId = (int)$request->link_id;

        if ($amount < 10) {
            return Response::json(['type' => 'error', 'message' => 'Минимальная сумма 10 руб.', 'link' => '/surfing'], 200);
        }

        if (Auth::user()->balance < $amount) {
            return Response::json(['type' => 'error', 'message' => 'На балансе недостаточно средств', 'link' => '/surfing'], 200);
        }

        $checkLink = SurfingLink::whereId($linkId)->count();
        if (!$checkLink) {
            return Response::json(['type' => 'error', 'message' => 'Ссылка не существует', 'link' => '/surfing'], 200);
        }
        $link = SurfingLink::whereId($linkId)->first();

        if ($link->user_id != Auth::id()) {
            return Response::json(['type' => 'error', 'message' => 'Кажется это не ваша ссылка.', 'link' => '/surfing'], 200);
        }


        User::whereId(Auth::id())->decrement('balance', $amount);
        SurfingLink::whereId($linkId)->increment('balance', $amount);

        // transaction here +++

        Helpers::createTransaction([
            'table' => 'transaction_deposit_ad',
            'type' => 'surfing',
            'task_id' => $linkId,
            'from_user_id' => Auth::id(),
            'user_id' => 0,
            'amount' => $amount,
        ]);





        DB::commit();
        return Response::json(['type' => 'success', 'message' => 'Баланс пополнен', 'link' => '/surfing'], 200);
    }

    public function playLink(Request $request) {
        $linkId = (int)$request->link_id;
        $link = SurfingLink::find($linkId);
        if ($link->balance < $link->result_price) {
            return Response::json(['type' => 'error', 'message' => 'Недостаточно средств для запуска. Пополните баланс', 'link' => '/surfing'], 200);
        }

        if ($link->user_id != Auth::id()) {
            return Response::json(['type' => 'error', 'message' => 'Кажется это не ваша ссылка.', 'link' => '/surfing'], 200);
        }

        if ($link->status == 0) {
            $status = 1;
            $message = "Ссылка запущена в ротацию";
        } elseif ($link->status == 1) {
            $status = 0;
            $message = "Ротация ссылки приостановлена";
        }

        SurfingLink::whereId($linkId)->update([
            'status' => $status,
        ]);

        return Response::json(['type' => 'success', 'message' => $message, 'link' => '/surfing'], 200);

    }

    public function viewLink($id) {

        try {
            $link = SurfingLink::findOrFail($id);
        } catch (\Exception $e) {
            exit("Ссылка не существует");
        }

        // you look a link already in other tab

        $checkComplete = SurfingComplete::where('ip', $_SERVER['REMOTE_ADDR'])
            ->whereDate('date', Carbon::today())
            ->where('link_id', $id)
            ->where('status', 1)
            ->count();

        if ($checkComplete) {
            exit('Вы уже посещали эту ссылку сегодня');
        }

        if ($link->status == 0) {
            SurfingComplete::where('user_id', Auth::id())
                            ->where('status', 0)
                            ->update(['status' => 1]);
            exit('Эта ссылка уже снята с ротации');
        }

        if ($link->visit_day and $link->todayComplete() >= $link->visit_day) {
            SurfingComplete::where('user_id', Auth::id())
                ->where('status', 0)
                ->update(['status' => 1]);
            exit('Эта ссылка уже снята с ротации');
        }

        $checkViewNow = SurfingComplete::where('user_id', Auth::id())
                                        ->where('status', 0)
                                        ->first();

        if ($checkViewNow and $checkViewNow->link_id != $id) {
            exit('Вы не можете просматривать несколько ссылок одновременно. <a href="/surfing/view/'.$checkViewNow->link_id.'">Продолжить просмотр ссылки</a>');
        }

        if ($link->user_id == Auth::id()) {
            return view('viewLink', compact('link'));
        }

        $checkRecord = SurfingComplete::where('ip', $_SERVER['REMOTE_ADDR'])
            ->whereDate('date', Carbon::today())
            ->where('link_id', $id)
            ->where('status', 0)
            ->count();

        if (!$checkRecord) {
            SurfingComplete::create([
                'user_id' => Auth::id(),
                'link_id' => $id,
                'date' => Carbon::now(),
                'status' => 0,
                'ip' => $_SERVER['REMOTE_ADDR'],
            ]);
        }

        return view('viewLink', compact('link'));
    }

    public function getReward($id) {
        DB::beginTransaction();

        $link = SurfingLink::find($id);
        if ($link->user_id == Auth::id()) {
            return Response::json(['message' => "Это ваша ссылка."], 200);
        }

        if ($link->status == 0 or $link->balance < $link->result_price) {
            return Response::json(['message' => "Произошла ошибка"], 200);
        }

        $checkComplete = SurfingComplete::where('user_id', Auth::id())
            ->where('link_id', $id)
            ->orderBy('id', 'desc')
            ->first();
        if ($checkComplete->status == 1) {
            return Response::json(['message' => "Вы уже получили вознаграждение"], 200);
        }

        $surfInfo = SurfingSetting::find(1);
        $sumOut = $link->result_price;
        $reward = $link->reward();
        $affiliateRewardAd = $link->priceAmount()/10;
        $affiliateRewardUs = $reward/10;
        $author = User::find($link->user_id);

        // transaction here +++

        SurfingLink::find($id)->decrement('balance', $sumOut);
        $linkNow = SurfingLink::find($id);
        if ($linkNow->balance < $linkNow->result_price) {
            SurfingLink::find($id)->update(['status' => 0]);
            SurfingLink::find($id)->increment('complete');
        }

        User::whereId(Auth::id())->increment('balance', $reward);
        Helpers::createTransaction([
            'table' => 'transaction_earn',
            'type' => 'surfing_reward',
            'task_id' => $id,
            'from_user_id' => $linkNow->user_id,
            'user_id' => Auth::id(),
            'amount' => $reward,
        ]);

        User::whereId(Auth::user()->refer)->increment('balance', $affiliateRewardUs);
        Helpers::createTransaction([
            'table' => 'transaction_earn',
            'type' => 'surfing_reward_affiliate',
            'task_id' => $id,
            'from_user_id' => Auth::id(),
            'user_id' => Auth::user()->refer,
            'amount' => $affiliateRewardUs,
        ]);

        User::whereId($author->refer)->increment('balance', $affiliateRewardAd);
        Helpers::createTransaction([
            'table' => 'transaction_earn',
            'type' => 'surfing_reward_affiliate_ad',
            'task_id' => $id,
            'from_user_id' => $linkNow->user_id,
            'user_id' => $author->refer,
            'amount' => $affiliateRewardAd,
        ]);




        Helpers::checkBoost(Auth::id(), 'boost_surfing', $reward, $id);
        Helpers::checkBoost(Auth::user()->refer, 'boost_surfing_aff', $affiliateRewardUs, $id);

        SurfingComplete::where('user_id', Auth::id())
            ->where('link_id', $id)
            ->where('status', 0)
            ->update([
                'status' => 1,
            ]);

        DB::commit();

        return Response::json(['message' => "Получено вознаграждение +$reward"], 200);
    }

}

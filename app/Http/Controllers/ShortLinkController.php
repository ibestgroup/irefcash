<?php

namespace App\Http\Controllers;

use App\Libraries\Helpers;
use App\Models\CoreSiteList;
use App\Models\Monitoring;
use App\Models\Payments;
use App\Models\ShortLink;
use App\Models\ShortLinkAdvert;
use App\Models\ShortLinkVisit;
use App\Models\SurfingLink;
use App\Models\SurfingSetting;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Teepluss\Theme\Facades\Theme;
use Validator;

class ShortLinkController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
        
        $this->middleware('auth');
    }

    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */

    public function index()
    {
        $links = ShortLink::where('user_id', Auth::id())->get();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('Сокращенные ссылки '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('shortLink', compact('trending_tags', 'links'))->render();

    }

    public function add(Request $request) {
        $short = ShortLink::create([
            'user_id' => Auth::id(),
            'link_id' => '',
            'link' => $request->link,
            'visit' => 0,
            'monetize' => $request->monetize,
        ]);

        $code = Helpers::shortLink($short->id);
        ShortLink::where('id', $short->id)->update(['link_id' => $code]);

        return Response::json(['type' => 'success', 'message' => 'Ссылка сокращена'], 200);
    }

    public function advert() {

        $surfInfo = SurfingSetting::find(1);
        $surfLinks = SurfingLink::where('user_id', Auth::id())->get();
        $advert = ShortLinkAdvert::where('user_id', Auth::id())->get();

        $trending_tags = Helpers::trandingTags();

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('default');
        $theme->setTitle('Реклама в ссылках '.Setting::get('title_seperator').' '.Setting::get('site_title').' '.Setting::get('title_seperator').' '.Setting::get('site_tagline'));

        return $theme->scope('shortLinkAdvert', compact('advert', 'trending_tags', 'surfInfo', 'surfLinks'))->render();

    }
    public function addAdvert(Request $request) {

        ShortLinkAdvert::create([
            'user_id' => Auth::id(),
            'status' => 0,
            'name' => $request->name,
            'link' => $request->link,
            'balance' => 0,
            'date' => Carbon::now(),
        ]);

        return Response::json(['type' => 'success', 'message' => 'Рекламная кампания успешно добавлена'], 200);

    }

    public function view($key) {

        try {
            $shortLink = ShortLink::where('link_id', $key)->firstOrFail();

            if ($shortLink->monetize == 0) {
                return redirect($shortLink->link);
            }

            $checkVisit = ShortLinkVisit::where('ip', $_SERVER['REMOTE_ADDR'])->where('status', 0)->first();

            if ($checkVisit) {
                $advert = ShortLinkAdvert::find($checkVisit->advert_id);
            } else {
                $visits = ShortLinkVisit::where('ip', $_SERVER['REMOTE_ADDR'])->whereDate('date', Carbon::today())->where('status', 1)->pluck('advert_id');
                $advertCount = ShortLinkAdvert::whereNotIn('id', $visits)->where('id', '!=', 1)->where('status', 1)->count();
                if ($advertCount) {
                    $advert = ShortLinkAdvert::whereNotIn('id', $visits)->where('id', '!=', 1)->where('status', 1)->get()->random(1)->first();
                } else {
                    $advert = ShortLinkAdvert::find(1);
                }
            }


        } catch (\Exception $e) {
            exit('Ссылка не существует');
        }

        $count = ShortLinkVisit::where('status', 0)->where('advert_id', $advert->id)->where('ip', $_SERVER['REMOTE_ADDR'])->count();
        if ($count) {
            $visitId = ShortLinkVisit::where('status', 0)->where('advert_id', $advert->id)->where('ip', $_SERVER['REMOTE_ADDR'])->first()->id;
        } else {
            $count = ShortLinkVisit::where('advert_id', $advert->id)->where('ip', $_SERVER['REMOTE_ADDR'])->count();

            if (!$count) {
                $visitId = ShortLinkVisit::create([
                    'status' => 0,
                    'link_id' => $shortLink->id,
                    'distributor_id' => $shortLink->user_id,
                    'advert_id' => $advert->id,
                    'date' => Carbon::now(),
                    'ip' => $_SERVER['REMOTE_ADDR'],
                ]);
                $visitId = $visitId->id;
            } else {
                $visitId = 0;
            }
        }

        return view('viewShortLink', compact('advert', 'shortLink', 'visitId'));
    }

    public function reward(Request $request) {
        DB::beginTransaction();
        $request->id = (int)$request->id;
        $surfInfo = SurfingSetting::find(1);

        if ($request->id != 0) {
            try {
                $visit = ShortLinkVisit::where('id', $request->id)->where('status', 0)->firstOrFail();
            } catch (\Exception $e) {
                Response::json(['message' => 'Что-то пошло не так'], 200);
            }

            ShortLinkVisit::find($visit->id)->update([
                'status' => 1,
            ]);

            $advert = ShortLinkAdvert::find($visit->advert_id);

            if ($advert->balance >= $surfInfo->short_link_price) {
                $reward = $surfInfo->short_link_reward;
                $affiliateRewardUs = $reward/10;
                ShortLinkAdvert::find($visit->advert_id)->decrement('balance', $surfInfo->short_link_price);

                User::find($visit->distributor_id)->increment('balance', $reward);
                $distributor = User::find($visit->distributor_id);
                Helpers::createTransaction([
                    'table' => 'transaction_earn',
                    'type' => 'short_link_reward',
                    'task_id' => $visit->link_id,
                    'from_user_id' => $advert->user_id,
                    'user_id' => $visit->distributor_id,
                    'amount' => $reward,
                ]);
                User::find($distributor->refer)->increment('balance', $affiliateRewardUs);
                Helpers::createTransaction([
                    'table' => 'transaction_earn',
                    'type' => 'short_link_reward',
                    'task_id' => $visit->link_id,
                    'from_user_id' => $distributor->id,
                    'user_id' => $distributor->refer,
                    'amount' => $affiliateRewardUs,
                ]);

                Helpers::checkBoost($visit->distributor_id, 'boost_surfing', $reward, $visit->link_id);
                Helpers::checkBoost($distributor->refer, 'boost_surfing_aff', $affiliateRewardUs, $visit->link_id);

                if (ShortLinkAdvert::find($visit->advert_id)->balance < $surfInfo->short_link_price) {
                    ShortLinkAdvert::find($visit->advert_id)->update([
                        'status' => 0,
                    ]);
                }
            }
        }

        DB::commit();
    }

    public function depositAdvert(Request $request) {
        DB::beginTransaction();
        if (empty($request->amount)) {
            return Response::json(['type' => 'error', 'message' => 'Введите сумму', 'link' => '/surfing'], 200);
        }

        $request->amount *= 1;
        if (!is_int($request->amount)) {
            return Response::json(['type' => 'error', 'message' => 'Вы должны ввести целое число', 'link' => '/surfing'], 200);
        }

        $amount = (int)$request->amount;
        $linkId = (int)$request->advert_id;

        if ($amount < 10) {
            return Response::json(['type' => 'error', 'message' => 'Минимальная сумма 10 руб.', 'link' => '/surfing'], 200);
        }

        if (Auth::user()->balance < $amount) {
            return Response::json(['type' => 'error', 'message' => 'На балансе недостаточно средств', 'link' => '/surfing'], 200);
        }

        $checkLink = ShortLinkAdvert::whereId($linkId)->count();
        if (!$checkLink) {
            return Response::json(['type' => 'error', 'message' => 'Ссылка не существует', 'link' => '/surfing'], 200);
        }
        $link = ShortLinkAdvert::whereId($linkId)->first();

        if ($link->user_id != Auth::id()) {
            return Response::json(['type' => 'error', 'message' => 'Кажется это не ваша ссылка.', 'link' => '/surfing'], 200);
        }

        User::whereId(Auth::id())->decrement('balance', $amount);
        ShortLinkAdvert::whereId($linkId)->increment('balance', $amount);

        // transaction here +++

        Helpers::createTransaction([
            'table' => 'transaction_deposit_ad',
            'type' => 'short_link',
            'task_id' => $linkId,
            'from_user_id' => Auth::id(),
            'user_id' => 0,
            'amount' => $amount,
        ]);


        DB::commit();
        return Response::json(['type' => 'success', 'message' => 'Баланс пополнен', 'link' => '/surfing'], 200);
    }


    public function playAdvert(Request $request) {
        $surfInfo = SurfingSetting::find(1);
        $linkId = (int)$request->link_id;
        $link = ShortLinkAdvert::find($linkId);
        if ($link->balance < $surfInfo->short_link_price) {
            return Response::json(['type' => 'error', 'message' => 'Недостаточно средств для запуска. Пополните баланс', 'link' => '/surfing'], 200);
        }

        if ($link->user_id != Auth::id()) {
            return Response::json(['type' => 'error', 'message' => 'Кажется это не ваша ссылка.', 'link' => '/surfing'], 200);
        }

        if ($link->status == 0) {
            $status = 1;
            $message = "Ссылка запущена в ротацию";
        } elseif ($link->status == 1) {
            $status = 0;
            $message = "Ротация ссылки приостановлена";
        }

        ShortLinkAdvert::whereId($linkId)->update([
            'status' => $status,
        ]);

        return Response::json(['type' => 'success', 'message' => $message, 'link' => '/surfing'], 200);

    }

}

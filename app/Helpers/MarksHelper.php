<?php
namespace App\Helpers;

use App\Libraries\Activate;
use App\Libraries\Helpers;
use App\Libraries\Multimark;
use App\Models\MarksElement;
use App\Models\MarksLevelInfo;
use App\Models\OrdersMarkInside;
use App\Models\PaymentsOrder;
use App\User;
use Carbon\Carbon;
use App\Models\MarksInside;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MarksHelper
{

    public static function calcMarkPrice($id, $level = 1) {

        $mark = MarksInside::find($id);
        $marksLevelInfo = MarksLevelInfo::find($id);
        $price = 0;

        if ($mark['type'] == 1) {

            $marksLevelInfo = MarksLevelInfo::find($id);

            $price = $mark['level'.$level] + $marksLevelInfo['to_par_'.$level] + $marksLevelInfo['to_admin_'.$level];

        } else {

            $marksLevelInfo = MarksLevelInfo::find($id);

            for ($i = 1; $mark['level'] >= $i; $i++) {
                $price += $mark['level'.$i] + $marksLevelInfo['to_par_'.$i] + $marksLevelInfo['to_admin_'.$i];
            }

        }

        $sumInviter = 0;
        for ($i = 1; $i <= MarksHelper::globalCount(); $i++) {
            $sumInviter = $sumInviter + $marksLevelInfo->{'to_inviter_'.$i};
        }

        if ($sumInviter > 0) {
            $price = $price + $sumInviter;
        }

        return $price;
    }

    public static function createNextRound($mark) {
        $mark = MarksInside::find($mark);
        $lastElement = MarksElement::where('mark_id', $mark->id)->where('status', '!=', 0)->latest('end')->first();

        $resultStart = strtotime($lastElement->start) + $mark->interval;
        $resultEnd = date('Y-m-d H:i:s', $resultStart + $mark->length);
        $resultStart = date('Y-m-d H:i:s', $resultStart);

        $newMark = MarksElement::create([
            'status' => 0,
            'mark_id' => $mark->id,
            'start' => $resultStart,
            'end' => $resultEnd,
        ]);

        $createAdminOrder = OrdersMarkInside::create([
            'id_user' => 1,
            'parent' => 1,
            'mark' => $mark->id,
            'mark_element' => $newMark->id,
            'level' => 10,
            'balance' => 0,
            'pay_people' => 0,
            'status' => 0,
            'par_or' => 0,
            'date' => Carbon::now(),
            'reinvest' => 0,
            'cashout' => 0,
        ]);

        $createAdminOrder->update([
            'par_or' => $createAdminOrder->id,
        ]);
    }

    public static function checkRounds() {

        $marks = MarksInside::where('status', 1)->get();

        foreach ($marks as $mark) {

            $checkElements = MarksElement::where('mark_id', $mark->id)->count();
            if (!$checkElements) {

                $dateNow = Carbon::now();

                $nextSaturday = ($dateNow->dayOfWeek == 6) ? $dateNow->setTime(19, 0, 0) : $dateNow->next(6)->setTime(19, 0, 0);
                $nextSunday = Carbon::parse($nextSaturday)->addSecond($mark->length);

                MarksElement::create([
                    'status' => 0,
                    'mark_id' => $mark->id,
                    'start' => $nextSaturday,
                    'end' => $nextSunday,
                ]);

                MarksHelper::checkOrderAdmin($mark->id);
            }

            $checkActiveMark = MarksElement::where('mark_id', $mark->id)->where('status', 1)->count();
            $checkNextMark = MarksElement::where('mark_id', $mark->id)->where('status', 0)->count();
            if (!$checkActiveMark) {

                //если нет активного тарифа идем сюда
                if (!$checkNextMark) {

                    //если нет неактивного тарифа, то создаем
                    self::createNextRound($mark->id);

                }

                //если есть, то проверяем не стартанул ли он
                $nextMark = MarksElement::where('mark_id', $mark->id)->where('status', 0)->first();
                $timeEnd = strtotime($nextMark->start);

                if ($timeEnd <= time()) {
                    MarksElement::where('id', $nextMark->id)->update(['status' => 1]);
                    self::createNextRound($mark->id);
                    MarksHelper::checkOrderAdmin($mark->id);
                    self::checkRounds();
                }

            } else {

                //если активный тариф есть, то проверяем не закрылся ли он
                $activeMark = MarksElement::where('mark_id', $mark->id)->where('status', 1)->first();
                $timeStart = strtotime($activeMark->end);
                if ($timeStart <= time()) {
                    MarksElement::where('id', $activeMark->id)->update(['status' => 2]);
                    if (!$checkNextMark) {
                        self::createNextRound($mark->id);
                    }

                    MarksHelper::checkOrderAdmin($mark->id);

                    if ($mark->body_next) {
                        if (OrdersMarkInside::where('mark_element', $activeMark->id)->count() > 10) {

                            $lastUsers = OrdersMarkInside::where('mark_element', $activeMark->id)->latest('id')->take($mark->body_next)->get();

                            foreach ($lastUsers as $item) {
                                \App\Jobs\Activate::dispatch($item->id_user, $item->mark, 'free');
                            }
                        }
                    }
                    self::checkRounds();
                }
            }
        }
    }

    public static function activeRound($id, $err = true, $meth = 0) {
        $markActive = MarksElement::where('mark_id', $id)->where('status', 1);
        $count = $markActive->count();

        if ($count == 0 and $meth === 'free') {
            return MarksElement::where('mark_id', $id)->where('status', 0)->first()->id;
        }

        if ($count) {
            return $markActive->first()->id;
        } elseif ($err == true) {
            return "Этот тариф завершен или еще не начался.";
        }
    }

    public static function checkOrderAdmin($id) {

        if ($id == 'all') {
            $arrayMark = MarksInside::pluck('id');
        } else {
            $arrayMark = [$id];
        }

        foreach ($arrayMark as $id) {
            $markActive = self::activeRound($id, false, 'free');
            $checkAdminOrder = OrdersMarkInside::where('id_user', 1)->where('mark', $id)->where('mark_element', $markActive)->count();
            if (!$checkAdminOrder and $markActive) {
                $createAdminOrder = OrdersMarkInside::create([
                    'id_user' => 1,
                    'parent' => 1,
                    'mark' => $id,
                    'mark_element' => $markActive,
                    'level' => 10,
                    'balance' => 0,
                    'pay_people' => 0,
                    'status' => 0,
                    'par_or' => 0,
                    'date' => Carbon::now(),
                    'reinvest' => 0,
                    'cashout' => 0,
                ]);

                $createAdminOrder->update([
                    'par_or' => $createAdminOrder->id,
                ]);
            }
        }
    }

    public static function calcMarkPercent($balance, $mark) {
        return round($balance * 100 / self::markProfit($mark), 2);
    }

    public static function getMarkDays($start) {
        $cDate = Carbon::parse($start);
        return $cDate->diffInDays();
    }

    public static function markUsers($mark) {
        $people = 0;

        for ($i = 1; $i <= $mark->level; $i++) {
            $people += pow($mark->width, $i);
        }

        return $people;
    }

    public static function markUser($level, $width) {
        $people = 0;

        for ($i = 1; $i <= $level; $i++) {
            $people += pow($width, $i);
        }

        return $people;
    }

    public static function markName($id) {
        $mark = MarksInside::find($id);
        return $mark['name'];
    }

    public static function markStatus($id) {
        $mark = MarksInside::find($id);
        return $mark['status'];
    }

    public static function markProfit($id) {
        $mark = MarksInside::find($id);

        $level_price = ['', $mark['level1'], $mark['level2'], $mark['level3'], $mark['level4'], $mark['level5'], $mark['level6'], $mark['level7'], $mark['level8'], $mark['level9'], $mark['level10']];
        $width = $mark['width'];
        $level = $mark['level'];

        $profit = 0;

        $i = 1;
        while ($i < $level + 1) {
            $profit = $profit + ($level_price[$i] * pow($width, $i));
            $i++;
        }

        $profit = round($profit, 2);

        if ($id == 0) $profit = 0;

        return $profit;
    }

    public static function markPeople($id) {
        $mark = MarksInside::find($id);

        $width = $mark['width'];
        $level = $mark['level'];

        $people = 0;

        $i = 1;
        while ($i < $level + 1) {
            $people = $people + pow($width, $i);
            $i++;
        }

        if ($id == 0) $people = 0;

        return $people;
    }

    public static function  markReset($type = 1) {
        $count = MarksInside::all()->count();

        for ($i = 1; $i <= $count; $i++) {

            MarksInside::where('id', $i)
                ->update([
                    'name' => 'Тариф №'.$i,
                    'status' => 0,
                    'level' => 2,
                    'width' => 2,
                    'reinvest' => 0,
                    'overflow' => 1,
                    'type' => $type,
                    'to_par' => 0,
                    'to_admin' => 0,
                    'level1' => 10,
                    'level2' => 20,
                    'level3' => 30,
                    'level4' => 40,
                    'level5' => 50,
                    'level6' => 60,
                    'level7' => 70,
                    'level8' => 80,
                    'reinvest_first' => 0,
                    'only_reinvest' => 0
                ]);
        }

    }

    public static function profitLevel($id, $level, $admin = false) {

        $mark = MarksInside::find($id);
        $overflow = $mark->overflow;
        $profit = 0;
        $countPeople = 0;

        if ($admin) {
            for ($i = 1; $i <= $level; $i++) {
                $countPeople += pow($mark['width'], $i);
            }
            $profit = (Helpers::myRound($mark['level'.$level]/100*(100 - self::fee())) * 1) * $countPeople;
        } else {
            $countPeople = pow($mark['width'], $level);
            $profit = (Helpers::myRound($mark['level'.$level]/100*(100 - self::fee())) * 1) * pow($mark['width'], $level);
        }

        if ($overflow == 0) {
            $profit = 999999;
        }

//        dd($profit);

        return $profit;
    }

    public static function reservedLevel($id, $level) {
        $mark = MarksInside::find($id);
        $marksLevelInfo = MarksLevelInfo::find($id);
        $overflow = $mark->overflow;
        $sumReinvest = 0;
        if ($marksLevelInfo['reinvest_type_'.$level] and $marksLevelInfo['reinvest_'.$level]) {
            $sumReinvest = self::priceReinvest($id, $level) * $marksLevelInfo['reinvest_type_'.$level];
        }

        if ($overflow == 0) {
            $sumReinvest = 0;
        }

        return self::priceNextLevel($id, $level) + $sumReinvest;
    }

    public static function cashoutLevel($id, $level, $admin = false) {
        $result = self::profitLevel($id, $level, $admin) - self::reservedAllForLevel($id, $level);
//        dd($result);
        return self::profitLevel($id, $level, $admin) - self::reservedAllForLevel($id, $level);
    }

    public static function priceNextLevel($id, $level) {

        $level = $level + 1;
        $mark = MarksInside::find($id);
        $marksLevelInfo = MarksLevelInfo::find($id);
        $price = 0;
        if ($mark['type'] == 1) {
            if ($level <= $mark->level) {
                $price = $mark['level'.$level] + $marksLevelInfo['to_par_'.$level] + $marksLevelInfo['to_admin_'.$level];
            }
        }

        return $price;
    }

    public static function priceReinvest($id, $level) {

        $marksLevelInfo = MarksLevelInfo::find($id);

        $price = 0;

        if ($marksLevelInfo['reinvest_type_'.$level] and $marksLevelInfo['reinvest_'.$level]) {
            $price = self::markEnter($marksLevelInfo['reinvest_'.$level]);
        }

        return $price;
    }


    public static function priceLevel($id, $level) {

        $mark = MarksInside::find($id);
        $marksLevelInfo = MarksLevelInfo::find($id);

        $price = 0;
        if ($level <= $mark->level) {
            $price = $mark['level'.$level] + $marksLevelInfo['to_par_'.$level] + $marksLevelInfo['to_admin_'.$level];
        }

        return $price;
    }

    public static function markEnter($id) {

        $mark = MarksInside::find($id);
        $marksLevelInfo = MarksLevelInfo::find($id);
        $result = 0;

        for ($i = 1; $i <= $mark['level']; $i++) {
            if ($mark['type'] == 2 or $i <= 1) {
                $result += $mark['level'.$i] + $marksLevelInfo['to_par_'.$i] + $marksLevelInfo['to_admin_'.$i];
            }
        }

        for ($i = 1; $i <= MarksHelper::globalCount('level'); $i++) {
            $result += $marksLevelInfo['to_inviter_'.$i];
        }

        return $result;
    }

    public static function reservedAllForLevel ($id, $level) {

        $marks = MarksInside::where('id', $id)->first();
        $marksLevelInfo = MarksLevelInfo::where('id', $id)->first();
        $reinvest = $marksLevelInfo['reinvest_'.$level];
        $reinvestType = $marksLevelInfo['reinvest_type_'.$level];
        $reinvestFirst = $marksLevelInfo['reinvest_first_'.$level];
        $reinvestLevel = 0;
        $reinvestFirstLevel = 0;

        if ($marks->type == 1) {
            $priceLevel = self::priceLevel($id, $level+1);
        } else {
            $priceLevel = 0;
        }

        if ($reinvestType and $reinvest) {
            $reinvestLevel = self::markEnter($reinvest) * $reinvestType;
        }

        if ($reinvestFirst) {
            $reinvestFirstLevel = self::markEnter(1) * $reinvestFirst;
        }

        $result = $priceLevel + $reinvestLevel + $reinvestFirstLevel;

        return $result;

    }

    public static function marksError($type = 'bool') {
        $marks = MarksInside::all();
        $message = '';

        foreach ($marks as $mark) {
            $markLevelInfo = MarksLevelInfo::where('id', $mark->id)->first();
            if (MarksHelper::markEnter($mark->id) == 0 and $mark->status > 0) {
                $message .= "Тариф \"$mark->name\" (#$mark->id)<br>Цена не может быть нулевой <br>";
            }


            for ($i = 1; $i <= $mark->level; $i++) {

                $profitLevel = Helpers::myRound(self::profitLevel($mark->id, $i));

                $reservedLevel = Helpers::myRound(self::reservedAllForLevel($mark->id, $i));


                if ($profitLevel < $reservedLevel) {
                    if ($type == 'bool') {
                        if ($mark->overflow != 0) {
                            return true;
                        }
                    }

                    if ($mark->overflow != 0) {
                        $message .= "Тариф \"$mark->name\" (#$mark->id)<br>На уровне $i  ошибка: \"Доход уровня $i меньше чем весь его резерв (следующий уровень или другие тарифы).\" <br>";
                    }

                } else {
//                    $message .= "На уровне $i нет ошибок<br>";
                }
            }

            for ($i = 1; $i <= MarksHelper::globalCount(); $i++) {

                if ($markLevelInfo['to_inviter_'.$i] < 0 or
                    $markLevelInfo['to_admin_'.$i] < 0 or
                    $markLevelInfo['to_par_'.$i] < 0 or
                    $markLevelInfo['to_inviter_'.$i] < 0 or
                    $mark['level'.$i] < 0) {
                    if ($type == 'bool') {
                        if ($mark->overflow != 0) {
                            return true;
                        }
                    }
                }
            }
//            $message .= "<br>";
        }

        return $message;
    }


    public static function peopleLevel($id, $level) {
        $mark = MarksInside::find($id);

        $width = $mark['width'];
        $people = pow($width, $level);

        if ($id == 0) $people = 0;

        return $people;
    }

    public static function checkClosed($tarifId) {
        $tarif = OrdersMarkInside::where('id', $tarifId)->first();
        $peopleNumbers = self::markPeople($tarif->mark);
        $peopleNumbersNow = PaymentsOrder::where('wallet', $tarif->id)->count();

        if ($peopleNumbersNow < $peopleNumbers) {
            return false;
        } else {
            return true;
        }

    }

    public static function checkForDelete($id, $mess = 0) {
        $orderCount = OrdersMarkInside::where('mark', $id)->whereRaw('id != par_or')->count();

        $result = true;
        $message = [];

        if ($orderCount) {
            $result = false;
            $message[] = 'Есть активированные тарифы';
        }

        $marksInside = MarksInside::all();
        $flag = 0;
        foreach ($marksInside as $mark) {
            $levelInfo = MarksLevelInfo::where('id', $mark->id)->first();
            for ($i = 1; $i <= self::globalCount(); $i++) {
                if ($levelInfo->{'reinvest_'.$i} == $id) {
                    $result = false;
                    if ($flag == 0) {
                        $message['error_reinvest'] = 'В этот тариф есть реинвесты с других тарифов:';
                    }
                    $message['error_reinvest'] .= '<br>'.$mark->name." уровень $i";
                    $flag = 1;
//                    break;
                }
            }
            if ($flag) break;
        }

        if ($id == 1) {
            $result = false;
            $message = ['Первый тариф нельзя удалить'];
        }

        if ($mess) {
            return $message;
        }

        return $result;

    }

    public static function globalCount($type = 'level') {
        if ($type == 'level') {
            return 10;
        }
        return 1;
    }

    public static function fee() {
        $fee = 0;
        return $fee;
    }

    public static function createTree($orderId, $child = false) {

        $arrayTree = Multimark::createTree($orderId);
        $startOrder = OrdersMarkInside::find($orderId);
        $user = User::find($startOrder->id_user);
        $parent = User::find($startOrder->parent);
        $openTree = ['', ''];
        $openTreeAdmin = ['', ''];
        $margin = "style='margin: auto;'";

        if ($startOrder->id != $startOrder->par_or) {

            $orderParent = OrdersMarkInside::find($startOrder->par_or);

            if ($orderParent->id != $orderParent->par_or) {

//                $orderAdmin = OrdersMarkInside::where('id_user', 1)->where('mark_element', MarksHelper::activeRound($startOrder->mark))->first();
                $orderAdmin = OrdersMarkInside::where('id_user', 1)->where('mark_element', $startOrder->mark_element)->first();
                $userAdmin = User::find(1);
                $openTreeAdmin[0] = "<ul style='margin: auto;'>
                    <li>            
                    <span class=\"tf-nc\" style='text-align: center;'>
                        <a href='/matrix/tree/$orderAdmin->id' style='font-size: 40px;'><i class=\"fas fa-arrow-up\" style='border-top: 3px solid #00AEEF; padding-top: 2px;'></i></a>

                        <br>
                        <a href='/$userAdmin->username' style='font-size: 13px;'>В начало</a>
                    </span>";
                $openTreeAdmin[1] = '</li></ul>';
                $margin = "";
            }

            $background = ($parent->id == Auth::user()->id)?'#e1fdf0':'transparent';
            $background = 'background: '.$background;
            $openTree[0] = $openTreeAdmin[0]."
                <ul $margin>
                    <li>            
                    <span class=\"tf-nc\" style='text-align: center; $background'>
                        <img src='$parent->avatar' width='50px' alt=''>
                        <a href='/matrix/tree/$startOrder->par_or' style='font-size: 16px;'><i class=\"fas fa-arrow-up\"></i></a>
                        <br>
                        <a href='/$parent->username' style='font-size: 13px;'>@$parent->username</a>
                    </span>";
            $openTree[1] = "</li></ul>";
            $margin = "";
        }


        if (!$child) {
            $background = ($user->id == Auth::user()->id)?'#e1fdf0':'transparent';
            $background = 'background: '.$background;
            $stringProfit = ($startOrder->id_user != 1)?"<p class='tree-profit'>+$startOrder->balance<i class='fa fa-rub'></i></p>":'<br>';
            echo "
            <div class=\"tf-tree example\" style='display: flex; overflow: visible;'>
                $openTree[0]
                <ul class='js-transform-scale' $margin>
                    <li>            
                    <span class=\"tf-nc\" style='text-align: center; $background'>
                        <img src='$user->avatar' width='50px' alt=''>
                        $stringProfit
                        <a href='/$user->username' style='font-size: 13px;'>@$user->username</a>
                    </span>";
        }

        echo "<ul>";

        foreach ($arrayTree as $item) {
            $user = User::find($item['user']);
            $background = ($user->id == Auth::user()->id)?'#e1fdf0':'transparent';
            $background = 'background: '.$background;
            echo "<li>            
                    <span class=\"tf-nc\" style='text-align: center; $background'>
                        <img src='$user->avatar' width='50px' alt=''>
                        <a href='/matrix/tree/$item[id]' style='font-size: 16px;'><i class=\"fas fa-search\"></i></a>
                        <p class='tree-profit'>+$item[profit]<i class='fa fa-rub'></i></p>
                        <a href='/$user->login' style='font-size: 13px;'>@$user->login</a>
                    </span>";
            if (!empty($item['children'])) {
                self::createTree($item['id'], true);
            }
            echo "</li>";
        }

        echo "</ul>";

        if (!$child) {
            echo "</li>
                </ul>
                $openTree[1]
                $openTreeAdmin[1]
            </div>
            ";
        }

    }

    static function referralPayList($array, $markNum, $orderMain, $level = 0, $url = '') {
        $mark = MarksInside::where('id', $markNum)->first();
        foreach ($array as $key => $order) {
            $orderInfo = OrdersMarkInside::where('id', $order['id'])->first();
            $userInfo = Users::where('id', $order['user'])->first();
            $payment = PaymentsOrder::where('wallet', $orderMain)->where('level', $level)->where('from_user', $order['user'])->count();

            if($payment) {
                $bg = '#7e4de1';
            } else {
                $bg = '#ff8d2b';
            }

            if($level == 0) {
                $widthStyle = 100;
                $bg = '#7e4de1';
            } else {
                $widthStyle = 100/$mark['width'];
            }

            if ($userInfo->avatar == 'no_image') {
                $avatar = "/iadminbest/storage/app/".\App\Models\SiteInfo::find(1)->first()['no_image'];
            } else {
                $avatar = "/iadminbest/storage/app/".$userInfo->avatar;
            }

            if ($order['name'] == Auth::user()->login) {
                $loginTree = '<i class="fa fa-user"></i> Вы';
            } else {
                $loginTree = $order['name'];
            }

            echo "
            <div style='text-align:center; min-height: 30px; color: $bg; width: ". $widthStyle ."%; float: left;'>
                <div style='padding: 10px 2px;' class='img-tree'>
                    <a href='".$url."?tarif_id=".$orderInfo->id."'><img src=\"$avatar\" style='width: 100px; height: 100px; max-width: 100%;' class=\"img-circle-my\" alt=\"".$userInfo->login."\"></a>
                    <br>".$loginTree."<br>".$orderInfo['level']." уровень
                </div>";

            if ($level+1 <= $mark['level']) {
                ReferalTree::referralPayList($order['children'], $markNum, $orderMain, $level + 1, $url);
            }

            if (count($order['children']) < $mark['width'] and $level + 1 <= $mark['level']) {
                if ($mark['width'] <= 8) {
                    ReferalTree::emptyPlace($level+1, $mark['level'], $mark['width'], count($order['children']) + 1);
                }
            }

            echo "<div class='clearfix'></div>
                </div>";
        }

    }

    static function emptyPlace($level, $levelMax, $width, $count) {
        $marks = MarksInside::where('id', 6)->first();

        $avatar = "/iadminbest/storage/app/".\App\Models\SiteInfo::find(1)->first()['no_image'];
        $bg = '#777';
        for ($i = $count; $i <= $width; $i++) {
            echo "
            <div style='text-align:center; min-height: 30px; color: $bg; width: " . 100 / $width . "%; float: left;'>
                <div style='padding: 10px 2px;' class='img-tree-free'>
                    <img src=\"".$avatar."\" style='width: 100px; height: 100px; max-width: 100%;' class=\"img-circle-my circle-free\" alt=\"User Image\">
                    <br>свободное
                    <br>место
                </div>";

            if ($level + 1 <= $levelMax) {
                ReferalTree::emptyPlace($level + 1, $levelMax, $width, 1);
            }

            echo "<div class='clearfix'></div>
                </div>";
        }
    }

}
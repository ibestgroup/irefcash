<?php

namespace App\Jobs;

use App\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Activate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $mark;
    protected $typeBalance;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $mark, $typeBalance = 0)
    {
        $this->user = $user;
        $this->mark = $mark;
        $this->typeBalance = $typeBalance;
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $activate = new \App\Libraries\Activate();
        $activate->activate($this->user, $this->mark, 0, 'activate', 0, 0, $this->typeBalance);

        //
    }
}

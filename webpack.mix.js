const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.less('./node_modules/bootstrap-less/bootstrap/bootstrap.less', './public/themes/default/assets/css/bootstrap.css')
	.less('./public/themes/default/assets/less/style.less','./public/css/main.css')
	.combine([
        './public/themes/default/assets/css/styleOld.css',
        './public/css/main.css',
	], 'public/css/app.css')
	.combine([
        // './public/themes/default/assets/js/admin.js',
		// './public/themes/default/assets/js/bootstrap.min.js',
		// './public/themes/default/assets/js/bootstrap-datepicker.js',
		// './public/themes/default/assets/js/bootstrap-datetimepicker.js',
		// './public/themes/default/assets/js/bootstrap-toggle.min.js',
		// './public/themes/default/assets/js/bootstrap-typeahead.js',
		// './public/themes/default/assets/js/chatboxes.js',
		// './public/themes/default/assets/js/core.js',
		// './public/themes/default/assets/js/emojify.js',
		// './public/themes/default/assets/js/jquery.bootpag.min.js',
		// './public/themes/default/assets/js/jquery.cssemoticons.js',
		// './public/themes/default/assets/js/jquery.dataTables.js',
		// './public/themes/default/assets/js/jquery.form.js',
		// './public/themes/default/assets/js/jquery.jscroll.js',
		// './public/themes/default/assets/js/jquery.mCustomScrollbar.concat.min.js',
		// './public/themes/default/assets/js/jquery.min.js',
		// './public/themes/default/assets/js/jquery.noty.packaged.js',
		// './public/themes/default/assets/js/jquery.nstSlider.min.js',
		// './public/themes/default/assets/js/jquery.picture.cut.js',
		// './public/themes/default/assets/js/jquery.simplePagination.js',
		// './public/themes/default/assets/js/jquery.timeago.js',
		// './public/themes/default/assets/js/jquery.tinymce.min.js',
		// './public/themes/default/assets/js/jquery-1.8.3.min.js',
		// './public/themes/default/assets/js/jquery-confirm.min.js',
		// './public/themes/default/assets/js/jquery-ui.js',
		// './public/themes/default/assets/js/jquery-ui-1.10.3.custom.min.js',
		// './public/themes/default/assets/js/license.txt',
		// './public/themes/default/assets/js/linkify-jquery.min.js',
		// './public/themes/default/assets/js/main.7e099de1c2d4b4d95065cb1d66b3cb74.js',
		// './public/themes/default/assets/js/main-871469da5d.js',
		// './public/themes/default/assets/js/mention.js',
		// './public/themes/default/assets/js/messages.js',
		// './public/themes/default/assets/js/notifications.js',
		// './public/themes/default/assets/js/npm-debug.log',
		// './public/themes/default/assets/js/owl.carousel.min.js',
		// './public/themes/default/assets/js/playSound.js',
		// './public/themes/default/assets/js/plyr.js',
		// './public/themes/default/assets/js/pusher.min.js',
		// './public/themes/default/assets/js/selectize.min.js',
		// './public/themes/default/assets/js/socket.js',
		// './public/themes/default/assets/js/sweetalert.min.js',
		// './public/themes/default/assets/js/sweetalert-dev.js',
		// './public/themes/default/assets/js/tinymce.dev.js',
		// './public/themes/default/assets/js/tinymce.js',
		// './public/themes/default/assets/js/vue.js',
		// './public/themes/default/assets/js/vue-resource.min.js',


        './public/themes/default/assets/js/jquery.min.js',
        './public/themes/default/assets/js/jquery-ui-1.10.3.custom.min.js',
        './public/themes/default/assets/js/jquery/moment.js',
        './public/themes/default/assets/js/jquery.form.js',
        './public/themes/default/assets/js/jquery.timeago.js',
        './public/themes/default/assets/js/bootstrap.min.js',
        './public/themes/default/assets/js/bootstrap-datepicker.js',
        './public/themes/default/assets/js/bootstrap-datetimepicker.js',
        './public/themes/default/assets/js/jquery-confirm.min.js',
        './public/themes/default/assets/js/jquery.noty.packaged.min.js',
        './public/themes/default/assets/js/selectize.min.js',
        './public/themes/default/assets/js/jquery.jscroll.js',
        './public/themes/default/assets/js/jquery.mCustomScrollbar.concat.min.js',
        './public/themes/default/assets/js/emojify.js',
        './public/themes/default/assets/js/bootstrap-typeahead.js',
        './public/themes/default/assets/js/mention.js',
        './public/themes/default/assets/js/playSound.js',
        './public/themes/default/assets/js/pusher.min.js',
        './public/themes/default/assets/js/vue.js',
        './public/themes/default/assets/js/vue-resource.min.js',
        './public/themes/default/assets/js/tinymce/tinymce.min.js',
        './public/themes/default/assets/js/login.js',

		'./public/themes/default/assets/js/mainOld.js',
		'./public/themes/default/assets/js/app.js',

        //
        './public/themes/default/assets/js/linkify.min.js',
        './public/themes/default/assets/js/linkify-jquery.min.js',

		// './public/themes/default/assets/js/main.js.map',
		// './public/themes/default/assets/js/lightbox.min.js',
		// './public/themes/default/assets/js/lightgallery.js',
		// './public/themes/default/assets/js/linkify.min.js',
        // './public/themes/default/assets/js/login.js',


		], 'public/js/app.js');

// mix.disableNotifications();

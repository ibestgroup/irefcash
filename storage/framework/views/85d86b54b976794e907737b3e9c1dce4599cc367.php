<div class="widget-events widget-left-panel">
	<div class="menu-list">
		<ul class="list-unstyled">
			<li><a href="<?php echo e(url(Auth::user()->username)); ?>" class="btn menu-btn"><i class="fa fa-user" aria-hidden="true"></i><?php echo e(trans('common.my_profile')); ?> </a></li>
			<li class="<?php echo (Request::segment(1)=='' ? 'active' : ''); ?>"><a href="<?php echo e(url('/')); ?>" class="btn menu-btn"><i class="fa fa-trophy" aria-hidden="true"></i><?php echo e(trans('common.home')); ?></a></li>

			<?php if(Setting::get('enable_browse') == 'on'): ?>
				<li class="<?php echo (Request::segment(1)=='browse' ? 'active' : ''); ?>"><a href="<?php echo e(url('/browse')); ?>" class="btn menu-btn"><i class="fa fa-globe" aria-hidden="true"></i><?php echo e(trans('common.browse')); ?> </a></li>
			<?php endif; ?>

			<li class="<?php echo (Request::segment(2)=='albums' ? 'active' : ''); ?>"><a href="<?php echo e(url('/'.Auth::user()->username.'/albums')); ?>" class="btn menu-btn"><i class="fa fa-film" aria-hidden="true"></i><?php echo e(trans('common.albums')); ?></a></li>

			<li class="<?php echo (Request::segment(1)=='messages' ? 'active' : ''); ?>"><a href="<?php echo e(url('messages')); ?>" class="btn menu-btn"><i class="fa fa-comments" aria-hidden="true"></i><?php echo e(trans('common.messages')); ?></a></li>



			<li><a href="<?php echo e(url(Auth::user()->username.'/groups')); ?>" class="btn menu-btn"><i class="fa fa-group" aria-hidden="true"></i><?php echo e(trans('common.groups')); ?> <span class="event-circle"><?php echo e(Auth::user()->own_groups()->count()); ?></span></a></li>

			<li><a href="<?php echo e(url(Auth::user()->username.'/everydaytask')); ?>" class="btn menu-btn"><i class="fa fa-tasks" aria-hidden="true"></i>Ежедневные <span class="event-circle"><?php echo e(Auth::user()->tasks()); ?></a></li>
			<li><a href="<?php echo e(url(Auth::user()->username.'/transaction')); ?>" class="btn menu-btn"><i class="fa fa-history" aria-hidden="true"></i>Операции</a></li>




			<li><a href="<?php echo e(url('/'.Auth::user()->username.'/settings/general')); ?>" class="btn menu-btn"><i class="fa fa-cog" aria-hidden="true"></i><?php echo e(trans('common.settings')); ?></a></li>
			
		</ul>
	</div>
    <div class="menu-heading">
        Заработок
    </div>

    <div class="menu-list">
        <ul class="list-unstyled">
            <li><a href="/matrix" class="btn menu-btn"><i class="fas fa-vector-square"></i>Матрицы</a></li>
            <li><a href="/surfing" class="btn menu-btn"><i class="far fa-window-restore"></i>Серфинг</a></li>
            <li><a href="/interview" class="btn menu-btn"><i class="fas fa-question-circle"></i>Опросы</a></li>
            <li><a href="<?php echo e(route('shortLink')); ?>" class="btn menu-btn"><i class="fas fa-link"></i>Ссылки</a></li>
            <li><a href="<?php echo e(url(Auth::user()->username.'/matrix')); ?>" class="btn menu-btn"><i class="fas fa-ticket-alt"></i>Лотерея</a></li>
        </ul>
    </div>

    <div class="menu-heading">
        Реклама
    </div>
    <div class="menu-list">
        <ul class="list-unstyled">
			
			<li><a href="<?php echo e(route('myLink')); ?>" class="btn menu-btn"><i class="far fa-window-restore"></i>Серфинг <i class="fas fa-bullhorn pull-right"></i></a></li>
			<li><a href="<?php echo e(route('myInterview')); ?>" class="btn menu-btn"><i class="fas fa-question-circle"></i>Опросы <i class="fas fa-bullhorn pull-right"></i></a></li>
			<li><a href="<?php echo e(route('shortLinkAdvert')); ?>" class="btn menu-btn"><i class="fas fa-link"></i>Ссылки <i class="fas fa-bullhorn pull-right"></i></a></li>
			<li><a href="<?php echo e(route('shortLinkAdvert')); ?>" class="btn menu-btn"><i class="fas fa-link"></i>Баннеры <i class="fas fa-bullhorn pull-right"></i></a></li>
		</ul>
    </div>
	<?php if(isset($trending_tags)): ?>
	<div class="menu-heading">
		<?php echo e(trans('common.most_trending')); ?>

	</div>
	<div class="categotry-list">
		<ul class="list-unstyled">
			<?php if($trending_tags != ""): ?>
				<?php $__currentLoopData = $trending_tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trending_tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li><span class="hash-icon"><i class="fa fa-hashtag"></i></span> <a href="<?php echo e(url('?hashtag='.$trending_tag->tag)); ?>"><?php echo e($trending_tag->tag); ?></a></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php else: ?>
				<span class="text-warning"><?php echo e(trans('messages.no_tags')); ?></span>
				
			<?php endif; ?>
		</ul>
	</div>
	<?php endif; ?>
</div><!-- /widget-events -->
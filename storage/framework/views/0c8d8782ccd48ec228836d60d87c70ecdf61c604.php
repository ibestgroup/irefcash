<li class="commented delete_comment_list comment-replied">
 <div class="comments"> <!-- main-comment -->
  <div class="commenter-avatar">
    <a href="#"><img src="<?php echo e($reply->user->avatar); ?>" title="<?php echo e($reply->user->name); ?>" alt="<?php echo e($reply->user->name); ?>"></a>
  </div>
  <div class="comments-list">
    <div class="commenter">
      <?php if($reply->user_id == Auth::user()->id): ?>
      <a href="#" class="delete-comment delete_comment" data-commentdelete-id="<?php echo e($reply->id); ?>"><i class="fa fa-times"></i></a>
      <?php endif; ?>  
      <div class="commenter-name">
        <a href="<?php echo e(url($reply->user->username)); ?>"><?php echo e($reply->user->name); ?></a><span class="comment-description"><?php echo e($reply->description); ?></span>
      </div>
      <ul class="list-inline comment-options">
          <?php
          if(!$reply->comments_liked->contains(Auth::user()->id)) {
              $likeIcon = '<i class="far fa-heart"></i>';
              $likeClass = '';
          } else {
              $likeIcon = '<i class="fas fa-heart active-like"></i>';
              $likeClass = 'active-like';
          }
          ?>
        <li>
          <a href="#" class="text-capitalize like-comment like-comment-<?php echo e($reply->id); ?> like" data-comment-id="<?php echo e($reply->id); ?>">
            <?php echo $likeIcon; ?>

          </a>
          <a href="#" class="show-users-modal count-likes-comment-<?php echo e($reply->id); ?> like3-<?php echo e($reply->id); ?> <?php echo $likeClass; ?>" data-heading="<?php echo e(trans('common.likes')); ?>" data-rec-id="<?php echo e($reply->id); ?>" data-type="comment">
            <?php echo e(($reply->comments_liked->count())?$reply->comments_liked->count():''); ?>

          </a>
        </li>
        <li>.</li>
        <li>
          <time class="post-time timeago" datetime="<?php echo e($reply->created_at); ?>" title="<?php echo e($reply->created_at); ?>"><?php echo e($reply->created_at); ?></time>
          
        </li>
      </ul>
    </div>
  </div>
</div><!-- main-comment -->
</li>
